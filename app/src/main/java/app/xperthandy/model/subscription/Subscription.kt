package app.xperthandy.model.subscription
import com.google.gson.annotations.SerializedName


data class SubscriptionData(
    @SerializedName("bgColor")
    val bgColor: String,
    @SerializedName("theme.color")
    val themeColor: String,
    @SerializedName("description")
    val description: String?,
    @SerializedName("freeValidity")
    val freeValidity: String?,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("order_id")
    val order_id: String,
    @SerializedName("currency")
    val currency: String,
    @SerializedName("name")
    val name: String?,
    @SerializedName("rate")
    val rate: String,
    @SerializedName("amount")
    val amount: String,
    @SerializedName("user_email")
    val userEmail: String,
    @SerializedName("shortDescription")
    val shortDescription: String?,
    @SerializedName("validity")
    val validity: String?   ,
    @SerializedName("expiry")
    val expiry: String?
)