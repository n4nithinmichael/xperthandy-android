package app.xperthandy.model.help

data class HelpData (
    var title : String,
    var id : Int,
    var items : List<HelpItems>
)
data class HelpItems (
    var title : String,
    var description : String,
    var id : Int,

)