package app.xperthandy.model.payments
import com.google.gson.annotations.SerializedName


class PaymentsResp : ArrayList<PaymentsData>()
data class PaymentsData(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("payment_date")
    val paymentDate: String?,
    @SerializedName("payment_status")
    val paymentStatus: String?,
    @SerializedName("payment_type")
    val paymentType: String?,
    @SerializedName("total")
    val total: String?,
    @SerializedName("user")
    val user: PaymentsUser?
)
data class PaymentsUser(
    @SerializedName("area")
    val area: String?,
    @SerializedName("district")
    val district: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("pin")
    val pin: String?,
    @SerializedName("profile_image")
    val profileImage: String?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("town")
    val town: String?
)