package app.xperthandy.model

data class SideMenuData (

    var id : Int,
    var title : String,
    var description : String = "",
    var type : Int = 0,
    var tintColor : Int = 0,
    var icon : Int = 0
    )
