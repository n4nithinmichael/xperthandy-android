package app.xperthandy.model.user
import com.google.gson.annotations.SerializedName


data class UserLoginResponse(
    @SerializedName("data")
    val `data`: UserLoginData?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("statusCode")
    val statusCode: Int?
)

data class UserLoginData(
    @SerializedName("area")
    val area: String?,
    @SerializedName("district")
    val district: String?,
    @SerializedName("dob")
    val dob: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("house_name")
    val houseName: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("otp_code")
    val otpCode: String?,
    @SerializedName("phone")
    val phone: String?,
    @SerializedName("phone_alternate")
    val phoneAlternate: String?,
    @SerializedName("pin")
    val pin: String?,
    @SerializedName("profile_image")
    val profileImage: String?,
    @SerializedName("roles")
    val roles: List<Role>,
    @SerializedName("state")
    val state: String?,
    @SerializedName("to_be_logged_out")
    val toBeLoggedOut: Boolean?,
    @SerializedName("token")
    val token: String?,
    @SerializedName("town")
    val town: String?,
    @SerializedName("url")
    val url: String?,
    @SerializedName("category")
    val category: List<Int>?,
)

data class ProfileImage(
    @SerializedName("user")
    val user: User?,
)
data class User(
    @SerializedName("profile_image")
    val profile_image: String?,
)

data class Role(
    val id: Int,
    val name: String,
    val permissions: List<Any>
)