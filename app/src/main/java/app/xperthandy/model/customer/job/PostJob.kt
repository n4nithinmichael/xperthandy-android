package app.xperthandy.model.customer.job

import com.google.gson.annotations.SerializedName

data class PostJobPayload(
    @SerializedName("service_category_id")
    val serviceCategory: Int?,
    @SerializedName("title")
    val title : String?,
    @SerializedName("description")
    val description : String?,
    @SerializedName("start_date")
    val startDate : String?,
    @SerializedName("start_time")
    val startTime : String?,
    @SerializedName("hours_required")
    val hoursRequired : String?
)