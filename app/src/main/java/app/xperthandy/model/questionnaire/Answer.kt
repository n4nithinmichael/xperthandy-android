package app.xperthandy.model.questionnaire


import com.google.gson.annotations.SerializedName

data class Answer(
    @SerializedName("answer")
    var answer: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("isCorrect")
    val isCorrect: Boolean?,
    var selected: Boolean  = false
)