package app.xperthandy.model.questionnaire


import com.google.gson.annotations.SerializedName

data class QuestionnaireData(
    @SerializedName("answerList")
    val answerList: List<Answer>,
    @SerializedName("id")
    val id: String?,
    @SerializedName("question")
    var question: String,
    var selected: Boolean  = false
)