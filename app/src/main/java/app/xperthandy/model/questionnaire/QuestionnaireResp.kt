package app.xperthandy.model.questionnaire


import com.google.gson.annotations.SerializedName

data class QuestionnaireResp(
    @SerializedName("data")
    val `data`: List<QuestionnaireData>?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("statusCode")
    val statusCode: Int?
)