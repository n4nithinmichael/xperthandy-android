package app.xperthandy.model.serviceProvider

import app.xperthandy.utils.Util
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

class JobListAllRes : ArrayList<JobListAllResItem>()
data class JobListAllResItem(
    @SerializedName("additional_hours_required")
    val additionalHoursRequired: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("hours_required")
    val hoursRequired: Int,
    @SerializedName("id")
    val id: Int,
    @SerializedName("job_status")
    val jobStatus: String,
    @SerializedName("job_status_history")
    val jobStatusHistory: List<Any>,
    @SerializedName("service_category")
    val serviceCategory: ServiceCategory,
    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("start_time")
    val startTime: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("user")
    val user: User
) {

    fun formattedDate () : String {

        return Util.apiDateFormatter().format(Util.apiDateFormatter().parse(startDate))

    }
    fun totalAmount () : String {

        return "${hoursRequired * serviceCategory.hourlyPrice.toDouble()}"

    }
}
data class ServiceCategory(
    @SerializedName("active")
    val active: Boolean,
    @SerializedName("background_colour_code")
    val backgroundColourCode: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("hourly_price")
    val hourlyPrice: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("list_order")
    val listOrder: Int,
    @SerializedName("minimum_price")
    val minimumPrice: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("service_type")
    val serviceType: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("web_image")
    val webImage: String
)

data class User(
    @SerializedName("area")
    val area: String?,
    @SerializedName("district")
    val district: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("pin")
    val pin: String?,
    @SerializedName("profile_image")
    val profileImage: String?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("town")
    val town: String?
)