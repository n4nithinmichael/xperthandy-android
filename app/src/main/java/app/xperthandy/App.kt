package app.xperthandy

import android.app.Application
import app.xperthandy.data.repository.preference.AppPreferences
import com.razorpay.Checkout
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AppPreferences.init(this)


    }

}
