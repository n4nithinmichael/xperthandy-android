package app.xperthandy.domain.subscription

import app.xperthandy.data.repository.SubscriptionRepository
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class SubscriptionActivateUseCase @Inject constructor(
    private val repository: SubscriptionRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<String, String?>(dispatcher) {
    override suspend fun execute(parameters: String): String? {
        return when(val result = repository.subscriptionActivate(parameters)){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}