package app.xperthandy.domain.subscription

import app.xperthandy.data.repository.SubscriptionRepository
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class ConfirmSubscriptionUseCase @Inject constructor(
    private val repository: SubscriptionRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<Pair<JsonObject,String>, SubscriptionData?>(dispatcher) {
    override suspend fun execute(parameters: Pair<JsonObject,String>): SubscriptionData? {
        return when(val result = repository.confirmSubscription(parameters)){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}