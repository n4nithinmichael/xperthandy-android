package app.xperthandy.domain.subscription

import app.xperthandy.data.repository.CustomerRepository
import app.xperthandy.data.repository.SubscriptionRepository
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class SubscriptionActiveListUseCase @Inject constructor(
    private val repository: SubscriptionRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<Unit, List<SubscriptionData>?>(dispatcher) {
    override suspend fun execute(parameters: Unit): List<SubscriptionData>? {
        return when(val result = repository.subscriptionActive()){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}