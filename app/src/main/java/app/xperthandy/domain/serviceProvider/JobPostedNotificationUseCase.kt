package app.xperthandy.domain.serviceProvider

import app.xperthandy.data.repository.ServiceProviderRepository
import app.xperthandy.model.serviceProvider.JobListAllRes
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class JobPostedNotificationUseCase @Inject constructor(
    private val repository: ServiceProviderRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : UseCase<Unit?,ArrayList<JobListAllResItem>?>(dispatcher){
    override suspend fun execute(parameters: Unit?): ArrayList<JobListAllResItem>? {
        return when(val result =repository.jobPostedNotification()){
            is Result.Success -> result.data
            is Result.Error -> throw  result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}