package app.xperthandy.domain.serviceProvider

import app.xperthandy.data.repository.ServiceProviderRepository
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class ServiceProviderProfileUseCase @Inject constructor(
    private val repository: ServiceProviderRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<Unit,UserData?>(dispatcher){
    override suspend fun execute(parameters: Unit): UserData? {
        return when(val result = repository.serviceProviderProfile()){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}