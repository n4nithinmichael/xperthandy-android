package app.xperthandy.domain.generic

import app.xperthandy.data.repository.CustomerRepository
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import com.google.gson.JsonObject
import com.squareup.moshi.Json
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class JobDetailsUseCase @Inject constructor(
    private val repository: CustomerRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<Int,JobListAllResItem?>(dispatcher) {
    override suspend fun execute(parameters: Int): JobListAllResItem? {
        return when(val  result = repository.jobDetails(parameters)){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}