package app.xperthandy.domain.fileupload


import app.xperthandy.data.repository.UploadRepository
import app.xperthandy.model.user.ProfileImage
import app.xperthandy.ui.registration.usecase.UseCase
import com.airpay.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import app.xperthandy.utils.*
import javax.inject.Inject

class FileUploadUseCase @Inject constructor(private val repository: UploadRepository,
                                            @IoDispatcher ioDispatcher: CoroutineDispatcher)
    : UseCase<String, ProfileImage?>(ioDispatcher) {

    override suspend fun execute(parameters: String): ProfileImage? {
        val frontImg = parameters?.let {
            val frontImage = File(it)
            MultipartBody.Part.createFormData(
                    "image",
                    frontImage.name, frontImage.asRequestBody("multipart/form-data".toMediaTypeOrNull()))
        }

        return when (val result = repository.uploadProfileImage(frontImg)
        ) {
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }


}