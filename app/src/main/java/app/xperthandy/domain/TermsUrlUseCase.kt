package app.xperthandy.domain

import app.xperthandy.data.repository.JsonRequestRepository
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class TermsUrlUseCase @Inject constructor(
    private val repository: JsonRequestRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<Unit,String?>(dispatcher) {
    override suspend fun execute(parameters: Unit): String? {
        return when(val result = repository.termsUrl()){
            is Result.Success -> result.data?.url
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}