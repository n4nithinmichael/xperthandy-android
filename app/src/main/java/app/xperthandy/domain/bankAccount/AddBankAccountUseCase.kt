package app.xperthandy.domain.bankAccount

import app.xperthandy.data.model.bankAccount.AddBankAccountPayload
import app.xperthandy.data.model.bankAccount.BankAccountListResItem
import app.xperthandy.data.repository.BankAccountRepository
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class AddBankAccountUseCase @Inject constructor(
    private val repository: BankAccountRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<AddBankAccountPayload, BankAccountListResItem?>(dispatcher) {
    override suspend fun execute(parameters: AddBankAccountPayload): BankAccountListResItem? {
        return when(val result = repository.addBankAccount(parameters)){
            is Result.Success -> result.data
            is Result.Error -> throw  result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}