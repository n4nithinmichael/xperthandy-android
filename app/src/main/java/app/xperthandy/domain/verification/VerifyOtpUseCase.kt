package app.xperthandy.domain.verification

import app.xperthandy.data.repository.JsonRequestRepository
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.ui.welcome.model.user.UserInfoData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class VerifyOtpUseCase @Inject constructor(
    private val repository: JsonRequestRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<JsonObject,UserLoginData?>(dispatcher) {
    override suspend fun execute(parameters: JsonObject): UserLoginData? {
        return when(val result = repository.verifyOtp(parameters)){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}