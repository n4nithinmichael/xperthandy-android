package app.xperthandy.domain.payment

import app.xperthandy.data.model.payment.SelectedPayment
import app.xperthandy.data.repository.CustomerRepository
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class ChoosePaymentUseCase @Inject constructor(
    private val repository: CustomerRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) :UseCase<Pair<JsonObject,String>, SelectedPayment?>(dispatcher){
    override suspend fun execute(parameters: Pair<JsonObject,String>): SelectedPayment? {
        return when(val result = repository.paymentMethod(parameters.first,parameters.second)){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}
