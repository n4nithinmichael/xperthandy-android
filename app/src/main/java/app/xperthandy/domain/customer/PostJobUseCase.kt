package app.xperthandy.domain.customer

import app.xperthandy.data.repository.CustomerRepository
import app.xperthandy.model.customer.job.PostJobPayload
import app.xperthandy.ui.registration.usecase.UseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Result
import com.airpay.shared.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class PostJobUseCase @Inject constructor(
    private val repository: CustomerRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
): UseCase<PostJobPayload, UserData?>(dispatcher) {
    override suspend fun execute(parameters: PostJobPayload): UserData? {
        return when (val result =  repository.postJob(parameters)){
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw AppException(Constants.UNKNOWN_ERROR)
        }
    }
}