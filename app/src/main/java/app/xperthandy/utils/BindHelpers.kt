package app.xperthandy.utils

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.ui.registration.adapter.WorkAreaAdapter
import app.xperthandy.ui.registration.listners.WorkAreaClickListener
import app.xperthandy.ui.registration.model.category.CategoryItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.io.File
import java.lang.Exception

class BindHelpers {
    companion object {


        @JvmStatic
        @BindingAdapter("menuimage")
        fun loadImage(

            imageView: ImageView,
            imageURL: String?
        ) {
            Glide.with(imageView.context)
                .setDefaultRequestOptions(
                    RequestOptions()
                        .override(300, 300)
                )
                .load(imageURL)
                .into(imageView)

        }

        @JvmStatic
        @BindingAdapter("visibility")
        fun setVisibility(view: View, isVisible: Boolean) {
            view.visibility = if (isVisible) View.VISIBLE else View.GONE
        }


        @JvmStatic
        @BindingAdapter("imagePath")
        fun imagePath(view: ImageView, path: String?) {
            if (!path.isNullOrEmpty()) {
                Glide.with(view.context)
                    .load(path)
                    .dontAnimate()
                    .placeholder(R.drawable.img_camera)
                    .into(view)
            } else {
                Glide.with(view.context)
                    .load(R.drawable.img_camera)
                    .into(view)
            }
        }

        @JvmStatic
        @BindingAdapter("thumbnail")
        fun thumbnail(view: ImageView, path: String?) {
            if (!path.isNullOrEmpty()) {
                val requestOptions = RequestOptions()
                requestOptions.isMemoryCacheable
                Glide.with(view.context).setDefaultRequestOptions(requestOptions).load(path)
                    .into(view)

            } else {
                Glide.with(view.context)
                    .load(R.drawable.img_camera)
                    .into(view)
            }
        }

        @JvmStatic
        @BindingAdapter("setIcon")
        fun setIcon(view: ImageView, path: Int) {

            view.setImageResource(path)
//
//                Glide.with(view.context)
//                    .load(path)
//                    .into(view)

        }

        @JvmStatic
        @BindingAdapter("backgroundColor")
        fun backgroundColor(view: View, color: String?) {
            if (!color.isNullOrEmpty()) {
                try {
                    view.setBackgroundColor(Color.parseColor(color))
                }catch (e:Exception){
                    view.setBackgroundColor(Color.WHITE)
                }

            } else {
                view.setBackgroundColor(Color.WHITE)
            }
        }

        @JvmStatic
        @BindingAdapter("goneUnless")
        fun goneUnless(view: View, visible: Boolean) {
            view.visibility = if (visible) View.VISIBLE else View.GONE
        }

        @JvmStatic
        @BindingAdapter("serviceType")
        fun serviceType(view: View, type: String) {
            when (type){

               "In Store" -> {
                   view.visibility = View.VISIBLE
               }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        @JvmStatic
        @BindingAdapter("circleImage")
        fun circleImage(view: ImageView, resource: String?) {
            if (!resource.isNullOrEmpty()) {
                if (resource.startsWith("http")) {
                    Glide.with(view.context)
                            .load(resource)
                            .dontAnimate()
                            .centerCrop()
                            .placeholder(R.drawable.male_avatar)
                            .apply(RequestOptions.circleCropTransform())
                            .into(view)
                } else {
                    Glide.with(view.context)
                            .load(File(resource))
                            .placeholder(R.drawable.male_avatar)
                            .apply(RequestOptions().circleCrop())
                            .dontAnimate()
                            .into(view)
                }

            } else {
                Glide.with(view.context)
                        .load(R.drawable.worker_avatar)
                        .into(view)
            }
        }

        @SuppressLint("NotifyDataSetChanged")
        @JvmStatic
        @BindingAdapter(value = ["setCategoryList", "workAreaClickListener"], requireAll = false)
        fun setCategoryList(recyclerView: RecyclerView,
                            list: LiveData<List<CategoryItem>>,
                            listener: WorkAreaClickListener
        ) {
            if (recyclerView.adapter == null) {
                recyclerView.adapter = WorkAreaAdapter(listener)
            }
            (recyclerView.adapter as WorkAreaAdapter).apply {

                list.value?.let {
                    val newList = ArrayList<CategoryItem>(it)
                    submitList(newList)
                    notifyDataSetChanged()
                }
//                submitList(list.value?.toList())
            }
//            recyclerView.scrollToPosition(0)
        }

        @JvmStatic
        @BindingAdapter(value = ["setImage","bgColor"],requireAll = false)
        fun setImage(view : ImageView, path : String?, bgColor : String?)
        {
            path?.let {
                Glide.with(view.context)
                    .load(path)
                    .dontAnimate()
                    .centerCrop()
                    .apply(RequestOptions.circleCropTransform())
                    .into(view)
            }
            bgColor?.let {
                ImageViewCompat.setImageTintList(view, ColorStateList.valueOf(Color.parseColor(it)));

            }

        }
        @JvmStatic
        @BindingAdapter("setCardBgColor")
        fun setCardBgColor(view : CardView, color : String?)
        {

            color?.let {
                view.setCardBackgroundColor(Color.parseColor(it));

            }

        }

        @JvmStatic
        @BindingAdapter("verifyStatus")
        fun verifyStatus(view : TextView, verified : Boolean)
        {
            if (verified) {
                view.text = "Verified"
                view.setTextColor(ContextCompat.getColor(view.context,R.color.green))
            }else {
                view.text = "Verify"
                view.setTextColor(ContextCompat.getColor(view.context,R.color.color_blue_send_btn))

            }

        }

        @JvmStatic
        @BindingAdapter("acceptIndicator")
        fun acceptIndicator(view : ImageView, status : String?)
        {

            when (status) {
                JobStatus.WorkAccepted.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.Accepted.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.Completed.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.InProgress.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.Cancelled.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.Rejected.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }

            }

        }
        @JvmStatic
        @BindingAdapter("inProgressIndicator")
        fun inProgressIndicator(view : ImageView, status : String?)
        {

            when (status) {
                JobStatus.WorkAccepted.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }
                JobStatus.Accepted.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }
                JobStatus.Completed.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.InProgress.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.Cancelled.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }
                JobStatus.Rejected.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }

            }

        }
        @JvmStatic
        @BindingAdapter("completedIndicator")
        fun completedIndicator(view : ImageView, status : String?)
        {

            when (status) {
                JobStatus.WorkAccepted.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }
                JobStatus.Accepted.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }
                JobStatus.Completed.value -> {
                    view.setImageResource(R.drawable.ic_status_complete)
                }
                JobStatus.InProgress.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }
                JobStatus.Cancelled.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }
                JobStatus.Rejected.value -> {
                    view.setImageResource(R.drawable.ic_status_uncomplete)
                }

            }

        }
        @JvmStatic
        @BindingAdapter("startWorkVisibility")
        fun startWorkVisibility(view : View, status : String?)
        {
            when (status) {
                JobStatus.WorkAccepted.value -> {
                    view.visibility = View.VISIBLE
                }
                JobStatus.Accepted.value -> {
                    view.visibility = View.VISIBLE
                }
                JobStatus.Completed.value -> {
                    view.visibility = View.GONE
                }
                JobStatus.InProgress.value -> {
                    view.visibility = View.GONE
                }
                JobStatus.Cancelled.value -> {
                    view.visibility = View.GONE
                }
                JobStatus.Rejected.value -> {
                    view.visibility = View.GONE
                }
            }
        }
        @JvmStatic
        @BindingAdapter("completeWorkVisibility")
        fun completeWorkVisibility(view : View, status : String?)
        {
            when (status) {
                JobStatus.WorkAccepted.value -> {
                    view.visibility = View.GONE
                }
                JobStatus.Accepted.value -> {
                    view.visibility = View.GONE
                }
                JobStatus.Completed.value -> {
                    view.visibility = View.GONE
                }
                JobStatus.InProgress.value -> {
                    view.visibility = View.VISIBLE
                }
                JobStatus.Cancelled.value -> {
                    view.visibility = View.GONE
                }
                JobStatus.Rejected.value -> {
                    view.visibility = View.GONE
                }
            }
        }

        @JvmStatic
        @BindingAdapter("statusNew")
        fun statusNew(view : TextView, status : String?)
        {
            when (status) {
                JobStatus.New.value -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.red_light))
                }else -> {
                view.setTextColor(ContextCompat.getColor(view.context,R.color.border_gray))
                }

            }

        }
        @JvmStatic
        @BindingAdapter("statusPending")
        fun statusPending(view : TextView, status : String?)
        {
            when (status) {
                JobStatus.WorkAccepted.value -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.red_light))
                }
                JobStatus.InProgress.value -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.red_light))
                }
                else -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.border_gray))
                }

            }

        }
        @JvmStatic
        @BindingAdapter("statusCompleted")
        fun statusCompleted(view : TextView, status : String?)
        {
            when (status) {
                JobStatus.Completed.value -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.red_light))
                }
                else -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.border_gray))
                }

            }

        }
        @JvmStatic
        @BindingAdapter("statusRejected")
        fun statusRejected(view : TextView, status : String?)
        {
            when (status) {
                JobStatus.Rejected.value -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.red_light))
                }
                else -> {
                    view.setTextColor(ContextCompat.getColor(view.context,R.color.border_gray))
                }

            }

        }
    }
}