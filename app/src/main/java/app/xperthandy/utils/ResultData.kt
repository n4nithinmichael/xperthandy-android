package app.xperthandy.utils

import com.google.gson.annotations.SerializedName

class ResultData<out T>(val statusCode: Status, val status: Status?,val message: String?, @SerializedName("data")val data: T?)