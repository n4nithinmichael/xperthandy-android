package app.xperthandy.utils

import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import app.xperthandy.R

class GenericTextWatcher constructor(private val last: Boolean, private val nextView: View?) :
    TextWatcher {
    override fun afterTextChanged(editable: Editable) {
        val text = editable.toString()

        if (text.length == 1 && !last) nextView!!.requestFocus()

//        when (currentView.id) {
//            R.id.edt_otp_one -> if (text.length == 1) nextView!!.requestFocus()
//            R.id.edt_otp_two -> if (text.length == 1) nextView!!.requestFocus()
//            R.id.edt_otp_three -> if (text.length == 1) nextView!!.requestFocus()
//        }
    }

    override fun beforeTextChanged(
        arg0: CharSequence,
        arg1: Int,
        arg2: Int,
        arg3: Int
    ) {
    }

    override fun onTextChanged(
        arg0: CharSequence,
        arg1: Int,
        arg2: Int,
        arg3: Int
    ) {
    }

}

class GenericKeyEvent constructor(private val first: Boolean, private val currentView: EditText,private val previousView: EditText?) : View.OnKeyListener{
    override fun onKey(p0: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if(event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL && !first && currentView.text.toString().isEmpty()) {
            //If current is empty then previous EditText's number will also be deleted
            previousView!!.text = null
            previousView.requestFocus()
            return true
        }
        return false
    }
}