package app.xperthandy.utils


import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import app.xperthandy.R
import com.google.android.material.button.MaterialButton


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class LoadingButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var button: MaterialButton
    private var progressBar: ProgressBar
    private var text: String? = null
    private var buttonColor: Int = 0
    private var textColor: Int = 0

    companion object {

        @JvmStatic
        @BindingAdapter("app:onClick")
        fun setOnClickListener(view: LoadingButton, onClick: OnClickListener?) {
            view.button.setOnClickListener(onClick)
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @JvmStatic
        @BindingAdapter("app:buttonColor")
        fun setButtonColor(view: LoadingButton, color: Int) {
            view.buttonColor = color
            view.drawButton()
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @JvmStatic
        @BindingAdapter("app:buttonEnable")
        fun setEnable(view: LoadingButton, isEnabled: Boolean) {
            view.setButtonEnabled(isEnabled)
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @JvmStatic
        @BindingAdapter("app:text")
        fun setTextColor(view: LoadingButton, color: Int) {
            view.textColor = color
            view.drawButton()
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @JvmStatic
        @BindingAdapter("app:buttonText")
        fun setButtonText(view: LoadingButton, res: Int) {
            view.text = view.resources.getString(res)
            view.drawButton()
        }

        @JvmStatic
        @BindingAdapter("app:buttonText")
        fun setButtonText(view: LoadingButton, text: String) {
            view.text = text
            view.button.text = text
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        @JvmStatic
        @BindingAdapter("app:loading")
        fun loading(view: LoadingButton, state: Boolean) {
            if (state) {
                view.alpha = 1f
                view.setButtonBackgroundEnabled(false)
                view.onStartLoading()
            } else {
                view.alpha = 1f
                view.setButtonEnabled(true)
                view.onStopLoading()
            }
        }

    }

    init {
        LayoutInflater.from(context).inflate(R.layout.progress_button, this, true)
        button = findViewById(R.id.button)
        progressBar = findViewById(R.id.progressBar)
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.LoadingButton, 0, 0)
        text = a.getString(R.styleable.LoadingButton_text)
        buttonColor = a.getColor(R.styleable.LoadingButton_buttonColor, 0)
        textColor = a.getColor(R.styleable.LoadingButton_textColor, 0)
        progressBar.visibility = View.GONE
        if (buttonColor == 0)
            buttonColor = R.color.color_blue_send_btn
        drawButton()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun drawButton() {
        if (button.isEnabled) {
            if (buttonColor != 0)
                button.setBackgroundColor(
                    ContextCompat.getColor(
                    context,
                    buttonColor
                ))
        } else {
            button.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.gray_light
                )
            )
        }
/*        if (buttonColor != 0)
            button.setBackgroundColor(buttonColor)*/
        button.text = text
        if (button.isEnabled) {
            if (textColor != 0) {
                button.setTextColor(textColor)
                progressBar.indeterminateTintList = ColorStateList.valueOf(textColor)
            } else {
                button.setTextColor(if (isDark(buttonColor)) Color.WHITE else Color.BLACK)
                progressBar.indeterminateTintList =
                    ColorStateList.valueOf(if (isDark(buttonColor)) Color.WHITE else Color.BLACK)
            }
        } else {
            button.setTextColor(Color.GRAY)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun enableDrawButton() {
        if (buttonColor != 0)
            button.setBackgroundColor(buttonColor)
        button.text = text
        if (button.isEnabled) {
            if (textColor != 0) {
                button.setTextColor(textColor)
                progressBar.indeterminateTintList = ColorStateList.valueOf(textColor)
            } else {
                button.setTextColor(if (isDark(buttonColor)) Color.WHITE else Color.BLACK)
                progressBar.indeterminateTintList =
                    ColorStateList.valueOf(if (isDark(buttonColor)) Color.WHITE else Color.BLACK)
            }
        } else {
            button.setTextColor(Color.GRAY)
        }
    }

    fun onClick(onClick: OnClickListener?) {
        button.setOnClickListener(onClick)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setButtonColor(color: Int) {
        buttonColor = color
        drawButton()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setTextColor(color: Int) {
        textColor = color
        drawButton()
    }

    fun setButtonText(text: String) {
        this.text = text
        button.text = text
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setButtonEnabled(isEnabled: Boolean) {
        button.isEnabled = isEnabled
        drawButton()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setButtonBackgroundEnabled(isEnabled: Boolean) {
        button.isEnabled = isEnabled
        //button.isFocused = (isEnabled)
        enableDrawButton()
    }

    fun onStartLoading() {
        button.text = ""
        button.isClickable = false
        progressBar.visibility = View.VISIBLE
    }

    fun onStopLoading() {
        button.isClickable = true
        button.text = text
        progressBar.visibility = View.GONE
    }

    fun isInProgress(): Boolean {
        return progressBar.visibility == View.VISIBLE
    }

    fun setButtonOnClick(onClick: OnClickListener?) {
        button.setOnClickListener(onClick)
    }

    private fun fetchAccentColor(): Int {
        val typedValue = TypedValue()
        val a = context.obtainStyledAttributes(typedValue.data, intArrayOf(R.attr.colorAccent))
        val color = a.getColor(0, 0)
        a.recycle()
        return color
    }

}