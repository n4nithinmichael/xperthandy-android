package app.xperthandy.utils

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ServiceGenerator {

    private val httpClient = OkHttpClient.Builder()


    fun <S> createService(serviceClass: Class<S>, baseUrl: String): S {

       // val gson = GsonBuilder().setLenient().create()
        val gson = GsonBuilder()
            .setLenient()
            .create()

        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build())
            .build()

        return builder.create(serviceClass)
    }
}