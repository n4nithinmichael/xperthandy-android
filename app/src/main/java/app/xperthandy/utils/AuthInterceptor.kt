package app.xperthandy.utils

import android.text.TextUtils
import app.xperthandy.data.repository.preference.AppPreferences
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import javax.inject.Inject

class AuthInterceptor @Inject constructor(private val preferenceStorage: AppPreferences) :
        Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val authenticatedRequest = request.newBuilder()
        authenticatedRequest.header("Authorization", preferenceStorage.token?:"")
//        authenticatedRequest.header("Authorization", "Bearer uRuofRVtc1RcXMHA6JVqcIzShLgnlINMOCRSe8FHPbZmjHc6xPWYKqCHvMk7yEP3JrOQmFgIDpxMeVDJ")
        authenticatedRequest.header("Content-Type", "application/json")
        if (!preferenceStorage.token.isNullOrEmpty()){
            authenticatedRequest.header("appToken", "fsda987243jhs89g29dsals89fl29ls932hs92hsa92")
        }

        return chain.proceed(authenticatedRequest.build())
    }
}

class JsonRequestInterceptor @Inject constructor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        if (response.isSuccessful) {
            val newResponse = response.newBuilder()
            var contentType = response.header("Content-Type")
            if (TextUtils.isEmpty(contentType)) contentType = "application/json"
            val responseString = response.body!!.string()
            val obj = JSONObject(responseString)
            try {
                return if (obj.has("statusCode")) {
                    when {
                        obj["statusCode"].toString() == "401" -> {
                            val json =
                                "{\"statusCode\": {\"statusCode\": \"401\",\"action\": \"000\"},\"message\": \"${
                                    if (obj.has("message")) obj["message"].toString() else Constants.UNKNOWN_ERROR
                                }\"}"
                            newResponse.body(
                                JSONObject(json).toString()
                                    .toResponseBody(contentType?.toMediaTypeOrNull())
                            )
                            newResponse.build()
                        }
                        obj["statusCode"].toString() == "500" -> {
                            val json =
                                "{\"statusCode\": {\"statusCode\": \"500\",\"action\": \"000\"},\"message\": \"${
                                    if (obj.has("message")) obj["message"].toString() else Constants.UNKNOWN_ERROR
                                }\"}"
                            newResponse.body(
                                JSONObject(json).toString()
                                    .toResponseBody(contentType?.toMediaTypeOrNull())
                            )
                            newResponse.build()
                        }
                        obj["statusCode"].toString() == "301" -> {
                            val json =
                                "{\"statusCode\": {\"statusCode\": \"301\",\"action\": \"000\"},\"message\": \"${
                                    if (obj.has("message")) obj["message"].toString() else Constants.SERVER_MAINTENANCE
                                }\"}"
                            newResponse.body(json.toResponseBody(contentType?.toMediaTypeOrNull()))
                            newResponse.build()
                        }
                        obj["statusCode"].toString() == "200" -> {
                            val resultData = obj
                            val json = "{\"statusCode\": ${createStatus(resultData)},\"message\": \"${
                                if (resultData.has("message")) resultData.getString("message") else "Success"
                            }\",\"data\": ${resultData}}"
                            newResponse.body(json.toResponseBody(contentType?.toMediaTypeOrNull()))
                            newResponse.build()
                        }
                        else -> {
                            val json =
                                "{\"statusCode\": {\"statusCode\": \"500\",\"action\": \"000\"},\"message\": \"${
                                    if (obj.has("message")) obj["message"].toString() else Constants.SERVER_MAINTENANCE
                                }\"}"
                            newResponse.body(json.toResponseBody(contentType?.toMediaTypeOrNull()))
                            newResponse.build()
                        }
                    }
                } else {
                    if (obj.has("data")) {
                        val data = obj.getJSONObject("data")
                        val json = "{\"statusCode\": ${createStatus(obj)},\"message\": \"${
                            if (obj.has("message")) obj.getString("message") else "Success"
                        }\",\"data\": $obj}"
                        newResponse.body(
                            JSONObject(json).toString()
                                .toResponseBody(contentType?.toMediaTypeOrNull())
                        )
                        newResponse.build()
                    } else {
                        val json =
                            "{\"statusCode\": {\"statusCode\": \"500\",\"action\": \"000\"},\"message\": \"${
                                if (obj.has("message")) obj["message"].toString() else Constants.UNKNOWN_ERROR
                            }\"}"
                        newResponse.body(
                            JSONObject(json).toString()
                                .toResponseBody(contentType?.toMediaTypeOrNull())
                        )
                        newResponse.build()
                    }
                }
            } catch (e: JSONException) {
                val json =
                    "{\"statusCode\": {\"statusCode\": \"500\",\"action\": \"000\"},\"message\": \"${e.message ?: Constants.UNKNOWN_ERROR}\"}"
                newResponse.body(json.toResponseBody(contentType?.toMediaTypeOrNull()))
                newResponse.build()
                e.printStackTrace()
            } catch (e: RuntimeException) {
                val json =
                    "{\"statusCode\": {\"statusCode\": \"500\",\"action\": \"000\"},\"message\": \"${e.message ?: Constants.UNKNOWN_ERROR}\"}"
                newResponse.body(json.toResponseBody(contentType?.toMediaTypeOrNull()))
                newResponse.build()
                e.printStackTrace()
            }
        }
        return response
    }

    private fun createStatus(resultData: JSONObject) =
        if (resultData.has("statusCode")) "{\"statusCode\":${resultData.get("statusCode")},\"action\":${
            if (resultData.has(
                    "respcode"
                )
            ) resultData.get("respcode") else "000"
        }}" else "{\"statusCode\":${"000"},\"action\":${if (resultData.has("respcode")) resultData.get("respcode") else "000"}}"
}