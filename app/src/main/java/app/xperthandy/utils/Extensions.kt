package app.xperthandy.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import app.xperthandy.databinding.MessageBottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

fun Fragment.showDatePicker(
    calendar: Calendar,
    disableBackDateSelection: Boolean = false,
    startFrom: Date = Date(),
    futureDate: Date? = null,
    listener: (Calendar) -> Unit
) {

    val datePickerDialog = DatePickerDialog(
        requireContext(), { _: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int ->
            calendar.set(year, monthOfYear, dayOfMonth)
            listener(calendar)
        }, calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    )
    if (disableBackDateSelection)
        datePickerDialog.datePicker.minDate = startFrom.time
    if (futureDate != null) {
        datePickerDialog.datePicker.maxDate = futureDate.time
    }
    datePickerDialog.show()
    datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
    datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
}

fun View.showKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_FORCED)
}

fun View.dismissKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun Fragment.messageHandle(status: Status, message: String?) {
    when (status) {
        Status.AUTHORIZATION_FAILED -> {
            showMessageBottomSheet(message)
        }
        Status.SERVER_MAINTENANCE -> {
            showMessageBottomSheet(message)
        }
        Status.ERROR -> {
            showMessageBottomSheet(message)
        }
        Status.LOCAL_MESSAGE -> {
            showMessageBottomSheet(message)
        }
        Status.NON_TRACEABLE -> {
            showMessageBottomSheet(message)
        }
        Status.TOAST -> {
            toast(message)
        }
        Status.NO_INTERNET -> {
            toast(message)
        }
        Status.TIME_OUT -> {
            showMessageBottomSheet(message)
        }
    }
}
fun Activity.messageHandle(status: Status, message: String?) {
    when (status) {
        Status.AUTHORIZATION_FAILED -> {
            showMessageBottomSheet(message)
        }
        Status.SERVER_MAINTENANCE -> {
            showMessageBottomSheet(message)
        }
        Status.ERROR -> {
            showMessageBottomSheet(message)
        }
        Status.LOCAL_MESSAGE -> {
            showMessageBottomSheet(message)
        }
        Status.NON_TRACEABLE -> {
            showMessageBottomSheet(message)
        }
        Status.TOAST -> {
            toast(message)
        }
        Status.NO_INTERNET -> {
            toast(message)
        }
        Status.TIME_OUT -> {
            showMessageBottomSheet(message)
        }
    }
}

fun Fragment.showMessageBottomSheet(message: String?, okButton: String = "Ok") {
    val bottomSheetDialog = BottomSheetDialog(requireContext())
    val binding = MessageBottomSheetBinding.inflate(layoutInflater)
    bottomSheetDialog.setContentView(binding.root)
    bottomSheetDialog.show()
    binding.message.text = message
    binding.buttonOk.text = okButton
    binding.buttonOk.setOnClickListener {
        bottomSheetDialog.cancel()
    }
    binding.imageViewClose.setOnClickListener {
        bottomSheetDialog.cancel()
    }
}fun Activity.showMessageBottomSheet(message: String?, okButton: String = "Ok") {
    val bottomSheetDialog = BottomSheetDialog(this)
    val binding = MessageBottomSheetBinding.inflate(layoutInflater)
    bottomSheetDialog.setContentView(binding.root)
    bottomSheetDialog.show()
    binding.message.text = message
    binding.buttonOk.text = okButton
    binding.buttonOk.setOnClickListener {
        bottomSheetDialog.cancel()
    }
    binding.imageViewClose.setOnClickListener {
        bottomSheetDialog.cancel()
    }
}

fun Fragment.toast(message: String?) = Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
fun Activity.toast(message: String?) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
fun isDark(color: Int): Boolean {
    val darkness =
        1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255
    return darkness >= 0.5
}

 fun getOtpEntered(edtOne: EditText, edtTwo: EditText, edtThree: EditText, edtFour: EditText) : String {
    return  edtOne.text.toString() +
            edtTwo.text.toString() +
            edtThree.text.toString() +
            edtFour.text.toString()
}