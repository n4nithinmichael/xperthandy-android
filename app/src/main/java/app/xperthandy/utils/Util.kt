package app.xperthandy.utils

import java.text.SimpleDateFormat
import java.util.*

object Util {
    fun displayDateFormatter() = SimpleDateFormat("dd MMM yyyy", Locale.US)
    fun apiDateFormatter() = SimpleDateFormat("yyyy-MM-dd",Locale.US)
}
enum class JobStatus (val value:String) {
    ALL(value = "All"),
    New(value = "New"),
    WorkAccepted(value = "Work accepted"),
    Accepted(value = "Accepted"),
    Completed(value = "Completed"),
    InProgress(value = "In Progress"),
    Cancelled(value = "Cancelled"),
    Rejected(value = "Rejected"),


}