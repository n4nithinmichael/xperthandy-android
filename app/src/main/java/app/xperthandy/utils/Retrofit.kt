package app.xperthandy.utils

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


interface Retrofit {
    @Headers("Accept: application/json", "Content-Type: application/json","appToken: fsda987243jhs89g29dsals89fl29ls932hs92hsa92")

   /* @POST(Urls.login)
    fun logIn(@Body jsonObject: JsonObject): Call<JsonObject>*/

    @GET(Urls.SERVICE_CATEGORY)
    fun getServiceCategory(@Header("Authorization") authHeader: String): Call<JsonObject>

    @POST(Urls.class_list)
    fun getClassList(@Header("Authorization") authHeader: String,@Body jsonObject: JsonObject): Call<JsonObject>

    @POST(Urls.student_subject)
    fun getSubjectList(@Header("Authorization") authHeader: String,@Body jsonObject: JsonObject): Call<JsonObject>

    @POST(Urls.live_video)
    fun getLiveVideoList(@Header("Authorization") authHeader: String,@Body jsonObject: JsonObject): Call<JsonObject>

    @POST(Urls.get_subject_video)
    fun getSubjectVideo(@Header("Authorization") authHeader: String,@Body jsonObject: JsonObject): Call<JsonObject>

    @POST(Urls.get_notice_list)
    fun getNoticeList(@Header("Authorization") authHeader: String,@Body jsonObject: JsonObject): Call<JsonObject>

    @POST(Urls.change_password)
    fun changePassword(@Header("Authorization") authHeader: String,@Body jsonObject: JsonObject): Call<JsonObject>

    @POST(Urls.student_reg)
    @FormUrlEncoded
    fun StudentRegister(@Header("Authorization") authHeader: String,@Field("name") name:String,
                        @Field("phone") phone:String,
                        @Field("place") place:String,
                        @Field("school_id") school_id:String,
                        @Field("class_id") class_id:String,
                        @Field("username") username:String,
                        @Field("password") password:String): Call<JsonObject>

    @GET(Urls.get_admin_video)
    fun getAdminVideo(@Header("Authorization") authHeader: String): Call<JsonObject>

}