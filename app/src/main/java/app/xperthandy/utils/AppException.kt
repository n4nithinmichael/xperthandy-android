package app.xperthandy.utils

class AppException(message: String, val code: Status) : Exception(message) {
    constructor(message: String) : this(message, Status.NON_TRACEABLE)
}

