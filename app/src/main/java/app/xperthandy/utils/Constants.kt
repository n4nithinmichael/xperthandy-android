package app.xperthandy.utils

import java.util.*

object Constants {
    const val UNKNOWN_ERROR = "Something went wrong"
    const val SERVER_MAINTENANCE = "Server maintenance"
    const val HTTP_NO_INTERNET = "Internet is not available"
    const val TIMEOUT_ERROR = "Please try again later"
    const val DEVICE_OS = "android"
    const val EMPTY_RESPONSE_MESSAGE = "please try after some time."
    const val DATA = "data"
    const val JOB_ID = "job_id"
    const val JOB_STATUS = "job_status"
    fun uniqueId():String = UUID.randomUUID().toString().replace("-","")

}