package app.xperthandy.utils

object XHConstant {


    val IS_FIRST_RUN_PREF = Pair("is_first_run", true)
    val TOKEN = Pair("auth_token", "")
    val USER_NAME = Pair("user_name", "")
    val PROVIDER_ID = Pair("provider_id", 0)
    val USER_TYPE = Pair("user_type", 0)
    const val SELECTED_ID = "selected)id"
    const val SHARED_PREFERENCES_NAME: String = "xperthandy_shared_preference"
    const val PREF_LANGUAGES: String = "pref_language"

    @JvmStatic // JvmStatic annotation for access in java code
    fun sumValue(v1: Int, v2: Int): Int {
        return v1 + v2
    }
}