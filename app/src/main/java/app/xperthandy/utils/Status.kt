package app.xperthandy.utils

import com.google.gson.annotations.SerializedName

enum class Status {
    @SerializedName("200")
    SUCCESS,

    @SerializedName("500",alternate = ["400"])
    ERROR,

    @SerializedName("401")
    AUTHORIZATION_FAILED,
    @SerializedName("301")
    SERVER_MAINTENANCE,
    LOCAL_MESSAGE,
    NON_TRACEABLE,
    NO_INTERNET,
    END_OF_RECORD,
    TOAST,
    NO_IMAGE_PATH,
    TIME_OUT,
    LOADING,

}