package app.xperthandy.utils

class Urls {
    companion object{
        const val baseUrl= "http://xperthandy.flyzoft.com/"
        const val SERVICE_CATEGORY = "api/v1/serviceCategory"
        const val school_list= "school"
        const val class_list= "class"
        const val student_reg="student_register"
        const val student_login = "login"
        const val student_subject = "subject"
        const val get_subject_video = "get_video"
        const val get_admin_video = "get_admin_video"
        const val get_user = "user"
        const val get_video_views = "video_views"
        const val postFirebaseToken = "user/set-firebase-token"
        const val get_notice_list = "notices"
     const val  get_all_comments = "get_comments"
     const val add_comment = "add_comment"
     const val profile_update = "student_register_update"
     const val view_notice = "view_notice"
     const val  live_video = "live_video"
     const val live_video_views="live_video_views";
     const val comment_delete = "delete_comment";
     const val change_password = "change-password"
     const val version_check = "version"

    }
}
