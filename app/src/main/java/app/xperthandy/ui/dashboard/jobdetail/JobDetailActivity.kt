package app.xperthandy.ui.dashboard.jobdetail

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.JobDetailScreenBinding
import app.xperthandy.ui.registration.view.UserTpe
import app.xperthandy.utils.Constants.JOB_ID
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.JobStatus
import app.xperthandy.utils.Status
import app.xperthandy.utils.messageHandle
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class JobDetailActivity : AppCompatActivity() {
    private lateinit var binding : JobDetailScreenBinding
    private val jobDetailsViewModel : JobDetailsViewModel by viewModels()
    private val jobId by lazy {
        intent.extras?.getInt(JOB_ID)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.job_detail_screen)
        binding.viewModel = jobDetailsViewModel
        binding.lifecycleOwner = this

        binding.apply {
            imageBackButton.setOnClickListener { onBackPressed() }
            jobId?.let {
                jobDetailsViewModel.updateJobId(it)
            }
        }
        jobDetailsViewModel.error.observe(this, EventObserver{
            messageHandle(Status.LOCAL_MESSAGE,it.second)
        })
        jobDetailsViewModel.success.observe(this, EventObserver{
            finish()
        })
        jobDetailsViewModel.jobDetails.observe(this) {
            binding.buttonContainer.visibility = View.VISIBLE
            if (it.jobStatus == JobStatus.New.value) {
                if (AppPreferences.userType == UserTpe.CUSTOMER.value) {
                    binding.buttonContainer.visibility = View.VISIBLE
                    binding.accept.visibility = View.GONE
                    binding.cancel.visibility = View.VISIBLE
                    jobDetailsViewModel.cancelText.value = "Cancel"
                } else {
                    binding.buttonContainer.visibility = View.VISIBLE
                    binding.accept.visibility = View.VISIBLE
                    binding.cancel.visibility = View.VISIBLE
                    jobDetailsViewModel.cancelText.value = "Cancel"
                }
            } else {
                binding.buttonContainer.visibility = View.VISIBLE
                binding.accept.visibility = View.GONE
                binding.cancel.visibility = View.VISIBLE
                binding.cancel.isEnabled = false
                jobDetailsViewModel.cancelText.value = "Cancel"
            }
        }
    }
}