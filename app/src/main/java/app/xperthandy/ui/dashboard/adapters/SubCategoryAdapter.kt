package app.xperthandy.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.xperthandy.databinding.SubCategoryItemBinding
import app.xperthandy.ui.dashboard.listener.SubCategoryListener
import app.xperthandy.ui.registration.model.category.SubCategoryItem
import kotlinx.android.synthetic.main.sub_category_item.view.*

class SubCategoryAdapter(var listener: SubCategoryListener) : ListAdapter<SubCategoryItem,SubCategoryAdapter.ViewHolder>(SubCategoryDiff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(SubCategoryItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
     holder.bind(getItem(position))
    }

    inner class  ViewHolder(private val binding: SubCategoryItemBinding): androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root){
        fun bind(item: SubCategoryItem){
            binding.apply {
                this.item = item
                this.listener = this@SubCategoryAdapter.listener
                executePendingBindings()
            }
        }
    }
}
object SubCategoryDiff  : DiffUtil.ItemCallback<SubCategoryItem>() {
    override fun areItemsTheSame(oldItem: SubCategoryItem, newItem: SubCategoryItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: SubCategoryItem, newItem: SubCategoryItem): Boolean {
        return oldItem.subCategoryName == newItem.subCategoryName
    }
}