package app.xperthandy.ui.dashboard.bank

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.Adapter.BankListAdapter
import app.xperthandy.R
import app.xperthandy.data.model.bankAccount.BankAccountListResItem
import app.xperthandy.databinding.BankListActivityBinding
import app.xperthandy.ui.dashboard.listener.BankListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class BankListActivity : AppCompatActivity() {
    private val viewModel : BankListViewModel by viewModels()
    private lateinit var binding : BankListActivityBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding = DataBindingUtil.setContentView(this,R.layout.bank_list_activity)
        binding.apply {
            viewModel = this@BankListActivity.viewModel
            lifecycleOwner = this@BankListActivity
            textScreenTitle.text = "Bank List"
            imageBackButton.setOnClickListener { onBackPressed() }
        }

        binding.btnLinkAccount.setOnClickListener {
            startActivity(Intent(this, AddBankActivity::class.java))
        }

    }

    override fun onResume() {
        super.onResume()
        viewModel.getBankAccountList()
    }

    private fun checkIsFirstTime() {

//        var firstRun = XHPreference(this).firstRun
//
//        val navHostFragment = navFragmentIntro as NavHostFragment
//        val graphInflater = navHostFragment.navController.navInflater
//        var navGraph = graphInflater.inflate(R.navigation.graph)
//        var navController = navHostFragment.navController
//
//        val destination = if (firstRun) R.id.homeFragment else R.id.newPrivacyPolicyFragment
//        navGraph.startDestination = destination
//        navController.graph = navGraph
    }
}
@BindingAdapter(value = ["bankList","bankListener"],requireAll = false)
fun bankList(recyclerView: RecyclerView,list : LiveData<List<BankAccountListResItem>>,listener: BankListener){
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = BankListAdapter(listener)
    (recyclerView.adapter as BankListAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}