package app.xperthandy.ui.dashboard

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.databinding.ChangePasswordBinding
import app.xperthandy.ui.registration.view.password.PasswordViewModel
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.Status
import app.xperthandy.utils.messageHandle
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ChangePasswordActivity : AppCompatActivity() {

    private val viewModel : PasswordViewModel by viewModels()
    private lateinit var binding : ChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.change_password)
        binding.apply {
            viewModel = this@ChangePasswordActivity.viewModel
            lifecycleOwner = this@ChangePasswordActivity
            imageBackButton.setOnClickListener { onBackPressed() }
        }

        viewModel.error.observe(this, EventObserver {
            messageHandle(Status.LOCAL_MESSAGE,it.second)
        })

    }

}