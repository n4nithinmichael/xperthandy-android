package app.xperthandy.ui.dashboard.bankdetails

import app.xperthandy.model.payments.PaymentsData

interface PaymentsListener {
    fun paymentClick (item: PaymentsData)
}