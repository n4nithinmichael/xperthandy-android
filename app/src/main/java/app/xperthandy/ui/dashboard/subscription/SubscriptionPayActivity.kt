package app.xperthandy.ui.dashboard.subscription

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.xperthandy.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*


@AndroidEntryPoint
class SubscriptionPayActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_subscription)
        textScreenTitle.text = "Subscription Payment"
        imageBackButton.setOnClickListener { onBackPressed() }


    }

}