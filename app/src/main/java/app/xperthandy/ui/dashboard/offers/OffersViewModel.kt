package app.xperthandy.ui.dashboard.offers

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.data.model.payment.PaymentData
import app.xperthandy.domain.payment.ApplyOfferUseCase
import app.xperthandy.domain.payment.GetOffersUseCase
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.dashboard.jobstatus.OfferListener
import app.xperthandy.utils.Event
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class OffersViewModel @ViewModelInject constructor(
    private val getOffersUseCase: GetOffersUseCase,
    private val applyOfferUseCase: ApplyOfferUseCase,
) : BaseViewModel(), OfferListener {

    val paymentInfo = MutableLiveData<PaymentData>()
    val offerData = MutableLiveData<List<OfferData>>()
    val selectedOffer = MutableLiveData<Event<OfferData>>()

    val _submit = MutableLiveData<Event<Unit>>()
    var submit : LiveData<Event<Unit>> = _submit

    private val _copyText = MutableLiveData<Event<String>>()
    var copyText : LiveData<Event<String>> = _copyText



    fun getOffers() {
        viewModelScope.launch {
            loading.postValue(true)
            when (val result = getOffersUseCase(Unit)) {
                is Result.Success -> {
                    loading.postValue(false)
                    result.data?.let {
                        offerData.postValue(it)
                    }

                }
                is Result.Error -> {
                    loading.postValue(false)
                    postError(result.exception.code, result.exception.message)
                }
            }
        }
    }

    override fun offerItemClick(item: OfferData) {

        _copyText.value = Event(item.offerCode?:"")
//
//        if (!offerData.value.isNullOrEmpty()) {
//
//            var list = offerData.value
//            list?.forEach {
//                if (it.id == item.id) {
//                    it.selected = true
//                } else {
//                    it.selected = false
//                }
//            }
//            offerData.value = list
//        }
//        selectedOffer.value = Event(item)
    }

    fun applyVoucher() {

        selectedOffer.value?.let {
            applyValidVoucher(it.peekContent())
        } ?: kotlin.run {
            postError(Status.LOCAL_MESSAGE, "Please select a voucher")
        }

    }

    private fun applyValidVoucher(peekContent: OfferData) {

        var jsonObject = JsonObject()
        jsonObject.addProperty("voucher", peekContent.offerCode)
        viewModelScope.launch {
            loading.postValue(true)
            when (val result = applyOfferUseCase(Pair(jsonObject, peekContent.id.toString()))) {
                is Result.Success -> {
                    loading.postValue(false)
                    result.data?.let {
                        paymentInfo.value = it
                    }

                }
                is Result.Error -> {
                    loading.postValue(false)
                    postError(result.exception.code, result.exception.message)
                }
            }
        }

    }

    fun payNow() {
        _submit.value = Event(Unit)
    }

}