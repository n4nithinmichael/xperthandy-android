package app.xperthandy.ui.dashboard.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.model.help.HelpItems
import app.xperthandy.utils.ExpandableLayout
import kotlinx.android.synthetic.main.help_inner_item.view.*
import kotlinx.android.synthetic.main.help_inner_item.view.dropDownArrow
import kotlinx.android.synthetic.main.help_inner_item.view.expand_layout

class AttributesAdapter(private var optionsList: List<HelpItems>) : RecyclerView.Adapter<AttributesAdapter.ViewHolder> () {
    lateinit var context: Context
    var expandedByDefault = true
    private var expandedPositionSet: HashSet<Int> = HashSet()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
                LayoutInflater.from(parent.context).inflate(R.layout.help_inner_item, parent, false)
        val vh = ViewHolder(v)
        context = parent.context
        return vh
    }
    override fun getItemCount(): Int {
        return optionsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var item = optionsList[position]
        holder.bindView(item,this)

    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(item: HelpItems, attributesAdapter: AttributesAdapter) {
            itemView.help_description.text = item.description

            itemView.helpDescriptionTitle.text = item.title
            itemView.expand_layout.setOnExpandListener(object :
                ExpandableLayout.OnExpandListener {
                override fun onExpand(expanded: Boolean) {
                    if (adapterPosition == 0) {
                        attributesAdapter.expandedByDefault = expanded
                    }else {
                        if (attributesAdapter.expandedPositionSet.contains(position)) {
                            attributesAdapter.expandedPositionSet.remove(position)
                        } else {
                            attributesAdapter.expandedPositionSet.add(position)
                        }
                    }
                    if (expanded) {
                        itemView.dropDownArrow.setImageResource(R.drawable.ic_minus)
                    }else {
                        itemView.dropDownArrow.setImageResource(R.drawable.ic_plus)
                    }

                }
            })
//            attributesAdapter.setFirstCellExpanded(this, position)
        }
    }
    private fun setFirstCellExpanded(holder: ViewHolder, position: Int) {
        if (position == 0) {
            if (expandedByDefault) {
                holder.itemView.post {
                    holder.itemView.expand_layout.setExpand(true)
                }
            } else {
                holder.itemView.post {
                    holder.itemView.expand_layout.setExpand(false)
                }
            }
        }
    }
}