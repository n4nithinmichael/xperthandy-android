package app.xperthandy.ui.dashboard.tasks

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.xperthandy.databinding.TaskItemBinding
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.dashboard.listener.TaskListener
import kotlinx.android.synthetic.main.layout_dashboard_notification_item.view.*
import kotlinx.android.synthetic.main.layout_payment_row_item.view.*
import kotlinx.android.synthetic.main.task_item.view.*

class TasksLisAdapter(var listener : TaskListener) : ListAdapter<JobListAllResItem, TasksLisAdapter.ViewHolder>(
    TaskListDiff
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(TaskItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(getItem(position))
        /*holder.itemView.setOnClickListener {  holder.itemView.context?.let {
            it.startActivity(Intent(it, JobStatusActivity :: class.java)) }
        }*/

    }

    inner class  ViewHolder(private val binding : TaskItemBinding): androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root){
        fun bind(item: JobListAllResItem){
            binding.apply {
                this.item = item
                this.listener = this@TasksLisAdapter.listener
                executePendingBindings()
            }
        }
    }
}
object TaskListDiff : DiffUtil.ItemCallback<JobListAllResItem>() {
    override fun areItemsTheSame(oldItem: JobListAllResItem, newItem: JobListAllResItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: JobListAllResItem,
        newItem: JobListAllResItem
    ): Boolean {
       return oldItem.id == newItem.id
    }
}