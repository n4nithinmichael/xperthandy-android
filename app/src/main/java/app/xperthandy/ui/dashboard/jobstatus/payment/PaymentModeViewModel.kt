package app.xperthandy.ui.dashboard.jobstatus.payment

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.model.payment.SelectedPayment
import app.xperthandy.domain.generic.JobDetailsUseCase
import app.xperthandy.domain.payment.*
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.utils.Event
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class PaymentModeViewModel @ViewModelInject constructor(
    private val jobDetailsUseCase: JobDetailsUseCase,
    private val confirmPaymentUseCase: ConfirmPaymentUseCase,
    private val choosePaymentUseCase: ChoosePaymentUseCase,
): BaseViewModel(), PaymentSelection {

    val jobId = MutableLiveData<String>()
    val totalAmount = MutableLiveData<String>()
    val buttonName = MutableLiveData<String>()

    val selectedPayment = MutableLiveData<SelectedPayment>()

    private val _onSubmit = MutableLiveData<Event<Any>>()
    val onSubmit : LiveData<Event<Any>> = _onSubmit

    val selectType = MutableLiveData<PaymentType>().apply { value = PaymentType.CARD }

    val paymentType = ""

    private fun getJobDetails(id: Int){
        viewModelScope.launch {
            loading.postValue(true)
           when(val result = jobDetailsUseCase(id)) {
               is Result.Success ->{
                   loading.postValue(false)
//                   jobDetails.value = result.data
               }
               is Result.Error ->{
                   loading.postValue(false)
                   postError(result.exception.code,result.exception.message)
               }
           }
        }
    }

    private fun choosePayment(name: String?) {

        var jsonObject  = JsonObject ()
        jsonObject.addProperty("payment_type",name?:"")

        viewModelScope.launch {
            loading.postValue(true)
            when(val result = choosePaymentUseCase(Pair(jsonObject,jobId.value?:""))) {
                is Result.Success ->{
                    loading.postValue(false)
                    result.data?.let {
                        selectedPayment.value  = it

                        when(it.paymentType){
                            PaymentType.CASH.data ->{
                                confirmPayment(it.paymentGatewayOrderId)
                            }
                            PaymentType.CARD.data ->{
                                _onSubmit.value = Event(it)
                            }
                        }

                    }

                }
                is Result.Error ->{
                    loading.postValue(false)
                    postError(result.exception.code,result.exception.message)
                }
            }
        }

    }

    fun confirmPayment(orderId: String?) {

        var jsonObject  = JsonObject ()
        jsonObject.addProperty("payment_gateway_order_id",orderId?:"")

        viewModelScope.launch {
            loading.postValue(true)
            when(val result = confirmPaymentUseCase(Pair(jsonObject,jobId.value?:""))) {
                is Result.Success ->{
                    loading.postValue(false)
                    result.data?.let {
                        postError(Status.LOCAL_MESSAGE,"Success, Status:${it.paymentStatus}")
                        _onSubmit.value = Event(Unit)
                    }

                }
                is Result.Error ->{
                    loading.postValue(false)
                    postError(result.exception.code,result.exception.message)
                }
            }
        }

    }


    fun proceed(){

        choosePayment(selectType.value?.data)

//        _onSubmit.value = Event(selectType.value!!)
    }

    fun updateJobId(it: Int) {
        jobId.value = it.toString()
    }

    override fun paymentItemClick(item: PaymentType) {
        selectType.value = item

        when (item){
            PaymentType.CARD -> {
                buttonName.value = "Proceed to pay ₹${totalAmount.value}"
            }
            PaymentType.CASH -> {
                buttonName.value = "Pay ₹${totalAmount.value}"
            }
            PaymentType.GPAY -> {}

        }

    }

}

enum class PaymentType (val data :String) {
    CARD (data = "card"),
    CASH (data="cash"),
    GPAY (data="gpay")
}