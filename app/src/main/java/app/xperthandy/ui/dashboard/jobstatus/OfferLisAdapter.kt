package app.xperthandy.ui.dashboard.jobstatus

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.databinding.OfferListItemBinding
import app.xperthandy.databinding.TaskItemBinding
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.dashboard.listener.TaskListener
import kotlinx.android.synthetic.main.layout_dashboard_notification_item.view.*
import kotlinx.android.synthetic.main.layout_payment_row_item.view.*
import kotlinx.android.synthetic.main.task_item.view.*

class OfferLisAdapter(var listener : OfferListener) : ListAdapter<OfferData, OfferLisAdapter.ViewHolder>(
    TaskListDiff
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(OfferListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(getItem(position))
        /*holder.itemView.setOnClickListener {  holder.itemView.context?.let {
            it.startActivity(Intent(it, JobStatusActivity :: class.java)) }
        }*/

    }

    inner class  ViewHolder(private val binding : OfferListItemBinding): androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root){
        fun bind(item: OfferData){
            binding.apply {
                this.data = item
                this.listener = this@OfferLisAdapter.listener
                executePendingBindings()
            }
        }
    }
}
object TaskListDiff : DiffUtil.ItemCallback<OfferData>() {
    override fun areItemsTheSame(oldItem: OfferData, newItem: OfferData): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: OfferData,
        newItem: OfferData
    ): Boolean {
       return oldItem.id == newItem.id
    }
}