package app.xperthandy.ui.dashboard.profile

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.domain.fileupload.FileUploadUseCase
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.registration.usecase.login.GetUserUseCase
import app.xperthandy.ui.registration.usecase.login.UpdateUserUseCase
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import kotlinx.coroutines.launch

class ProfileViewModel @ViewModelInject constructor(
    private val getProfileUseCase: GetUserUseCase,
    private val updateUserUseCase: UpdateUserUseCase,
    private val fileUploadUseCase: FileUploadUseCase,

    ) : BaseViewModel() {

    private val _profileData = MutableLiveData<UserLoginData>()
    var profileData: LiveData<UserLoginData> = _profileData

    var name = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var mobile = MutableLiveData<String>()
    var dob = MutableLiveData<String>()
    var gender = MutableLiveData<String>()
    var houseName = MutableLiveData<String>()
    var town = MutableLiveData<String>()
    var area = MutableLiveData<String>()
    var district = MutableLiveData<String>()
    var pinCode = MutableLiveData<String>()
    var state = MutableLiveData<String>()
    val userImage = MutableLiveData<String>().apply { value = "" }

    init {
        profile()
    }

    fun profile() {
        viewModelScope.launch {
            loading.value = true
            when (val result = getProfileUseCase(Unit)) {
                is Result.Success -> {
                    loading.value = false
                    result.data?.let {
                        _profileData.value = it
                        name.value = it.name
                        email.value = it.email
                        mobile.value = it.phone
                        dob.value = it.dob
                        pinCode.value = it.pin
                        district.value = it.district
                        area.value = it.area
                        town.value = it.town
                        gender.value = it.gender
                        houseName.value = it.houseName
                        state.value = it.state
                        userImage.value = it.profileImage
                    }
                }
                is Result.Error -> {
                    loading.value = true
                    postError(result.exception.code, result.exception.message)
                }
            }
        }
    }

    fun submit() {
        if (validate()) {
            val request =
                UpdateUserRequest(
                    area = area.value ?: "",
                    district = district.value ?: "",
                    dob = dob.value ?: "",
                    gender = gender.value ?: "",
                    houseName = houseName.value ?: "",
                    name = name.value ?: "",
                    pin = pinCode.value ?: "",
                    town = town.value ?: "",
                    email = email.value,
                    phoneAlternate = null,
                    roles = AppPreferences.userType,
                    state = state.value?:""
                )
            updateUser(request)
        }
    }

    private fun validate(): Boolean {

        var isValid = true
        when {
            name.value.isNullOrEmpty() -> {
                isValid = false
                postError(Status.LOCAL_MESSAGE, "Please enter valid name")
            }
            email.value.isNullOrEmpty() -> {
                isValid = false
                postError(Status.LOCAL_MESSAGE, "Please enter valid email")
            }
            mobile.value.isNullOrEmpty() -> {
                isValid = false
                postError(Status.LOCAL_MESSAGE, "Please enter valid mobile")
            }
            dob.value.isNullOrEmpty() -> {
                isValid = false
                postError(Status.LOCAL_MESSAGE, "Please enter valid dob")
            }
        }

        return isValid
    }


    fun updateUser(request: UpdateUserRequest) {

        loading.postValue(true)
        viewModelScope.launch {
            when (val result = updateUserUseCase(request)) {
                is Result.Success -> {
                    result.data?.let {
                        profile()
                    }
                    postError(Status.LOCAL_MESSAGE, "Profile updated successfully!")
                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    fun uploadFile(filePath: String) {
        loading.postValue(true)
        viewModelScope.launch {
            when (val result = fileUploadUseCase(filePath)) {
                is Result.Success -> {
                    postError(Status.LOCAL_MESSAGE, "Profile image updated successfully!")
                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }


    }

}

