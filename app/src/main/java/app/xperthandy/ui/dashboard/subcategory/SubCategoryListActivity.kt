package app.xperthandy.ui.dashboard.subcategory

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.databinding.SelectSubCategoryActivityBinding
import app.xperthandy.ui.dashboard.jobpost.PostJobActivity
import app.xperthandy.ui.dashboard.adapters.*
import app.xperthandy.ui.dashboard.listener.SubCategoryListener
import app.xperthandy.ui.registration.model.category.SubCategoryItem
import app.xperthandy.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.select_sub_category_activity.*

@AndroidEntryPoint
class SubCategoryListActivity : AppCompatActivity() {
    private lateinit var binding : SelectSubCategoryActivityBinding
    private val viewModelSubCat : SubCategoryViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.select_sub_category_activity)
        binding = DataBindingUtil.setContentView(this,R.layout.select_sub_category_activity)
        binding.viewModel = viewModelSubCat
        binding.lifecycleOwner = this
        binding.apply {
            textScreenTitle.text = "Post a Job"
            imageBackButton.setOnClickListener { onBackPressed() }
            val data = intent?.extras?.get(Constants.DATA)
            btnNext.setOnClickListener {

                if (viewModelSubCat.selectedSubCategory.value.isNullOrEmpty()){
                    Toast.makeText(this@SubCategoryListActivity,"Please select a sub category",Toast.LENGTH_SHORT).show()
                }else {
                    val intent = Intent(this@SubCategoryListActivity, PostJobActivity::class.java)
                    val bundle = bundleOf(Constants.DATA to data,"subCategory" to viewModelSubCat.selectedSubCategory.value)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }

            }
        }


    }

}
@SuppressLint("NotifyDataSetChanged")
@BindingAdapter(value = ["setSubCategoryList", "subCategoryListener"], requireAll = false)
fun setSubCategoryList(recyclerView: RecyclerView,
                    list: LiveData<List<SubCategoryItem>>,
                    listener: SubCategoryListener
) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = SubCategoryAdapter(listener)
    }
    (recyclerView.adapter as SubCategoryAdapter).apply {
        submitList(list.value)
        notifyDataSetChanged()
    }
    recyclerView.scrollToPosition(0)
}