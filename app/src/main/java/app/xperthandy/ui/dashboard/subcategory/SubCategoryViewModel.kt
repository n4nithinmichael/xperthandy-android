package app.xperthandy.ui.dashboard.subcategory

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.dashboard.listener.SubCategoryListener
import app.xperthandy.ui.registration.model.category.SubCategoryItem

class SubCategoryViewModel @ViewModelInject constructor(): BaseViewModel(), SubCategoryListener {
    val subCategoryList = MutableLiveData<List<SubCategoryItem>>()
    val selectedSubCategory = MutableLiveData<String>()
    init {
        getSubCategory()
    }
    override fun onSubCategorySelect(item: SubCategoryItem) {
        selectedSubCategory.value = item.subCategoryName
        val list = subCategoryList.value
        list?.apply {
            for (i in this){
                i.selected = i.subCategoryName == item.subCategoryName
            }
        }
        subCategoryList.value = list

    }
    private fun getSubCategory () {
        subCategoryList.value = listOf(SubCategoryItem("Maintenance"),
            SubCategoryItem("Panel Builder"),
            SubCategoryItem("Machine Repairer"),
            SubCategoryItem("Industrial Electrician"),
            SubCategoryItem("Automotive Electrician"),
        )
    }
}