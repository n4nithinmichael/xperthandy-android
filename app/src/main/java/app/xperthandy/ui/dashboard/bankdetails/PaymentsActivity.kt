package app.xperthandy.ui.dashboard.bankdetails

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.Adapter.PaymentAdapter
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.PaymentsActivityBinding
import app.xperthandy.model.payments.PaymentsData
import app.xperthandy.ui.dashboard.bank.AddBankActivity
import app.xperthandy.ui.registration.view.UserTpe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dashboard_activity.*
import kotlinx.android.synthetic.main.intro_activity.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.payments_activity.*

@AndroidEntryPoint
class PaymentsActivity : AppCompatActivity() {

    private val viewModel : BankPaymentsViewModel by viewModels()
    private lateinit var binding : PaymentsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.payments_activity)
        binding.apply {
            viewModel = this@PaymentsActivity.viewModel
            lifecycleOwner = this@PaymentsActivity
        }
        textScreenTitle.text = "Payments"
        imageBackButton.setOnClickListener { onBackPressed() }
        if (AppPreferences.userType == UserTpe.PROVIDER.value){
            binding.btnLinkAccount.visibility = View.VISIBLE
            viewModel.paymentsReceived()
        }else{
            binding.btnLinkAccount.visibility = View.GONE
            viewModel.paymentsPaid()
        }

        viewModel.addBank.observe(this, Observer {
            startActivity(Intent(this, AddBankActivity::class.java))
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.getBankAccountList()
    }
}

@BindingAdapter ("linkAccount")
fun linkAccount(view: AppCompatButton, haveBankAccount : Boolean) {

    if (haveBankAccount) {
        view.text = "Remove linked bank account"
        view.setTextColor(ContextCompat.getColor(view.context, R.color.red_light))
    }else{
        view.text = "Link Account"
        view.setTextColor(ContextCompat.getColor(view.context, R.color.color_blue_login_title))
    }

}

@BindingAdapter(value = ["payments","paymentsListener"],requireAll = false)
fun payments(recyclerView: RecyclerView, list : LiveData<List<PaymentsData>>, listener: PaymentsListener){
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = PaymentAdapter(listener)
    (recyclerView.adapter as PaymentAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}