package app.xperthandy.ui.dashboard.jobstatus

import app.xperthandy.data.model.payment.OfferData

interface OfferListener {
    fun offerItemClick (item : OfferData)
}