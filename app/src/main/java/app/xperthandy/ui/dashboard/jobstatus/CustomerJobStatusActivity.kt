package app.xperthandy.ui.dashboard.jobstatus

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.FragmentJobDetailsCutomserBinding
import app.xperthandy.ui.dashboard.jobstatus.payment.PaymentModeActivity
import app.xperthandy.ui.registration.view.UserTpe
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.JobStatus
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*


@AndroidEntryPoint
class JobStatusCustomerActivity : AppCompatActivity() {

    private lateinit var binding: FragmentJobDetailsCutomserBinding
    private val jobStatusViewModel: JobStatusViewModel by viewModels()
    private val jobId by lazy {
        intent.extras?.getInt(Constants.JOB_ID)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_job_details_cutomser)
        binding.viewModel = jobStatusViewModel
        binding.lifecycleOwner = this


        binding.apply {
            imageBackButton.setOnClickListener { onBackPressed() }
            jobId?.let {
                jobStatusViewModel.updateJobId(it)
            }
        }

        textScreenTitle.text = "Job Details"
        imageBackButton.setOnClickListener { onBackPressed() }


        jobStatusViewModel.success.observe(this, EventObserver {
            jobId?.let {
                jobStatusViewModel.updateJobId(it)
            }
        })


        jobStatusViewModel.jobDetails.observe(this, {

            when (it.jobStatus) {
                JobStatus.Completed.value -> {
                    if (AppPreferences.userType == UserTpe.CUSTOMER.value) {
                        jobStatusViewModel.completed.postValue(true)
                        jobStatusViewModel.showPaymentInfo()
                    }
                }
            }

        })

        jobStatusViewModel.submit.observe(this,EventObserver{

            var intent = Intent(this, PaymentModeActivity::class.java)
            val bundle = bundleOf(Constants.JOB_ID to jobId,Constants.DATA to jobStatusViewModel.paymentInfo.value?.total)
            intent.putExtras(bundle)
            startActivity(intent)

        })


    }

}

@BindingAdapter(value = ["offerLists","offerListsClick"],requireAll = false)
fun voucherLists(
    recyclerView: RecyclerView,
    list: LiveData<List<OfferData>>,
    listener : OfferListener
){
    if(recyclerView.adapter == null){
        recyclerView.adapter = OfferLisAdapter(listener)
    }
    (recyclerView.adapter as OfferLisAdapter).apply {
        submitList(list.value)
        notifyDataSetChanged()
    }
    recyclerView.scrollToPosition(0)
}

@BindingAdapter("offerBg")
fun offerBg(
    view: LinearLayout,
    selected:Boolean
){
    if (selected) {
        view.background = ContextCompat.getDrawable(view.context,R.drawable.shape_rect_stroke_green)
    }else{
        view.background = null
    }
}