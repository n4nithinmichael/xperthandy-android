package app.xperthandy.ui.dashboard.bankdetails

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.model.bankAccount.BankAccountListResItem
import app.xperthandy.domain.bankAccount.BankAccountListUseCase
import app.xperthandy.domain.bankAccount.DeleteBankAccountUseCase
import app.xperthandy.domain.payment.PaymentCompletedUseCase
import app.xperthandy.domain.payment.PaymentReceivedUseCase
import app.xperthandy.model.payments.PaymentsData
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.dashboard.listener.BankListener
import app.xperthandy.utils.Event
import app.xperthandy.utils.Result
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class BankPaymentsViewModel @ViewModelInject constructor(
    private val bankAccountListUseCase: BankAccountListUseCase,
    private val deleteBankAccountUseCase: DeleteBankAccountUseCase,
    private val paymentReceivedUseCase: PaymentReceivedUseCase,
    private val paymentCompletedUseCase: PaymentCompletedUseCase,
): BaseViewModel(),PaymentsListener {

    val list = MutableLiveData<List<BankAccountListResItem>>()
    val payments = MutableLiveData<List<PaymentsData>>()

    val haveBank = MutableLiveData<Boolean> ()
    private val singleBankId = MutableLiveData<String> ()

    private val _addBank = MutableLiveData<Event<Unit>> ()
    val addBank : LiveData<Event<Unit>> get() =  _addBank

    fun getBankAccountList(){
        viewModelScope.launch {
            loading.postValue(true)
            when(val result = bankAccountListUseCase(Unit)){
                is Result.Success ->{
                    loading.postValue(false)
                    list.postValue(result.data)

                    if (!result.data.isNullOrEmpty()) {
                        singleBankId.value = result.data[0].id
                        haveBank.value = true
                    }else{
                        singleBankId.value = ""
                        haveBank.value = false
                    }

                }
                is Result.Error ->{
                    loading.postValue(false)
                }
                is Result.Loading ->{
                    loading.postValue(true)
                }
            }
        }
    }

    fun paymentsPaid(){

        var jsonObject = JsonObject ()

        viewModelScope.launch {
            loading.postValue(true)
            when(val result = paymentCompletedUseCase(jsonObject)){
                is Result.Success ->{
                    loading.postValue(false)
                    payments.postValue(result.data)
                }
                is Result.Error ->{
                    loading.postValue(false)
                }
                is Result.Loading ->{
                    loading.postValue(true)
                }
            }
        }
    }

    fun paymentsReceived(){

        var jsonObject = JsonObject ()

        viewModelScope.launch {
            loading.postValue(true)
            when(val result = paymentReceivedUseCase(jsonObject)){
                is Result.Success ->{
                    loading.postValue(false)
                    payments.postValue(result.data)
                }
                is Result.Error ->{
                    loading.postValue(false)
                }
                is Result.Loading ->{
                    loading.postValue(true)
                }
            }
        }
    }



    fun deleteBankAccount(){
        viewModelScope.launch {
            loading.postValue(true)
            when(val result = deleteBankAccountUseCase(singleBankId.value?:"")){
                is Result.Success ->{
                    loading.postValue(false)
                    getBankAccountList()
                }
                is Result.Error ->{
                    loading.postValue(false)
                }
                is Result.Loading ->{
                    loading.postValue(true)
                }
            }
        }
    }

    fun linkAccountClick() {

        if (haveBank.value == true) {
            deleteBankAccount()
        }else{
            _addBank.value = Event(Unit)
        }

    }

    override fun paymentClick(item: PaymentsData) {

    }
}