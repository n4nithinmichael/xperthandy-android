package app.xperthandy.ui.dashboard.listener

import app.xperthandy.model.serviceProvider.JobListAllResItem

interface TaskListener {
    fun onTaskSelect(item : JobListAllResItem)
}