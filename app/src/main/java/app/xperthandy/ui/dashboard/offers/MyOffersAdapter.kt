package app.xperthandy.ui.dashboard.offers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.databinding.MyOfferListItemBinding
import app.xperthandy.databinding.OfferListItemBinding
import app.xperthandy.databinding.SubscriptionListItemBinding
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.ui.dashboard.jobstatus.OfferListener

class MyOffersAdapter (val listener: OfferListener): ListAdapter<OfferData,MyOffersAdapter.ViewHolder>(OfferListDiff)
{
  inner class ViewHolder(private val binding : MyOfferListItemBinding) : RecyclerView.ViewHolder(binding.root)
  {
      fun bind(item: OfferData)
      {
          binding.apply {
              this.data = item
              this.listener = this@MyOffersAdapter.listener
              executePendingBindings()
          }
      }
  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(MyOfferListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }
}
object OfferListDiff : DiffUtil.ItemCallback<OfferData>() {
    override fun areItemsTheSame(oldItem: OfferData, newItem: OfferData): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: OfferData, newItem: OfferData): Boolean {
        return oldItem == newItem
    }
}
