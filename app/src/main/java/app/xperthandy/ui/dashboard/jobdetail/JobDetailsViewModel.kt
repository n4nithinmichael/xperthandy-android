package app.xperthandy.ui.dashboard.jobdetail

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.domain.generic.JobDetailsUseCase
import app.xperthandy.domain.generic.JobStatusPayload
import app.xperthandy.domain.generic.UpdateJobStatusUseCase
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.utils.JobStatus
import app.xperthandy.utils.Result
import kotlinx.coroutines.launch

class JobDetailsViewModel @ViewModelInject constructor(
    private val jobDetailsUseCase: JobDetailsUseCase,
    private val updateJobStatusUseCase: UpdateJobStatusUseCase
): BaseViewModel() {
     val jobDetails = MutableLiveData<JobListAllResItem>()
     val cancelText = MutableLiveData<String>().apply { value = "REJECTED" }
   private fun jobDetails(id: Int){
        viewModelScope.launch {
            loading.postValue(true)
           when(val result = jobDetailsUseCase(id)) {
               is Result.Success ->{
                   loading.postValue(false)
                   jobDetails.value = result.data
               }
               is Result.Error ->{
                   loading.postValue(false)
                   postError(result.exception.code,result.exception.message)
               }
           }
        }
    }
    private fun updateJobStatus(status : JobStatus){

        viewModelScope.launch {
            loading.postValue(true)
            when(val result = updateJobStatusUseCase(Pair(JobStatusPayload(jobStatus = status.value),jobDetails.value?.id?:0))) {
                is Result.Success ->{
                    loading.postValue(false)
                    postSuccess(true)
                }
                is Result.Error ->{
                    loading.postValue(false)
                    postError(result.exception.code,result.exception.message)
                }
            }
        }
    }

    fun updateJobId(id: Int) {
        jobDetails(id)
    }

    fun acceptJob() {
        updateJobStatus(JobStatus.WorkAccepted)
    }
    fun rejectJob() {
        updateJobStatus(JobStatus.Rejected)
    }

}