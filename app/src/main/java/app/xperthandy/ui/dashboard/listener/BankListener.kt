package app.xperthandy.ui.dashboard.listener

import app.xperthandy.data.model.bankAccount.BankAccountListResItem

interface BankListener {
    fun onBankClick(item : BankAccountListResItem)
}