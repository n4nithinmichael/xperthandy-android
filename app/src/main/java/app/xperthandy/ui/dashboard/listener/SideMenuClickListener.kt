package app.xperthandy.ui.dashboard.listener

import app.xperthandy.model.SideMenuData


interface SideMenuClickListener {
    fun onSideMenuClick(item: SideMenuData)
}