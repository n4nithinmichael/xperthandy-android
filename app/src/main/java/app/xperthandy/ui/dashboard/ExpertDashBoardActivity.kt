package app.xperthandy.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.Adapter.DashboardNotificationAdapter
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.DashboardActivityBinding
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.dashboard.adapters.SideMenuListAdapter
import app.xperthandy.ui.dashboard.bankdetails.PaymentsActivity
import app.xperthandy.ui.dashboard.jobdetail.JobDetailActivity
import app.xperthandy.ui.dashboard.listener.SideMenuClickListener
import app.xperthandy.ui.dashboard.listener.TaskListener
import app.xperthandy.model.SideMenuData
import app.xperthandy.ui.dashboard.notification.NotificationActivity
import app.xperthandy.ui.dashboard.profile.MyProfileActivity
import app.xperthandy.ui.dashboard.subscription.ActiveSubscriptionActivity
import app.xperthandy.ui.dashboard.tasks.TaskListActivity
import app.xperthandy.ui.dashboard.viewmodel.DashBoardViewModel
import app.xperthandy.ui.help_contactus.ContactUsActivity
import app.xperthandy.ui.help_contactus.HelpActivity
import app.xperthandy.ui.registration.view.RegistrationActivity
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.JobStatus
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.dashboard_activity.*
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class DashBoardActivity : AppCompatActivity() {

    private val viewModel: DashBoardViewModel by viewModels()
    private lateinit var binding: DashboardActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.dashboard_activity
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        textScreenTitle.text = "Dashboard"
        binding.rvNotification.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL, false
        )

        imageBackButton.setImageResource(R.drawable.ic_menu)
        viewModel.sideMenuList.value = fetchSideMenu()

        binding.llTasks.setOnClickListener {

            var intent = Intent(this, TaskListActivity::class.java)
            intent.putExtra(Constants.JOB_STATUS, JobStatus.ALL.value)
            startActivity(intent)
        }

        binding.llCompleted.setOnClickListener {
            var intent = Intent(this, TaskListActivity::class.java)
            intent.putExtra(Constants.JOB_STATUS, JobStatus.Completed.value)
            startActivity(intent)
        }
        binding.llPending.setOnClickListener {
            var intent = Intent(this, TaskListActivity::class.java)
            intent.putExtra(Constants.JOB_STATUS, JobStatus.InProgress.value)
            startActivity(intent)
        }

        img_dp.setOnClickListener {
            startActivity(Intent(this, MyProfileActivity::class.java))
        }
        viewModel.select.observe(this, EventObserver {
            Log.i("JOB_ID", "EventObserver: $it")
            val intent = Intent(this, JobDetailActivity::class.java)
            val bundle = bundleOf(Constants.JOB_ID to it)
            intent.putExtras(bundle)
            startActivity(intent)
        })

        imageBackButton.setOnClickListener { openDrawer() }
        observeLeftMenuClick()

    }

    private fun observeLeftMenuClick() {
        viewModel.sideMenuItemClick.observe(this, Observer {

            closeDrawer()

            when (it) {
                2 -> {
                    startActivity(Intent(this, MyProfileActivity::class.java))
                }
                4 -> {
                    startActivity(Intent(this, PaymentsActivity::class.java))
                }
                5 -> {
                    startActivity(Intent(this, NotificationActivity::class.java))
                }
                6 -> {
                    startActivity(Intent(this, ContactUsActivity::class.java))
                }
                7 -> {
                    startActivity(Intent(this, HelpActivity::class.java))
                }
                10 -> {
                    startActivity(Intent(this, ActiveSubscriptionActivity::class.java))
                }
                3 -> {
                    var intent = Intent(this, TaskListActivity::class.java)
                    intent.putExtra(Constants.JOB_STATUS, JobStatus.ALL.value)
                    startActivity(intent)
                }
                8 -> {
                    AppPreferences.logOut()
                    startActivity(Intent(this, RegistrationActivity::class.java))
                }
            }

        })
    }

    private fun fetchSideMenu(): List<SideMenuData> {

        var sideMenuList = mutableListOf<SideMenuData>()

        sideMenuList.add(
            SideMenuData(
                id = 1,
                title = "Dashboard",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_dashboard,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 2,
                title = "My profile",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_profile,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 3,
                title = "Tasks",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_tasks,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 4,
                title = "Payments",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_payments,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 10,
                title = "My Subscriptions",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_subscriptions,
                type = 0
            )
        )
//        sideMenuList.add(
//            SideMenuData(
//                id = 5,
//                title = "Notifications",
//                tintColor = R.color.tint_blue,
//                icon = R.drawable.ic_notifications,
//                type = 0
//            )
//        )
        sideMenuList.add(
            SideMenuData(
                id = 6,
                title = "Contact Us",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_contactus,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 7,
                title = "Help",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_help,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 8,
                title = "Logout",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_logout_red,
                type = 0
            )
        )


        return sideMenuList

    }

    fun openDrawer() {
        binding.drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer() {
        binding.drawerLayout?.closeDrawers()
    }

    override fun onResume() {
        super.onResume()
        viewModel.profile()
    }
}

@BindingAdapter(value = ["sideMenu", "sideMenuClick"], requireAll = true)
fun sideMenu(
    recyclerView: RecyclerView,
    list: LiveData<List<SideMenuData>>,
    listener: SideMenuClickListener,
) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = SideMenuListAdapter(listener)
    }
    (recyclerView.adapter as SideMenuListAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}

@BindingAdapter(value = ["jobNotificationList", "jobNotificationListener"], requireAll = false)
fun jobNotificationList(
    recyclerView: RecyclerView,
    list: LiveData<List<JobListAllResItem>>,
    listener: TaskListener,
) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = DashboardNotificationAdapter(listener)
    }
    (recyclerView.adapter as DashboardNotificationAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}
