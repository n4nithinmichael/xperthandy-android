package app.xperthandy.ui.dashboard.notification

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.ui.dashboard.adapters.NotificationAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.notification_activity.*

@AndroidEntryPoint
class NotificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notification_activity)
        textScreenTitle.text = "Notifications"
        imageBackButton.setOnClickListener { onBackPressed() }

        rv_notifications.layoutManager = LinearLayoutManager(this,
            RecyclerView.VERTICAL,false)
        rv_notifications.adapter =  NotificationAdapter(this)
        rv_notifications.adapter!!.notifyDataSetChanged()

    }
}