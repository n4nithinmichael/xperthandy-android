package app.xperthandy.ui.dashboard.jobpost

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.databinding.PostJobActivityBinding
import app.xperthandy.ui.dashboard.subcategory.SelectCategoryActivity
import app.xperthandy.ui.dashboard.CustomerDashboardActivity
import app.xperthandy.ui.dashboard.subcategory.SubCategoryListActivity
import app.xperthandy.ui.registration.model.category.CategoryItem
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.messageHandle
import com.github.dhaval2404.imagepicker.ImagePicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.post_job_activity.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class PostJobActivity : AppCompatActivity() {
    private lateinit var binding : PostJobActivityBinding
    private lateinit var timePicker : TimePickerDialog
    private val postJobViewModel : PostJobViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.post_job_activity)

        binding = DataBindingUtil.setContentView(this,R.layout.post_job_activity)
        binding.apply {
            viewModel = this@PostJobActivity.postJobViewModel
            lifecycleOwner = this@PostJobActivity
            textScreenTitle.text = "Post a Job"
            imageBackButton.setOnClickListener { onBackPressed() }
            val data = intent?.extras?.getParcelable<CategoryItem>(Constants.DATA)
            val subCategory = intent?.extras?.getString("subCategory")?:""
            postJobViewModel.updateCategories(data?.id,subCategory)
            tvJobTypeValue.text = data?.name
            tvJobSubTypeValue.text = subCategory

            binding.llBtnImgUpload.setOnClickListener { openImagePicker() }
            binding.llBtnTakePic.setOnClickListener { openImagePicker() }

            val cal = Calendar.getInstance()
            val startDateSetListener =
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    cal.set(Calendar.YEAR, year)
                    cal.set(Calendar.MONTH, monthOfYear)
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    postJobViewModel.onStartDateUpdate(setDateLogic(cal))
                }

            val endDateSetListener =
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    cal.set(Calendar.YEAR, year)
                    cal.set(Calendar.MONTH, monthOfYear)
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    postJobViewModel.onEndDateUpdate(setDateLogic(cal))
                }
            val startTimePickListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    // logic to properly handle
                    // the picked timings by user
                    postJobViewModel.onStartTimeUpdate(setTimeLogic(hourOfDay,minute))
                }
            val endTimePickListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    // logic to properly handle
                    // the picked timings by user
                    postJobViewModel.onEndTimeUpdate(setTimeLogic(hourOfDay,minute))
                }
            tv_change.setOnClickListener {
                startActivity(Intent(this@PostJobActivity, SelectCategoryActivity::class.java))

            }
            appCompatTextView.setOnClickListener {
                startActivity(Intent(this@PostJobActivity, SubCategoryListActivity::class.java))

            }
            seekBarHour.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    postJobViewModel.onHoursUpdate(seekBar?.progress?:1)

                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                    postJobViewModel.onHoursUpdate(seekBar?.progress?:1)
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    postJobViewModel.onHoursUpdate(seekBar?.progress?:1)
                }

            })
            tvPrefTimeFrom.setOnClickListener{
                val timePicker  = TimePickerDialog(
                    this@PostJobActivity,R.style.MyTimePickerDialogTheme,startTimePickListener,12,10,false
                )
                timePicker.show()
            }
            tvPrefTimeTo.setOnClickListener{
                val timePicker  = TimePickerDialog(
                    this@PostJobActivity,R.style.MyTimePickerDialogTheme,endTimePickListener,12,10,false
                )
                timePicker.show()
            }
            tvDateFrom.setOnClickListener {
                var datePicker = DatePickerDialog(
                    this@PostJobActivity,R.style.MyTimePickerDialogTheme,
                    startDateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                )
                datePicker.datePicker.minDate = cal.timeInMillis;
                datePicker.show()
            }
            tvDateTo.setOnClickListener {
                DatePickerDialog(
                    this@PostJobActivity,R.style.MyTimePickerDialogTheme,
                    endDateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                ).show()
            }

            postJobViewModel.error.observe(this@PostJobActivity, EventObserver {
                messageHandle(it.first, it.second)
            })
            postJobViewModel.success.observe(this@PostJobActivity, EventObserver {
                when (it){
                    is Boolean -> {
                        startActivity(Intent(this@PostJobActivity, CustomerDashboardActivity :: class.java))
                        finish()
                    }
                }
            })

            postJobViewModel.serviceType.observe(this@PostJobActivity, EventObserver {

                when (it) {

                    ServiceType.LOCATION -> {

                        binding.instoreLayout.background = ContextCompat.getDrawable(this@PostJobActivity, R.drawable.shape_rect_corner_gray_fill)
                        binding.locationLayout.background = ContextCompat.getDrawable(this@PostJobActivity, R.drawable.shape_rect_corner_green_fill)
                        binding.instoreText.setTextColor( ContextCompat.getColor(this@PostJobActivity,R.color.color_blue_desc))
                        binding.locationText.setTextColor( ContextCompat.getColor(this@PostJobActivity,R.color.white))
                        binding.instoreTick.visibility = View.GONE
                        binding.locationTick.visibility = View.VISIBLE
                    }
                    else -> {
                        binding.instoreLayout.background = ContextCompat.getDrawable(this@PostJobActivity, R.drawable.shape_rect_corner_green_fill)
                        binding.locationLayout.background = ContextCompat.getDrawable(this@PostJobActivity, R.drawable.shape_rect_corner_gray_fill)
                        binding.instoreText.setTextColor( ContextCompat.getColor(this@PostJobActivity,R.color.white))
                        binding.locationText.setTextColor( ContextCompat.getColor(this@PostJobActivity,R.color.color_blue_desc))
                        binding.instoreTick.visibility = View.VISIBLE
                        binding.locationTick.visibility = View.GONE
                    }
                }

            })


        }
    }

    private fun setDateLogic(cal: Calendar): String {
        val myFormat = "yyyy-MM-dd" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        return sdf.format(cal.time)
    }

    private fun setTimeLogic(hourOfDay: Int, minute: Int): String {
         return when {
            hourOfDay == 0 -> {
                if (minute < 10) {
                    "${hourOfDay + 12}:0${minute}"
                } else {
                    "${hourOfDay + 12}:${minute}"
                }
            }
            hourOfDay > 12 -> {
                if (minute < 10) {
                    "${hourOfDay - 12}:0${minute}"
                } else {
                    "${hourOfDay - 12}:${minute}"
                }
            }
            hourOfDay == 12 -> {
                if (minute < 10) {
                    "${hourOfDay}:0${minute}"
                } else {
                    "${hourOfDay}:${minute}"
                }
            }
            else -> {
                if (minute < 10) {
                    "${hourOfDay}:${minute}"
                } else {
                    "${hourOfDay}:${minute}"
                }
            }
        }
    }
    private fun openImagePicker () {
        ImagePicker.with(this)
            .crop()	    			//Crop image(Optional), Check Customization for more option
            .compress(1024) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {

                val filePath:String = ImagePicker.getFilePath(data)!!
                val file:File = ImagePicker.getFile(data)!!
                postJobViewModel.uploadFile(file.name)
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    }
}