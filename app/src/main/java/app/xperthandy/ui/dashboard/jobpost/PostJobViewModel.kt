package app.xperthandy.ui.dashboard.jobpost

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.domain.customer.PostJobUseCase
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.model.customer.job.PostJobPayload
import app.xperthandy.utils.Event
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import kotlinx.coroutines.launch

class PostJobViewModel @ViewModelInject constructor(
    private val postJobUseCase: PostJobUseCase
): BaseViewModel() {
    val jobTitle = MutableLiveData<String>()
    val jobDescription = MutableLiveData<String>()
    val categoryId = MutableLiveData<Int?>()
    val subCategoryId = MutableLiveData<String>()
    private val _startDate = MutableLiveData<String>()
    val startDate : LiveData<String> get() = _startDate
    private val _endDate = MutableLiveData<String>()
    val endDate : LiveData<String> get() = _endDate
    private val _startTime = MutableLiveData<String>()
    val startTime : LiveData<String> get() = _startTime
    private val _endTime = MutableLiveData<String>()
    val endTime : LiveData<String> get() = _endTime
    private val _hours = MutableLiveData<String>().apply { value = "2" }
    val hours : LiveData<String> get() = _hours

    private val _price = MutableLiveData<String>().apply { value = "₹ 400" }
    val price : LiveData<String> get() = _price


    private val _filePath = MutableLiveData<String>().apply { value = "Maximum 5 images" }
    val filePath : LiveData<String> get() = _filePath

    private val _serviceType = MutableLiveData<Event<ServiceType>>().apply { value = Event(ServiceType.INSTORE)}
    val serviceType : LiveData<Event<ServiceType>> get() = _serviceType

    private fun postJob(){
     viewModelScope.launch {
         loading.postValue(true)
         val payload = PostJobPayload(
             serviceCategory = categoryId.value,
             title = jobTitle.value,
             description = jobDescription.value,
             startDate = _startDate.value,
             startTime = _startTime.value,
             hoursRequired = hours.value
         )
         when(val result = postJobUseCase(payload)){
             is Result.Success ->{
                 loading.postValue(false)
                 postSuccess(true)
                 postError(Status.LOCAL_MESSAGE,"Job has been posted successfully!")
             }
             is Result.Error ->{
                 loading.postValue(false)
                 postError(result.exception.code,result.exception.message)
             }
         }
     }
    }
    fun onPostJobClick(){
        if(validate()){
            postJob()
        }
    }
    fun validate():Boolean{
        var valid = true
        when {
            categoryId.value == null -> {
                valid = false
                postError(Status.LOCAL_MESSAGE, "Please select a category")
            }
            subCategoryId.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE, "Please select a sub category")
            }
            jobTitle.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE, "Please enter job title")
            }
            jobDescription.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE, "Job description cannot be empty")

            }
            startTime.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE, "Please select time")
            }
            startDate.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE, "Please select date")
            }
            hours.value.isNullOrEmpty() -> {
                valid =false
                postError(Status.LOCAL_MESSAGE, "Hours cannot be empty")
            }
        }
        return valid
    }
    fun updateCategories(catId : Int?,subCatId : String){
        categoryId.value = catId
        subCategoryId.value = subCatId
    }
    fun onStartDateUpdate(date : String){
        _startDate.value = date
    }
    fun onEndDateUpdate(date : String){
        _endDate.value = date
    }
    fun onStartTimeUpdate(time : String){
        _startTime.value = time
    }
    fun onEndTimeUpdate(time : String){
        _endTime.value = time
    }
    fun onHoursUpdate(hour : Int){

        val progress = hour*2

        _price.value = "₹ ${progress*200}"
        _hours.value = "$progress"
    }


    fun locationClick () {
        _serviceType.value = Event(ServiceType.LOCATION)
    }

    fun inStoreClick () {
        _serviceType.value = Event(ServiceType.INSTORE)

    }

    fun uploadFile(filePath: String) {
        _filePath.value = filePath
    }
}
enum class ServiceType (val value: String) {
    INSTORE (value = "INSTORE"),
    LOCATION (value = "LOCATION")
}