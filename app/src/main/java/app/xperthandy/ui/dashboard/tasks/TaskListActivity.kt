package app.xperthandy.ui.dashboard.tasks

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.MyTaskActivityBinding
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.dashboard.jobdetail.JobDetailActivity
import app.xperthandy.ui.dashboard.jobstatus.ExpertJobStatusActivity
import app.xperthandy.ui.dashboard.jobstatus.JobStatusCustomerActivity
import app.xperthandy.ui.dashboard.listener.TaskListener
import app.xperthandy.ui.registration.view.UserTpe
import app.xperthandy.utils.Constants
import app.xperthandy.utils.Constants.JOB_ID
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.dismissKeyboard
import com.michalsvec.singlerowcalendar.calendar.CalendarChangesObserver
import com.michalsvec.singlerowcalendar.calendar.CalendarViewManager
import com.michalsvec.singlerowcalendar.calendar.SingleRowCalendarAdapter
import com.michalsvec.singlerowcalendar.selection.CalendarSelectionManager
import com.michalsvec.singlerowcalendar.utils.DateUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.date_item.view.*
import kotlinx.android.synthetic.main.date_unselected.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.my_task_activity.*
import java.util.*


@AndroidEntryPoint
class TaskListActivity : AppCompatActivity() {
    private val taskListViewModel: TaskListViewModel by viewModels()
    private lateinit var binding: MyTaskActivityBinding

    private val calendar = Calendar.getInstance()
    private var currentMonth = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.my_task_activity)
        binding.viewModel = taskListViewModel
        binding.lifecycleOwner = this

        binding.userContainer.visibility = View.GONE

        var title = "My Tasks"
        if (intent.hasExtra(Constants.DATA)) {
            title = "Job"
            taskListViewModel.isCustomer.value = true
        }
        if (intent.hasExtra(Constants.JOB_STATUS)) {
            taskListViewModel.selectedStatus(intent.getStringExtra(Constants.JOB_STATUS))
        }

        binding.svSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                binding.svSearchView.dismissKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    taskListViewModel.doSearch(it)
                }
                return true
            }

        })


        binding.apply {
            textScreenTitle.text = title
            imageBackButton.setOnClickListener { onBackPressed() }

//            dateList.adapter = DateListAdatpter(this@TaskListActivity)
//            dateList.adapter?.notifyDataSetChanged()

        }
        taskListViewModel.select.observe(this, EventObserver {
            Log.i("JOB_ID", "EventObserver: $it")
            val intent = Intent(this, JobDetailActivity::class.java)
            val bundle = bundleOf(JOB_ID to it)
            intent.putExtras(bundle)
            startActivity(intent)
        })
        taskListViewModel.jobStatus.observe(this, EventObserver {
            val intent =
                if (AppPreferences.userType == UserTpe.PROVIDER.value) {
                    Intent(this, ExpertJobStatusActivity::class.java)
                } else {
                    Intent(this, JobStatusCustomerActivity::class.java)
                }

            val bundle = bundleOf(JOB_ID to it)
            intent.putExtras(bundle)
            startActivity(intent)
        })

        calendar.time = Date()
        currentMonth = calendar[Calendar.MONTH]
        val myCalendarViewManager = object :
            CalendarViewManager {
            override fun setCalendarViewResourceId(
                position: Int,
                date: Date,
                isSelected: Boolean
            ): Int {
                // set date to calendar according to position where we are
                val cal = Calendar.getInstance()
                cal.time = date
                // if item is selected we return this layout items
                // in this example. monday, wednesday and friday will have special item views and other days
                // will be using basic item view
                return if (isSelected)
                    R.layout.date_item
                else R.layout.date_unselected

                // NOTE: if we don't want to do it this way, we can simply change color of background
                // in bindDataToCalendarView method
            }

            override fun bindDataToCalendarView(
                holder: SingleRowCalendarAdapter.CalendarViewHolder,
                date: Date,
                position: Int,
                isSelected: Boolean
            ) {
                // using this method we can bind data to calendar view
                // good practice is if all views in layout have same IDs in all item views
                holder.itemView.textDay.text = DateUtils.getDayNumber(date)
                holder.itemView.textDate.text = DateUtils.getDay3LettersName(date)

            }
        }

        // using calendar changes observer we can track changes in calendar
        val myCalendarChangesObserver = object :
            CalendarChangesObserver {
            // you can override more methods, in this example we need only this one
            override fun whenSelectionChanged(isSelected: Boolean, position: Int, date: Date) {
                textDay.text = "${DateUtils.getMonthName(date)}, ${DateUtils.getDayNumber(date)} "
                textDate.text = DateUtils.getDayName(date)

                taskListViewModel.dateChanged (date)

                super.whenSelectionChanged(isSelected, position, date)
            }


        }

        // selection manager is responsible for managing selection
        val mySelectionManager = object : CalendarSelectionManager {
            override fun canBeItemSelected(position: Int, date: Date): Boolean {
                // set date to calendar according to position
                val cal = Calendar.getInstance()
                cal.time = date
                // in this example sunday and saturday can't be selected, others can
                return when (cal[Calendar.DAY_OF_WEEK]) {
//                    Calendar.SATURDAY -> false
//                    Calendar.SUNDAY -> false
                    else -> true
                }
            }
        }

        // here we init our calendar, also you can set more properties if you haven't specified in XML layout
        val singleRowCalendar = dateList.apply {
            calendarViewManager = myCalendarViewManager
            calendarChangesObserver = myCalendarChangesObserver
            calendarSelectionManager = mySelectionManager
            setDates(getFutureDatesOfCurrentMonth())
            init()
        }


    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra(Constants.DATA)) {
            taskListViewModel.getMyJobs()
        } else {
            taskListViewModel.getAllJobs()
        }

    }
    private fun getFutureDatesOfCurrentMonth(): List<Date> {
        // get all next dates of current month
        currentMonth = calendar[Calendar.MONTH]
        return getDates(mutableListOf())
    }

    private fun getDates(list: MutableList<Date>): List<Date> {
        // load dates of whole month
        calendar.set(Calendar.MONTH, currentMonth)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        list.add(calendar.time)
        while (currentMonth == calendar[Calendar.MONTH]) {
            calendar.add(Calendar.DATE, +1)
            if (calendar[Calendar.MONTH] == currentMonth)
                list.add(calendar.time)
        }
        calendar.add(Calendar.DATE, -1)
        return list
    }

}
@BindingAdapter(value = ["taskList","taskListener"],requireAll = false)
fun setTaskList(
    recyclerView: RecyclerView,
    list: LiveData<List<JobListAllResItem>>,
    listener : TaskListener
){
    if(recyclerView.adapter == null){
        recyclerView.adapter = TasksLisAdapter(listener)
    }
    (recyclerView.adapter as TasksLisAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}