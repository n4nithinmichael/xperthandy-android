package app.xperthandy.ui.dashboard.bank

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.model.bankAccount.AddBankAccountPayload
import app.xperthandy.domain.bankAccount.AddBankAccountUseCase
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import kotlinx.coroutines.launch

class AddBankViewModel @ViewModelInject constructor(
    private val addBankAccountUseCase: AddBankAccountUseCase
):BaseViewModel() {
    val name = MutableLiveData("")
    val ifscCode = MutableLiveData("")
    val accountNumber = MutableLiveData("")
    val confirmAccount = MutableLiveData("")
    val bankName = MutableLiveData("")
    val branchName = MutableLiveData("")
    val branchAddress = MutableLiveData("")
    val branchCity = MutableLiveData("")
    val branchState = MutableLiveData("")
    val pinCode = MutableLiveData("")
    init {
      //  addBankAccount()
    }
    private fun addBankAccount(){
      viewModelScope.launch {
          loading.postValue(true)
          val payload = AddBankAccountPayload(
              accountName = name.value?:"",
              accountNumber = accountNumber.value?:"",
              bankName = bankName.value?:"",
              branchName = branchName.value?:"",
              branchAddress = branchAddress.value?:"",
              branchCity = branchCity.value?:"",
              branchState = branchState.value?:"",
              branchPin = pinCode.value?:"",
          )
          when(val result = addBankAccountUseCase(payload)){
              is Result.Success -> {
                  loading.postValue(false)
                  postSuccess(true)
//                  Log.i("TAG", "addBankAccountSuccess: ${result.data}")
              }
              is Result.Error ->{
                  loading.postValue(false)
                  postError(result.exception.code,result.exception.message)
//                  Log.i("TAG", "addBankAccountError: ${result.exception.code} : ${result.exception.message}")
              }
          }
      }
    }
    fun onSaveClick(){
        if(validate()){
            addBankAccount()
        }
        else{
           // validation code
        }
    }
    fun validate(): Boolean{
        var valid = true
        when {
            name.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid Name")
            }
            ifscCode.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid IFSC Code")

            }
            accountNumber.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid Account Number")

            }
            confirmAccount.value.isNullOrEmpty() || confirmAccount.value != accountNumber.value -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Account Number Doesn't Match")

            }
            bankName.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid Bank Name")

            }
            branchName.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid Branch Name")

            }
            branchAddress.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid Branch Address")

            }
            branchCity.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid City")

            }
            branchState.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid State")

            }
            pinCode.value.isNullOrEmpty() -> {
                valid = false
                postError(Status.LOCAL_MESSAGE,"Invalid Pin Code")

            }
        }
        return valid
    }
}