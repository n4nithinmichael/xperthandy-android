package app.xperthandy.ui.dashboard.jobstatus

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.JobStatusScreenBinding
import app.xperthandy.ui.registration.view.UserTpe
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.JobStatus
import com.agik.AGIKSwipeButton.Controller.OnSwipeCompleteListener
import com.agik.AGIKSwipeButton.View.Swipe_Button_View
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_job_status.*
import kotlinx.android.synthetic.main.layout_toolbar.*


@AndroidEntryPoint
class ExpertJobStatusActivity : AppCompatActivity() {

    private lateinit var binding : JobStatusScreenBinding
    private val jobStatusViewModel : JobStatusViewModel by viewModels()
    private val jobId by lazy {
        intent.extras?.getInt(Constants.JOB_ID)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.job_status_screen)
        binding.viewModel = jobStatusViewModel
        binding.lifecycleOwner = this


        binding.apply {
            imageBackButton.setOnClickListener { onBackPressed() }
            jobId?.let {
                jobStatusViewModel.updateJobId(it)
            }
        }

        textScreenTitle.text = "Job Details"
        imageBackButton.setOnClickListener { onBackPressed() }

        start.setOnSwipeCompleteListener_forward_reverse(object : OnSwipeCompleteListener {
            override fun onSwipe_Forward(swipeView: Swipe_Button_View) {
                jobStatusViewModel.updateJobStatus(JobStatus.InProgress)
            }
            override fun onSwipe_Reverse(swipeView: Swipe_Button_View) {
                //inactive function
            }
        })

        btn_work_complete.setOnClickListener {
            jobStatusViewModel.updateJobStatus(JobStatus.Completed)
        }
        btn_cancel.setOnClickListener {
            jobStatusViewModel.updateJobStatus(JobStatus.Cancelled)
        }

        jobStatusViewModel.success.observe(this, EventObserver{
            jobId?.let {
                jobStatusViewModel.updateJobId(it)
            }
        })


        jobStatusViewModel.onCall.observe(this,EventObserver{
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:${it}")
            startActivity(intent)

        })

        jobStatusViewModel.onSMS.observe(this,EventObserver{
            val smsIntent = Intent(Intent.ACTION_VIEW)
            smsIntent.data = Uri.parse("sms:${it}");
            smsIntent.putExtra("sms_body", "XpertHandy Support")
            startActivity(smsIntent)

        })

        jobStatusViewModel.jobDetails.observe(this) {
            if (AppPreferences.userType == UserTpe.PROVIDER.value) {

//                rv_star_rating.visibility = View.GONE
//                tv_profession.visibility = View.GONE

                when (it.jobStatus) {

                    JobStatus.WorkAccepted.value -> {
                        start.visibility = View.VISIBLE
                        btn_work_complete.visibility = View.GONE
                        btn_cancel.visibility = View.VISIBLE
                        if (AppPreferences.userType == UserTpe.CUSTOMER.value) {
                            jobStatusViewModel.showPaymentInfo()
                        }
                    }
                    JobStatus.Accepted.value -> {
                        start.visibility = View.VISIBLE
                        btn_work_complete.visibility = View.GONE
                        btn_cancel.visibility = View.VISIBLE
                    }
                    JobStatus.Completed.value -> {
                        start.visibility = View.GONE
                        btn_work_complete.visibility = View.GONE
                        btn_cancel.visibility = View.GONE
                    }
                    JobStatus.InProgress.value -> {
                        start.visibility = View.GONE
                        btn_work_complete.visibility = View.VISIBLE
                        btn_cancel.visibility = View.GONE
                    }
                    JobStatus.Cancelled.value -> {
                        start.visibility = View.GONE
                        btn_work_complete.visibility = View.GONE
                        btn_cancel.visibility = View.GONE
                    }
                    JobStatus.Rejected.value -> {
                        start.visibility = View.GONE
                        btn_work_complete.visibility = View.GONE
                        btn_cancel.visibility = View.GONE
                    }
                }
            } else {
                start.visibility = View.GONE
                btn_work_complete.visibility = View.GONE
                btn_cancel.visibility = View.GONE
            }

        }


    }

}