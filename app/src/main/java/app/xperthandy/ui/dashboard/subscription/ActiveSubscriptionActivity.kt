package app.xperthandy.ui.dashboard.subscription

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.databinding.ActiveSubscriptionBinding
import app.xperthandy.ui.dashboard.payments.SavedCardsActivity
import app.xperthandy.ui.registration.view.subscription.SubscriptionViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.active_subscription.*
import kotlinx.android.synthetic.main.layout_toolbar.*


@AndroidEntryPoint
class ActiveSubscriptionActivity : AppCompatActivity() {

    private val viewModel : SubscriptionViewModel by viewModels()
    private lateinit var binding: ActiveSubscriptionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.active_subscription)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.subscriptionListActive()

        textScreenTitle.text = "Active Subscription"
        imageBackButton.setOnClickListener { onBackPressed() }


        upgradeSubs.setOnClickListener {
            startActivity(Intent(this, SubscriptionsActivity :: class.java))
        }

        changeCard.setOnClickListener {
            startActivity(Intent(this, SavedCardsActivity :: class.java))
        }

    }

}