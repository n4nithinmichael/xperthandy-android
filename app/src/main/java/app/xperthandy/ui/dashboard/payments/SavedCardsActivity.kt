package app.xperthandy.ui.dashboard.payments

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.xperthandy.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*


@AndroidEntryPoint
class SavedCardsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.saved_cards)
        textScreenTitle.text = "Saved cards"
        imageBackButton.setOnClickListener { onBackPressed() }



    }

}