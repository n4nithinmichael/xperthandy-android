package app.xperthandy.ui.dashboard.jobstatus

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.data.model.payment.PaymentData
import app.xperthandy.domain.generic.JobDetailsUseCase
import app.xperthandy.domain.generic.JobStatusPayload
import app.xperthandy.domain.generic.UpdateJobStatusUseCase
import app.xperthandy.domain.payment.ApplyOfferUseCase
import app.xperthandy.domain.payment.ConfirmPaymentUseCase
import app.xperthandy.domain.payment.GetOffersUseCase
import app.xperthandy.domain.payment.PaymentDetailsUseCase
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.utils.Event
import app.xperthandy.utils.JobStatus
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class JobStatusViewModel @ViewModelInject constructor(
    private val jobDetailsUseCase: JobDetailsUseCase,
    private val updateJobStatusUseCase: UpdateJobStatusUseCase,
    private val paymentDetailsUseCase: PaymentDetailsUseCase,
    private val getOffersUseCase: GetOffersUseCase,
    private val applyOfferUseCase: ApplyOfferUseCase,
    private val confirmPaymentUseCase: ConfirmPaymentUseCase,
) : BaseViewModel(), OfferListener {

    val completed = MutableLiveData<Boolean>()

    val jobDetails = MutableLiveData<JobListAllResItem>()
    val paymentInfo = MutableLiveData<PaymentData>()
    val offerData = MutableLiveData<List<OfferData>>()
    val selectedOffer = MutableLiveData<Event<OfferData>>()

    val _submit = MutableLiveData<Event<Unit>>()
    var submit : LiveData<Event<Unit>> = _submit

    val _onCall = MutableLiveData<Event<String>>()
    var onCall : LiveData<Event<String>> = _onCall

    val _onSMS = MutableLiveData<Event<String>>()
    var onSMS : LiveData<Event<String>> = _onSMS

    private fun getJobDetails(id: Int) {
        viewModelScope.launch {
            loading.postValue(true)
            when (val result = jobDetailsUseCase(id)) {
                is Result.Success -> {
                    loading.postValue(false)
                    jobDetails.value = result.data
                }
                is Result.Error -> {
                    loading.postValue(false)
                    postError(result.exception.code, result.exception.message)
                }
            }
        }
        getOffers()
    }

    fun updateJobStatus(status: JobStatus) {

        viewModelScope.launch {
            loading.postValue(true)
            when (val result = updateJobStatusUseCase(
                Pair(
                    JobStatusPayload(jobStatus = status.value),
                    jobDetails.value?.id ?: 0
                )
            )) {
                is Result.Success -> {
                    loading.postValue(false)
                    postSuccess(true)
                }
                is Result.Error -> {
                    loading.postValue(false)
                    postError(result.exception.code, result.exception.message)
                }
            }
        }
    }

    fun updateJobId(id: Int) {
        getJobDetails(id)
    }

    fun showPaymentInfo() {
        jobDetails.value?.let {
            jobPaymentDetails(it.id.toString())
        }
    }

    private fun jobPaymentDetails(id: String) {
        viewModelScope.launch {
            loading.postValue(true)
            when (val result = paymentDetailsUseCase(id)) {
                is Result.Success -> {
                    result.data?.let {
                        paymentInfo.value = it
                    }
                    loading.postValue(false)
                }
                is Result.Error -> {
                    loading.postValue(false)
                    postError(result.exception.code, result.exception.message)
                }
            }
        }
    }

    private fun getOffers() {
        viewModelScope.launch {
            loading.postValue(true)
            when (val result = getOffersUseCase(Unit)) {
                is Result.Success -> {
                    loading.postValue(false)
                    result.data?.let {
                        offerData.value = it
                    }

                }
                is Result.Error -> {
                    loading.postValue(false)
                    postError(result.exception.code, result.exception.message)
                }
            }
        }
    }

    override fun offerItemClick(item: OfferData) {
        if (!offerData.value.isNullOrEmpty()) {

            var list = offerData.value
            list?.forEach {
                if (it.id == item.id) {
                    it.selected = true
                } else {
                    it.selected = false
                }
            }
            offerData.value = list
        }
        selectedOffer.value = Event(item)
    }

    fun applyVoucher() {

        selectedOffer.value?.let {
            applyValidVoucher(it.peekContent())
        } ?: kotlin.run {
            postError(Status.LOCAL_MESSAGE, "Please select a voucher")
        }

    }

    private fun applyValidVoucher(peekContent: OfferData) {

        var jsonObject = JsonObject()
        jsonObject.addProperty("voucher", peekContent.offerCode)
        viewModelScope.launch {
            loading.postValue(true)
            when (val result = applyOfferUseCase(Pair(jsonObject, peekContent.id.toString()))) {
                is Result.Success -> {
                    loading.postValue(false)
                    result.data?.let {
                        paymentInfo.value = it
                    }

                }
                is Result.Error -> {
                    loading.postValue(false)
                    postError(result.exception.code, result.exception.message)
                }
            }
        }

    }

    fun payNow() {
        _submit.value = Event(Unit)
    }
    fun onCall(phone:String) {
        _onCall.value = Event(phone)
    }

    fun onSms(sms:String) {
        _onSMS.value = Event(sms)
    }

}