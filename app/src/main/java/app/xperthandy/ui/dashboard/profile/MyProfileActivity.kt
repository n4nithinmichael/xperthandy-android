package app.xperthandy.ui.dashboard.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.databinding.MyProfileActivityBinding
import app.xperthandy.ui.dashboard.ChangePasswordActivity
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.Status
import app.xperthandy.utils.messageHandle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.dhaval2404.imagepicker.ImagePicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.my_profile_activity.*
import java.io.File

@AndroidEntryPoint
class MyProfileActivity : AppCompatActivity() {

    private lateinit var binding : MyProfileActivityBinding
    private  val viewModel : ProfileViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.my_profile_activity)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        viewModel.success.observe(this, EventObserver {

        })
        viewModel.error.observe(this, EventObserver {
            messageHandle(Status.LOCAL_MESSAGE,it.second)
        })

        textScreenTitle.text = "My Profile"
        imageBackButton.setOnClickListener { onBackPressed() }

        img_profile_avatar.setOnClickListener { openImagePicker() }
        change_password.setOnClickListener {
            startActivity(Intent(this, ChangePasswordActivity ::class.java))
        }
    }

    private fun openImagePicker () {
        ImagePicker.with(this)
            .crop()	    			//Crop image(Optional), Check Customization for more option
            .compress(1024) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {

                ImagePicker.getFilePath(data)?.let {
                    Glide.with(view.context)
                            .load(File(it))
                            .placeholder(R.drawable.worker_avatar)
                            .apply(RequestOptions().circleCrop())
                            .dontAnimate()
                            .into(img_profile_avatar)
                }
                val filePath:String = ImagePicker.getFilePath(data)!!
                viewModel.uploadFile(filePath)
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    }
}