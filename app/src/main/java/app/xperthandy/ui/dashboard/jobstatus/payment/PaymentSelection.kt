package app.xperthandy.ui.dashboard.jobstatus.payment

import app.xperthandy.ui.dashboard.jobstatus.payment.PaymentType

interface PaymentSelection {
    fun paymentItemClick(item: PaymentType)
}