package app.xperthandy.ui.dashboard.tasks

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.domain.customer.JobListUseCase
import app.xperthandy.domain.serviceProvider.JobListAllUseCase
import app.xperthandy.model.serviceProvider.JobListAllRes
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.dashboard.listener.TaskListener
import app.xperthandy.utils.Event
import app.xperthandy.utils.JobStatus
import app.xperthandy.utils.Result
import app.xperthandy.utils.Util
import kotlinx.coroutines.launch
import java.util.*

class TaskListViewModel @ViewModelInject constructor(
    private val jobListAllUseCase: JobListAllUseCase,
    private val myJobListUseCase: JobListUseCase,

    ): BaseViewModel(),TaskListener{

    val jobListAll = MutableLiveData<List<JobListAllResItem>>()
    val jobFilterList = MutableLiveData<List<JobListAllResItem>>()
    val jobList = MutableLiveData<List<JobListAllResItem>>()

    private val _select = MutableLiveData<Event<Int>>()
    val select get() = _select

    private val _jobStatus = MutableLiveData<Event<Int>>()
    val jobStatus get() = _jobStatus

     val newCountValue = MutableLiveData<String>()
    val pendingCountValue = MutableLiveData<String>()
    val completedCountValue = MutableLiveData<String>()
    val rejectedCountValue = MutableLiveData<String>()

    val selectedStatus = MutableLiveData<JobStatus>()
    val isCustomer = MutableLiveData<Boolean>().apply { value = false }

    init {
//        getAllJobs()
    }
    fun getAllJobs(){
        viewModelScope.launch {
            loading.postValue(true)
            when(val result = jobListAllUseCase(Unit)){
             is Result.Success ->{
                 loading.postValue(false)
//                 jobList.value = result.data
                 jobListAll.value = result.data
                 updateCount (result.data)

                 selectedStatus.value?.let { filter(it) }

             }
             is Result.Error ->{
                 loading.postValue(false)
                 updateError(Event(Pair(result.exception.code,result.exception.message)))
             }
            }
        }
    }

    fun getMyJobs(){
        viewModelScope.launch {
            loading.postValue(true)
            when(val result = myJobListUseCase(Unit)){
                is Result.Success ->{
                    loading.postValue(false)
                    jobList.value = result.data
                    jobListAll.value = result.data
                    updateCount (result.data)

                    selectedStatus.value?.let { filter(it) }

                }
                is Result.Error ->{
                    loading.postValue(false)
                    updateError(Event(Pair(result.exception.code,result.exception.message)))
                }
                else -> {}
            }
        }
    }

    private fun updateCount(data: JobListAllRes?) {

        var newCount = 0
        var pendingCount = 0
        var completedCount = 0
        var rejectedCount = 0

        data?.forEach {

            when (it.jobStatus) {
                JobStatus.New.value -> {
                    newCount += 1
                }
                JobStatus.WorkAccepted.value -> {
                    pendingCount +=1
                }
                JobStatus.Accepted.value -> {
                    pendingCount +=1
                }
                JobStatus.Completed.value -> {
                    completedCount +=1
                }
                JobStatus.InProgress.value -> {
                    pendingCount +=1
                }
                JobStatus.Cancelled.value -> {
                    rejectedCount +=1
                }
                JobStatus.Rejected.value -> {
                    rejectedCount +=1
                }
            }

        }
        newCountValue.postValue(newCount.toString())
        pendingCountValue.postValue(pendingCount.toString())
        completedCountValue.postValue(completedCount.toString())
        rejectedCountValue.postValue(rejectedCount.toString())

    }

    override fun onTaskSelect(item: JobListAllResItem) {
        when(item.jobStatus){
            JobStatus.New.value -> {
                _select.value = Event(item.id)
            }
            else -> {
                _jobStatus.value = Event(item.id)
            }
        }

    }

    fun filter (status : JobStatus) {
        selectedStatus.value = status
        var jobListFilter = mutableListOf<JobListAllResItem>()
        jobListAll.value?.forEach {

            if (status.value == JobStatus.InProgress.value){
                if (it.jobStatus == JobStatus.InProgress.value || it.jobStatus == JobStatus.WorkAccepted.value || it.jobStatus == JobStatus.Accepted.value)
                jobListFilter.add(it)
            }
            else if (status.value == JobStatus.ALL.value){
                jobListFilter.add(it)
            }
            else if (it.jobStatus == status.value){
                jobListFilter.add(it)
            }

        }
        jobFilterList.postValue(jobListFilter)
        jobList.postValue(jobListFilter)
    }

    fun selectedStatus(stringExtra: String?) {

        stringExtra?.let {
            selectedStatus.value = valueOf(stringExtra)
        }


    }
    private fun valueOf(value: String): JobStatus? = JobStatus.values().find { it.value == value }

    fun dateChanged(date: Date) {

        var jobListDate = mutableListOf<JobListAllResItem>()
        var formattedDate = Util.apiDateFormatter().format(date.time)
        jobFilterList.value?.forEach {
            if (it.formattedDate() == formattedDate)
                jobListDate.add(it)
        }
        jobList.value = jobListDate

    }
    fun doSearch(data: String) {
        val list = jobListAll.value?.filter { it.title.toLowerCase(Locale.ROOT).contains(data.toLowerCase()) == true }
        jobList.postValue(list)
    }

}

