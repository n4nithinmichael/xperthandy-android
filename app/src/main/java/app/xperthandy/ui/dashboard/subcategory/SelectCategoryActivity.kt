package app.xperthandy.ui.dashboard.subcategory

import android.content.Intent
import android.os.Bundle
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.databinding.SelectCategoryActivityBinding
import app.xperthandy.ui.dashboard.jobpost.PostJobActivity
import app.xperthandy.model.SideMenuData
import app.xperthandy.ui.dashboard.viewmodel.DashBoardViewModel
import app.xperthandy.ui.registration.view.category.WorkAreaViewModel
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class SelectCategoryActivity : AppCompatActivity() {

    private val viewModel : DashBoardViewModel by viewModels()
    private val viewModelWorkArea : WorkAreaViewModel by viewModels()

    private lateinit var binding : SelectCategoryActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.select_category_activity
        )
        binding.viewModel = viewModelWorkArea
        binding.lifecycleOwner = this
        viewModel.sideMenuList.value = fetchSideMenu()

        textScreenTitle.text = "Post a Job"
        imageBackButton.setOnClickListener { onBackPressed() }

        binding.btnNext.setOnClickListener {
            startActivity(Intent(this, PostJobActivity::class.java))
        }
        viewModelWorkArea.selectedJobPost.observe(this, EventObserver{
            val intent = Intent(this, SubCategoryListActivity::class.java)
            val bundle = bundleOf(Constants.DATA to it )
            intent.putExtras(bundle)
            startActivity(intent)
        })

        binding.svSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
//                binding.svSearchView.dismissKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    viewModelWorkArea.doSearch(it)
                }
                return true
            }

        })

        fetServiceCategory()
    }

    private fun fetServiceCategory (){
        viewModelWorkArea.fetchCategory()
    }

    private fun fetchSideMenu () : List<SideMenuData> {

        var sideMenuList = mutableListOf<SideMenuData>()

        sideMenuList.add(
            SideMenuData(
                id = 1,
                title = "Dashboard",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_dashboard,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 2,
                title = "My profile",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_profile,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 3,
                title = "Tasks",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_tasks,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 4,
                title = "Payments",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_payments,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 5,
                title = "Notifications",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_notifications,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 6,
                title = "Contact Us",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_contactus,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 7,
                title = "Help",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_help,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 8,
                title = "Logout",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_logout,
                type = 0
            )
        )


        return sideMenuList

    }

}
