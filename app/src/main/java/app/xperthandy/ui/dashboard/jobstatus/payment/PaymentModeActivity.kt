package app.xperthandy.ui.dashboard.jobstatus.payment

import android.content.Intent
import android.os.Bundle
import android.widget.RadioGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.data.model.payment.SelectedPayment
import app.xperthandy.databinding.PaymentModSelectionBinding
import app.xperthandy.ui.dashboard.CustomerDashboardActivity
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.messageHandle
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.json.JSONObject


@AndroidEntryPoint
class PaymentModeActivity : AppCompatActivity(), PaymentResultListener {

    private lateinit var binding : PaymentModSelectionBinding
    private val paymentViewModel : PaymentModeViewModel by viewModels()
    private val jobId by lazy {
        intent.extras?.getInt(Constants.JOB_ID)
    }
    private val amount by lazy {
        intent.extras?.getString(Constants.DATA)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.payment_mod_selection)
        binding.viewModel = paymentViewModel
        binding.lifecycleOwner = this


        binding.apply {
            imageBackButton.setOnClickListener { onBackPressed() }
            jobId?.let {
                paymentViewModel.updateJobId(it)
            }
            amount?.let {
                paymentViewModel.totalAmount.value = it
                paymentViewModel.paymentItemClick(PaymentType.CASH)

            }
        }

        paymentViewModel.error.observe(this, EventObserver {
            messageHandle(it.first, it.second)
        })

        paymentViewModel.onSubmit.observe(this, EventObserver{

            when(it) {
                is SelectedPayment -> {
                    startPayment(it)
                }
                is Unit -> {
                    startActivity(Intent(this, CustomerDashboardActivity :: class.java))
                    finish()
                }
            }

        })

        textScreenTitle.text = "Select Payment Type"
        imageBackButton.setOnClickListener { onBackPressed() }

    }

    private fun startPayment(subscriptionData: SelectedPayment) {
        /*
        *  You need to pass current activity in order to let Razorpay create CheckoutActivity
        * */

        val co = Checkout()
        co.setKeyID("rzp_test_AekvglK3ygDvVH")

        try {
            val options = JSONObject()
            options.put("name",subscriptionData.user?.name)
            options.put("description",subscriptionData.user?.email)
            options.put("image",subscriptionData.user?.profileImage)
            options.put("theme.color","");
            options.put("currency","INR");
            options.put("order_id", subscriptionData.paymentGatewayOrderId);
            options.put("amount",subscriptionData.total)
            val prefill = JSONObject()
            prefill.put("email",subscriptionData.user?.email)
            options.put("prefill",prefill)
            co.open(this,options)


        }catch (e: Exception){
            Toast.makeText(this,"Error in payment: "+ e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errorCode: Int, response: String?) {
        println("ERROR:$response")
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        println("SUCCESS:$razorpayPaymentId")
        paymentViewModel.confirmPayment(razorpayPaymentId?:"")
    }

}

@BindingAdapter("paymentSelection")
fun paymentSelection(view: RadioGroup, listener: PaymentSelection) {

    view.setOnCheckedChangeListener { _, i ->
        when (i) {
            R.id.radioButtonCash-> listener.paymentItemClick(PaymentType.CASH)
            R.id.radioButtonCard -> listener.paymentItemClick(PaymentType.CARD)
            R.id.radioButtonGpay -> listener.paymentItemClick(PaymentType.CASH)
        }
    }
}