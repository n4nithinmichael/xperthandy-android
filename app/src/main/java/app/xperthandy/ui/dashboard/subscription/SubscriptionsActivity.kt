package app.xperthandy.ui.dashboard.subscription

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.databinding.SubscriptionPlanBinding
import app.xperthandy.ui.registration.view.subscription.SubscriptionDetailActivity
import app.xperthandy.ui.registration.view.subscription.SubscriptionViewModel
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class SubscriptionsActivity : AppCompatActivity() {

    private val viewModel : SubscriptionViewModel by viewModels()
    private lateinit var binding: SubscriptionPlanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.subscription_plan)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        textScreenTitle.text = "Subscriptions"
        imageBackButton.setOnClickListener { onBackPressed() }

        viewModel.subscriptionList()
        viewModel.submit.observe(this, EventObserver {
            var detailIntent = Intent(this, SubscriptionDetailActivity::class.java)
            detailIntent.putExtra("subsId",it.id)
            startActivityForResult(detailIntent,103)

//            findNavController().navigate(ChooseSubscriptionFragmentDirections.openDetails(subsId = it.id))
        })

    }
}