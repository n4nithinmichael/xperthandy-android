package app.xperthandy.ui.dashboard.bank

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import app.xperthandy.R
import app.xperthandy.databinding.ActivityAddBankBinding
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.Status
import app.xperthandy.utils.messageHandle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class AddBankActivity : AppCompatActivity() {
    private val viewModel : AddBankViewModel by viewModels()
    private lateinit var binding : ActivityAddBankBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_add_bank)
        binding.apply {
            viewModel = this@AddBankActivity.viewModel
            lifecycleOwner = this@AddBankActivity
            textScreenTitle.text = getString(R.string.add_bank_account)
        }

        viewModel.error.observe(this, EventObserver{
            messageHandle(Status.LOCAL_MESSAGE,it.second)
        })
        viewModel.success.observe(this, EventObserver{
            finish()
        })
    }
}