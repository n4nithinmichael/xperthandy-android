package app.xperthandy.ui.dashboard.subscription

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.xperthandy.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.subscription_detail.*

@AndroidEntryPoint
class SubscriptionsDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.subscription_detail)
        textScreenTitle.text = "Choose plan"
        imageBackButton.setOnClickListener { onBackPressed() }
        btn_post2.setOnClickListener {
            startActivity(Intent(this, SubscriptionPayActivity :: class.java))
        }

    }

}