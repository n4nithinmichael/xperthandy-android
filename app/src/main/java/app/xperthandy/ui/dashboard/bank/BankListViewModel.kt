package app.xperthandy.ui.dashboard.bank

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.model.bankAccount.BankAccountListResItem
import app.xperthandy.domain.bankAccount.BankAccountListUseCase
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.dashboard.listener.BankListener
import app.xperthandy.utils.Result
import kotlinx.coroutines.launch

class BankListViewModel @ViewModelInject constructor(
    private val bankAccountListUseCase: BankAccountListUseCase
): BaseViewModel(),BankListener {
    val list = MutableLiveData<List<BankAccountListResItem>>()


    fun getBankAccountList(){
        viewModelScope.launch {
            loading.postValue(true)
            when(val result = bankAccountListUseCase(Unit)){
                is Result.Success ->{
                    loading.postValue(false)
                    list.postValue(result.data)
                }
                is Result.Error ->{
                    loading.postValue(false)
                }
                is Result.Loading ->{
                    loading.postValue(true)
                }
            }
        }
    }

    override fun onBankClick(item: BankAccountListResItem) {

    }

}