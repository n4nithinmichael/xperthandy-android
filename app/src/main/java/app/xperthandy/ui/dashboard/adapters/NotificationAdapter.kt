package app.xperthandy.ui.dashboard.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.xperthandy.R

class NotificationAdapter(var ctx: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.layout_notification_row_item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return  5
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


    }

    class  ViewHolder(v: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(v){

    }
}