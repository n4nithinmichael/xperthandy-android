package app.xperthandy.ui.dashboard.listener

import app.xperthandy.ui.registration.model.category.SubCategoryItem

interface SubCategoryListener{
    fun onSubCategorySelect(item : SubCategoryItem)
}