package app.xperthandy.ui.dashboard.payments

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.xperthandy.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*


@AndroidEntryPoint
class PaymentCardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_card)
        textScreenTitle.text = "Payment information"
        imageBackButton.setOnClickListener { onBackPressed() }


    }

}