package app.xperthandy.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.WelcomeBackActivityBinding
import app.xperthandy.ui.dashboard.bankdetails.PaymentsActivity
import app.xperthandy.model.SideMenuData
import app.xperthandy.ui.dashboard.notification.NotificationActivity
import app.xperthandy.ui.dashboard.offers.OffersActivity
import app.xperthandy.ui.dashboard.profile.MyProfileActivity
import app.xperthandy.ui.dashboard.subcategory.SelectCategoryActivity
import app.xperthandy.ui.dashboard.subcategory.SubCategoryListActivity
import app.xperthandy.ui.dashboard.tasks.TaskListActivity
import app.xperthandy.ui.dashboard.viewmodel.DashBoardViewModel
import app.xperthandy.ui.help_contactus.ContactUsActivity
import app.xperthandy.ui.help_contactus.HelpActivity
import app.xperthandy.ui.registration.view.RegistrationActivity
import app.xperthandy.ui.registration.view.category.WorkAreaViewModel
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.drawer_left.*
import kotlinx.android.synthetic.main.welcome_back_activity.*

@AndroidEntryPoint
class CustomerDashboardActivity : AppCompatActivity() {

    private val viewModel : DashBoardViewModel by viewModels()
    private val viewModelWorkArea : WorkAreaViewModel by viewModels()

    private lateinit var binding : WelcomeBackActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.welcome_back_activity
        )
        binding.viewModel = viewModel
        binding.workAreaViewModel = viewModelWorkArea
        binding.lifecycleOwner = this

        binding.btnPost.setOnClickListener {
            //startActivity(Intent(this,SelectCategoryActivity::class.java))
           /* val intent = Intent(this, SubCategoryListActivity::class.java)
            val bundle = bundleOf(Constants.DATA to viewModelWorkArea.selectedCategory.value )
            intent.putExtras(bundle)
            startActivity(intent)*/
            startActivity(Intent(this, SelectCategoryActivity::class.java))
            finish()
        }
        binding.tvCompletedValue.setOnClickListener {
            startActivity(Intent(this, PaymentsActivity::class.java))
        }
        binding.tvTasksValue.setOnClickListener {
            var intent = Intent (this, TaskListActivity::class.java)
            intent.putExtra(Constants.DATA,true)
            startActivity(intent)
        }
        binding.tvOffers.setOnClickListener {
            startActivity(Intent(this, OffersActivity::class.java))
        }

        binding.svSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
//                binding.svSearchView.dismissKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    viewModelWorkArea.doSearch(it)
                }
                return true
            }

        })

        binding.menuIcon.setOnClickListener { openDrawer() }
        imageView2.setImageResource(R.drawable.female_user)
        drawerName.text  = viewModel.name.value
        textView3.text  = "Cochin"

        img_dp.setOnClickListener { startActivity(Intent(this, MyProfileActivity :: class.java)) }

        viewModel.sideMenuList.value = fetchSideMenu()
        viewModelWorkArea.selectedJobPost.observe(this,EventObserver{
            val intent = Intent(this, SubCategoryListActivity::class.java)
            val bundle = bundleOf(Constants.DATA to it )
            intent.putExtras(bundle)
            startActivity(intent)
        })
        fetServiceCategory()
        observeLeftMenuClick()
    }

    private fun fetServiceCategory (){
        viewModelWorkArea.fetchCategory()
    }

    private fun fetchSideMenu () : List<SideMenuData> {

        var sideMenuList = mutableListOf<SideMenuData>()

        sideMenuList.add(
            SideMenuData(
                id = 1,
                title = "Dashboard",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_dashboard,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 2,
                title = "My profile",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_profile,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 5,
                title = "Notifications",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_notifications,
                type = 0
            )
        )

        sideMenuList.add(
            SideMenuData(
                id = 6,
                title = "Contact Us",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_contactus,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 7,
                title = "Help",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_help,
                type = 0
            )
        )
        sideMenuList.add(
            SideMenuData(
                id = 8,
                title = "Logout",
                tintColor = R.color.tint_blue,
                icon = R.drawable.ic_logout_red,
                type = 0
            )
        )


        return sideMenuList

    }

    fun openDrawer() {
        binding.drawerLayout.openDrawer(GravityCompat.START)
    }
    private  fun closeDrawer() {
        binding.drawerLayout?.closeDrawers()
    }

    private fun observeLeftMenuClick() {
        viewModel.sideMenuItemClick.observe(this, Observer {

            closeDrawer()

            when (it) {
                2 -> {
                    startActivity(Intent(this, MyProfileActivity :: class.java))
                }
                5 -> {
                    startActivity(Intent(this, NotificationActivity :: class.java))
                }
                6 -> {
                    startActivity(Intent(this, ContactUsActivity :: class.java))
                }
                7 -> {
                    startActivity(Intent(this, HelpActivity :: class.java))
                }
                8 -> {
                    AppPreferences.logOut()
                    startActivity(Intent(this, RegistrationActivity :: class.java))
                }

            }

        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.profile()
    }
}
