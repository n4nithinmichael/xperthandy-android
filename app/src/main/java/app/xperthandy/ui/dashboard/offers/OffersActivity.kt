package app.xperthandy.ui.dashboard.offers

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.databinding.OffersItemBinding
import app.xperthandy.ui.dashboard.jobstatus.OfferListener
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*


@AndroidEntryPoint
class OffersActivity : AppCompatActivity() {

    private val viewModel : OffersViewModel by viewModels()
    private lateinit var binding : OffersItemBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.offers_item)
        binding.apply {
            viewModel = this@OffersActivity.viewModel
            lifecycleOwner = this@OffersActivity

        }
        viewModel.getOffers()

        viewModel.copyText.observe(this,EventObserver {
            copyText(it)
        })

        textScreenTitle.text = "My Offers"
        imageBackButton.setOnClickListener { onBackPressed() }

    }

    private fun copyText (text : String) {
        val clipboard: ClipboardManager =
            getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied Code", text)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, "Copied Code",Toast.LENGTH_SHORT).show()
    }

    private fun checkIsFirstTime() {

//        var firstRun = XHPreference(this).firstRun
//
//        val navHostFragment = navFragmentIntro as NavHostFragment
//        val graphInflater = navHostFragment.navController.navInflater
//        var navGraph = graphInflater.inflate(R.navigation.graph)
//        var navController = navHostFragment.navController
//
//        val destination = if (firstRun) R.id.homeFragment else R.id.newPrivacyPolicyFragment
//        navGraph.startDestination = destination
//        navController.graph = navGraph
    }
}
@BindingAdapter(value = ["offerList","offerListener"],requireAll = false)
fun offerList(recyclerView: RecyclerView,
              list : LiveData<List<OfferData>>,
              listener: OfferListener
){
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = MyOffersAdapter(listener)
    (recyclerView.adapter as MyOffersAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}
