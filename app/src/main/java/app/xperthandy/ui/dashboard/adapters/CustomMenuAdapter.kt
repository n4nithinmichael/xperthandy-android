package app.xperthandy.ui.dashboard.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.model.help.HelpData
import app.xperthandy.utils.ExpandableLayout
import kotlinx.android.synthetic.main.help_item.view.*


class CustomMenuAdapter() :
        RecyclerView.Adapter<CustomMenuAdapter.ViewHolder>() {
    var attributeList = mutableListOf<HelpData>()
    var expandedByDefault = true
    private var expandedPositionSet: HashSet<Int> = HashSet()
    lateinit var context: Context

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(attributeItem: HelpData, ad: CustomMenuAdapter) {

            itemView.helpTitle.text = attributeItem.title
            var attributesAdapter = AttributesAdapter(attributeItem.items)
            itemView.optionList.adapter = attributesAdapter
            itemView.expand_layout.setOnExpandListener(object :
                    ExpandableLayout.OnExpandListener {
                override fun onExpand(expanded: Boolean) {
                    if (adapterPosition == 0) {
                        ad.expandedByDefault = expanded
                    }else {
                        if (ad.expandedPositionSet.contains(position)) {
                            ad.expandedPositionSet.remove(position)
                        } else {
                            ad.expandedPositionSet.add(position)
                        }
                    }
                    if (expanded) {
                        itemView.dropDownArrow.setImageResource(R.drawable.ic_minus)
                    }else {
                        itemView.dropDownArrow.setImageResource(R.drawable.ic_plus)
                    }

                }
            })
            ad.setFirstCellExpanded(this, position)


        }
    }

    private fun setFirstCellExpanded(holder: ViewHolder, position: Int) {
        if (position == 0) {
            if (expandedByDefault) {
                holder.itemView.post {
                    holder.itemView.expand_layout.setExpand(true)
                }
            } else {
                holder.itemView.post {
                    holder.itemView.expand_layout.setExpand(false)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.help_item, parent, false)
        val vh = ViewHolder(v)
        context = parent.context
        return vh
    }

    override fun getItemCount(): Int {
        return attributeList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Add data to cells

        var attributeItem = attributeList[position]
        holder.bindView(attributeItem,this)


    }

    fun setList(attributesList: List<HelpData>) {

        attributeList?.apply {
            clear()
            addAll(attributesList)
            notifyDataSetChanged()

        }

    }
}