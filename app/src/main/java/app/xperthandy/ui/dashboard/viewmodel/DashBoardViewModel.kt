package app.xperthandy.ui.dashboard.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import app.xperthandy.data.repository.RegistrationRepository
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.domain.serviceProvider.JobListAllUseCase
import app.xperthandy.domain.serviceProvider.JobPostedNotificationUseCase
import app.xperthandy.model.serviceProvider.JobListAllRes
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.dashboard.listener.SideMenuClickListener
import app.xperthandy.ui.dashboard.listener.TaskListener
import app.xperthandy.model.SideMenuData
import app.xperthandy.ui.registration.usecase.login.GetUserUseCase
import app.xperthandy.utils.*
import kotlinx.coroutines.launch

class DashBoardViewModel @ViewModelInject constructor(
    private val registrationRepository: RegistrationRepository,
    private val networkHelper: NetworkHelper,
    private val appPreferences: AppPreferences,
    private val getProfileUseCase: GetUserUseCase,
    private val jobPostedNotificationUseCase: JobPostedNotificationUseCase,
    private val jobListAllUseCase: JobListAllUseCase

) : BaseViewModel(), SideMenuClickListener,TaskListener {


    private val _select = MutableLiveData<Event<Int>>()
    val select get() = _select

    val search = MutableLiveData<String>()

    val name = MutableLiveData<String>().apply { value = appPreferences.userName }
    val profession = MutableLiveData<String>()
    val sideMenuList = MutableLiveData<List<SideMenuData>>()
    val sideMenuItemClick = MutableLiveData<Int>()
    val jobNotificationList = MutableLiveData<List<JobListAllResItem>>()

    val userImage = MutableLiveData<String>().apply { value = "" }

    val allCountValue = MutableLiveData<String>()
    val pendingCountValue = MutableLiveData<String>()
    val completedCountValue = MutableLiveData<String>()

    init {
        jobNotification()
        getAllJobs()
    }
    fun jobNotification(){
        viewModelScope.launch {
            when(val result = jobPostedNotificationUseCase(Unit)){
                is Result.Success ->{
                    loading.postValue(true)
                    jobNotificationList.value = result.data
//                    filter(JobStatus.New)
                }
                is Result.Error ->{
                }
            }
        }
    }

    fun profile() {
        viewModelScope.launch {
            loading.value = true
            when (val result = getProfileUseCase(Unit)) {
                is Result.Success -> {

                    loading.value = false
                    result.data?.let {
                        userImage.value =it.profileImage
                        name.value = it.name
                        if (!it.category.isNullOrEmpty()) {
                            fetchCategory(it.category[0])
                        }

                    }
                }
                is Result.Error -> {
                    loading.value = true
                    postError(result.exception.code, result.exception.message)
                }
                else -> {}
            }
        }
    }

    fun getAllJobs(){
        viewModelScope.launch {
            loading.postValue(true)
            when(val result = jobListAllUseCase(Unit)){
                is Result.Success ->{
                    loading.postValue(false)
//                    jobNotificationList.value = result.data
                    updateCount(result.data)
                    filter(JobStatus.New)

                }
                is Result.Error ->{
                    loading.postValue(false)
                    updateError(Event(Pair(result.exception.code,result.exception.message)))
                }
            }
        }
    }

    private fun fetchCategory(professionId: Int) {
        viewModelScope.launch {
            loading.postValue(true)
            if (networkHelper.isNetworkConnected()) {
                registrationRepository.fetchCategory().let {
                    if (it.isSuccessful) {

                        it.body()?.data?.let { list ->
                            val value =  list.find { data -> data.id == professionId }
                            value.let { cat ->
                                profession.value = cat?.name
                            }
                        }
                    }
                    loading.postValue(false)
                }
            } else {
                loading.postValue(false)
            }
        }
    }


    private fun updateCount(data: JobListAllRes?) {

        var pendingCount = 0
        var completedCount = 0
        var rejectedCount = 0

        data?.forEach {

            when (it.jobStatus) {

                JobStatus.WorkAccepted.value -> {
                    pendingCount +=1
                }
                JobStatus.Accepted.value -> {
                    pendingCount +=1
                }
                JobStatus.Completed.value -> {
                    completedCount +=1
                }
                JobStatus.InProgress.value -> {
                    pendingCount +=1
                }
                JobStatus.Cancelled.value -> {
                    rejectedCount +=1
                }
                JobStatus.Rejected.value -> {
                    rejectedCount +=1
                }
            }

        }
        allCountValue.postValue(data?.size?.toString())
        pendingCountValue.postValue(pendingCount.toString())
        completedCountValue.postValue(completedCount.toString())

    }

    override fun onSideMenuClick(item: SideMenuData) {
        sideMenuItemClick.value = item.id
    }

    override fun onTaskSelect(item: JobListAllResItem) {
        _select.value = Event(item.id)
    }

    fun filter (status : JobStatus) {

        var jobListFilter = mutableListOf<JobListAllResItem>()

        jobNotificationList.value?.forEach {
           if (it.jobStatus == status.value){
                jobListFilter.add(it)
            }
        }
        jobNotificationList.postValue(jobListFilter)
    }

}
