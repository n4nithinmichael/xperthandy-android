package app.xperthandy.ui.dashboard.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.databinding.SidemenuItemBinding
import app.xperthandy.ui.dashboard.listener.SideMenuClickListener
import app.xperthandy.model.SideMenuData


class SideMenuListAdapter (val listener: SideMenuClickListener): ListAdapter<SideMenuData,SideMenuListAdapter.ViewHolder>(SideMenuListDiff)
{
  inner class ViewHolder(private val binding : SidemenuItemBinding) : RecyclerView.ViewHolder(binding.root)
  {
      fun bind(item: SideMenuData)
      {
          binding.apply {
              this.item = item
              this.listener = this@SideMenuListAdapter.listener
              executePendingBindings()
          }
      }
  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(SidemenuItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }
}
object SideMenuListDiff : DiffUtil.ItemCallback<SideMenuData>() {
    override fun areItemsTheSame(oldItem: SideMenuData, newItem: SideMenuData): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: SideMenuData, newItem: SideMenuData): Boolean {
        return oldItem == newItem
    }
}
