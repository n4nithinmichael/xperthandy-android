package app.xperthandy.ui.help_contactus

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.ui.dashboard.adapters.CustomMenuAdapter
import app.xperthandy.model.help.HelpData
import app.xperthandy.model.help.HelpItems
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.help_activity.*
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class HelpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.help_activity)
        textScreenTitle.text = "Help"
        imageBackButton.setOnClickListener { onBackPressed() }

        rv_help.layoutManager = LinearLayoutManager(this,
            RecyclerView.VERTICAL,false)
        var adapter =  CustomMenuAdapter()
        rv_help.adapter = adapter
        rv_help.adapter!!.notifyDataSetChanged()
        
        var helpItems = getHelpItems ()
        adapter.setList(helpItems)
    }

    private fun getHelpItems(): List<HelpData> {

        var helpDataList = mutableListOf<HelpData>()


        var helpDataItemListOne = mutableListOf<HelpItems>()
            var helItemsOne = HelpItems(title = "What you need to get started",description = resources.getString(R.string.sample_description),id = 1)
            var helItemsTwo = HelpItems(title = "Pellentesque lobortis pharetra",description = resources.getString(R.string.sample_description),id = 1)
            var helItemsThree = HelpItems(title = "hac habitasse platea",description = resources.getString(R.string.sample_description),id = 1)
        helpDataItemListOne.add(helItemsOne)
        helpDataItemListOne.add(helItemsTwo)
        helpDataItemListOne.add(helItemsThree)

        var helpDataItemListTwo = mutableListOf<HelpItems>()
            var helItemsFour = HelpItems(title = "Your information",description = resources.getString(R.string.sample_description),id = 1)
            var helItemsFive = HelpItems(title = "Account security",description = resources.getString(R.string.sample_description),id = 1)
            var helItemsSix = HelpItems(title = "How to sign in",description = resources.getString(R.string.sample_description),id = 1)
        helpDataItemListTwo.add(helItemsFour)
        helpDataItemListTwo.add(helItemsFive)
        helpDataItemListTwo.add(helItemsSix)

        var helpDataItemListThree = mutableListOf<HelpItems>()
            var helItemsSeven = HelpItems(title = "Link you bank account",description = resources.getString(R.string.sample_description),id = 1)
            var helItemsEight = HelpItems(title = "Redeem points",description = resources.getString(R.string.sample_description),id = 1)
            var helItemsNine = HelpItems(title = "Delete bank accounts",description = resources.getString(R.string.sample_description),id = 1)
        helpDataItemListThree.add(helItemsSeven)
        helpDataItemListThree.add(helItemsEight)
        helpDataItemListThree.add(helItemsNine)

        helpDataList.add(HelpData(id = 1,title = "Getting Started",items = helpDataItemListOne))
        helpDataList.add(HelpData(id = 2,title = "My Account & Login",items = helpDataItemListTwo))
        helpDataList.add(HelpData(id = 3,title = "Bank Transfer and Linkking",items = helpDataItemListThree))


        return helpDataList
    }
}