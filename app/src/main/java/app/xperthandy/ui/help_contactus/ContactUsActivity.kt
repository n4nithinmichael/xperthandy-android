package app.xperthandy.ui.help_contactus

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.xperthandy.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class ContactUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_contact_us)
        textScreenTitle.text = "Contact us"
        imageBackButton.setOnClickListener { onBackPressed() }

    }

}