package app.xperthandy.ui.welcome.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.data.repository.RegistrationRepository
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.registration.usecase.login.LoginUseCase
import app.xperthandy.ui.registration.usecase.login.RegisterUseCase
import app.xperthandy.ui.registration.usecase.login.UpdateUserUseCase
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.Event
import app.xperthandy.utils.NetworkHelper
import app.xperthandy.utils.Resource
import app.xperthandy.utils.Result
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val registrationRepository: RegistrationRepository,
    private val networkHelper: NetworkHelper,
    private val registerUseCase: RegisterUseCase,
    private val updateUserUseCase: UpdateUserUseCase,
    private val appPreference: AppPreferences,
) : BaseViewModel() {


    private val _apiIsLoading = MutableLiveData<Boolean>()
    val apiStatus: LiveData<Boolean>
        get() = _apiIsLoading

    private val _users = MutableLiveData<Event<Resource<UserData>>>()
    val users: LiveData<Event<Resource<UserData>>>
        get() = _users


//    fun fetchUsers(jsonObject: JsonObject) {
//
//        viewModelScope.launch {
//            _users.postValue(Event(Resource.loading(null)))
//            if (networkHelper.isNetworkConnected()) {
//                registrationRepository.registerUser(jsonObject).let {
//                    if (it.isSuccessful) {
//                        if (it.body()?.statusCode == 200){
//
//                            appPreference.token = it.body()?.data?.token
//                            appPreference.userName = it.body()?.data?.name
//                            appPreference.userRegPref?.apply {
//                                this.alternativeNumber = it.body()?.data?.phone?:""
//                                this.dob  = it.body()?.data?.dob?:""
//                                this.email = it.body()?.data?.email?:""
//                                this.gender = it.body()?.data?.gender?:""
//                            }
//
//                            _users.postValue(Event(Resource.success(it.body())))
//                        }else {
//                            _users.postValue(Event(Resource.error(it.body()?.message?:"", null)))
//                        }
//                    } else _users.postValue(Event(Resource.error(it.errorBody().toString(), null)))
//                }
//            } else _users.postValue(Event(Resource.error("No internet connection", null)))
//        }
//    }

    fun registerUser(jsonObject: JsonObject) {

        loading.postValue(true)
        viewModelScope.launch {
            when (val result = registerUseCase(jsonObject)) {
                is Result.Success -> {
                    result.data?.let {
                        appPreference.token = it.token
                        appPreference.userName = it.name
//                        appPreference.userRegPref?.apply {
//                            this.alternativeNumber = it.phone?:""
//                            this.dob  = it.dob?:""
//                            this.email = it.email?:""
//                            this.gender = it.gender?:""
//                        }
                        postSuccess(result.data)
                    }

                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }
    fun updateUser(request: UpdateUserRequest) {

        loading.postValue(true)
        viewModelScope.launch {
            when (val result = updateUserUseCase(request)) {
                is Result.Success -> {
                    result.data?.let {
                        appPreference.token = it.token
                        appPreference.userName = it.name

//                        appPreference.userRegPref?.apply {
//                            this.alternativeNumber = it.phone?:""
//                            this.dob  = it.dob?:""
//                            this.email = it.email?:""
//                            this.gender = it.gender?:""
//                        }
                        postSuccess(result.data)
                    }

                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

//    fun  updateUsers(request: UpdateUserRequest){
//        viewModelScope.launch {
//            _users.postValue(Event(Resource.loading(null)))
//            if(networkHelper.isNetworkConnected()){
//                registrationRepository.updateUser(request).let {
//                    if(it.isSuccessful){
//                        if (it.body()?.statusCode == 200){
//                            _users.postValue(Event(Resource.success(it.body())))
//                        }else {
//                            _users.postValue(Event(Resource.error(it.body()?.message?:"", null)))
//                        }                    }
//                    else _users.postValue(Event(Resource.error(it.errorBody().toString(),null)))
//                }
//            }else _users.postValue(Event(Resource.error("No internet connection",null)))
//        }
//    }
}