package app.xperthandy.ui.welcome.model.user

data class Role(
    val id: Int,
    val name: String,
    val permissions: List<Any>
)