package app.xperthandy.ui.welcome.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.ui.dashboard.DashBoardActivity
import app.xperthandy.ui.dashboard.CustomerDashboardActivity
import app.xperthandy.ui.registration.view.RegistrationActivity
import app.xperthandy.ui.registration.view.UserTpe

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        setUpUi()
    }

    private fun setUpUi() {
        Handler().postDelayed({
            if (AppPreferences.firstRun) {
                AppPreferences.firstRun = false
                startActivity(Intent(this, IntroActivity::class.java))
            } else {
                if (!AppPreferences.token.isNullOrEmpty() && AppPreferences.userType != 0) {
                    if (AppPreferences.userType == UserTpe.CUSTOMER.value) {
                        startActivity(Intent(this, CustomerDashboardActivity::class.java))
                    } else {
                        startActivity(Intent(this, DashBoardActivity::class.java))
                    }
                } else {
                    startActivity(Intent(this, RegistrationActivity::class.java))

                }
            }
            finish()
        }, 3000)


    }
}