package app.xperthandy.ui.welcome.model.user
import com.google.gson.annotations.SerializedName



data class UserInfoData(
    @SerializedName("area")
    val area: String?,
    @SerializedName("district")
    val district: String?,
    @SerializedName("dob")
    val dob: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("house_name")
    val houseName: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("phone_alternate")
    val phoneAlternate: String?,
    @SerializedName("pin")
    val pin: String?,
    @SerializedName("profile_image")
    val profileImage: String?,
    @SerializedName("roles")
    val roles: List<Role>?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("to_be_logged_out")
    val toBeLoggedOut: Boolean?,
    @SerializedName("town")
    val town: String?,
    @SerializedName("otp")
    val otp: String?,
    @SerializedName("token")
    val token: String?
)