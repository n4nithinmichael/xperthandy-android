package app.xperthandy.ui.welcome.model.user

data class UserData(
    val `data`: UserInfoData,
    val message: String,
    val statusCode: Int
)