package app.xperthandy.ui.welcome.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.findNavController
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import kotlinx.android.synthetic.main.fragment_intro.*

class IntroFragment : Fragment () {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
    }

    private fun setupView() {
        tv_skip.setOnClickListener {
            val action =
                IntroFragmentDirections
                    .openRegistration()
            findNavController(this).navigate(action)
        }
        img_right_arrow.setOnClickListener {
            val action =
                IntroFragmentDirections
                    .openRegistration()
            findNavController(this).navigate(action)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_intro,container,false)
    }


}