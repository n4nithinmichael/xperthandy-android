package app.xperthandy.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.xperthandy.utils.Event
import app.xperthandy.utils.Status

open class BaseViewModel : ViewModel() {

    val loading = MutableLiveData<Boolean>()

    val noData = MutableLiveData<Boolean>()

    private val _error = MutableLiveData<Event<Pair<Status, String?>>>()
    val error: LiveData<Event<Pair<Status, String?>>> get() = _error


    private val _success = MutableLiveData<Event<Any>>()
    val success: LiveData<Event<Any>> get() = _success

    private val _exit = MutableLiveData<Event<Any>>()
    val exit: LiveData<Event<Any>> get() = _exit

    protected fun updateError(event: Event<Pair<Status, String?>>) {
        _error.postValue(event)
    }

    protected fun postError(status:Status,message:String?) {
        _error.postValue(Event(Pair(status,message)))
    }

    protected fun postSuccess(parameter: Any){
        _success.postValue(Event(parameter))
    }
    protected fun updateSuccess(any: Any){
        _success.value = Event(any)
    }
    protected fun postExit(parameter: Any){
        _exit.postValue(Event(parameter))
    }

}