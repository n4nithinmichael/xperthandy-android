package app.xperthandy.ui.base

import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import app.xperthandy.utils.Status
import app.xperthandy.utils.dismissKeyboard
import app.xperthandy.utils.messageHandle

open class BaseFragment : Fragment() {

    fun errorNavigation(status: Status, message: String?) {
        messageHandle(status,message)
    }

    open fun showLoader(loading: Boolean) {
        if (loading) {
            view?.dismissKeyboard()
            requireActivity().window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    override fun onDestroy() {
        super.onDestroy()
        showLoader(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun showError(s: String) {
        Toast.makeText(activity, s, Toast.LENGTH_SHORT).show()
    }
}