package app.xperthandy.ui.registration.view

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import app.xperthandy.data.repository.RegistrationRepository
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.registration.listners.VideoListClickListener
import app.xperthandy.ui.registration.model.video.VideoData
import app.xperthandy.ui.registration.model.video.VideoItem
import app.xperthandy.utils.NetworkHelper
import app.xperthandy.utils.Resource
import kotlinx.coroutines.launch

class VideoViewModel @ViewModelInject constructor(
    private val registrationRepository: RegistrationRepository,
    private val networkHelper: NetworkHelper
) : BaseViewModel(),VideoListClickListener {



    private val _videoData = MutableLiveData<Resource<VideoData>>()
    val videoData: LiveData<Resource<VideoData>>
        get() = _videoData

    val videoList = MutableLiveData<List<VideoItem>>()
    val selectedYoutubeId = MutableLiveData<String>()

    fun fetchVInfoVideos() {

        viewModelScope.launch {
            loading.postValue(true)
            if (networkHelper.isNetworkConnected()) {
                registrationRepository.fetchInformativeVideo().let {
                    if (it.isSuccessful) {
                        videoList.postValue(it.body()?.data)
                    } else _videoData.postValue(Resource.error(it.errorBody().toString(), null))
                    loading.postValue(false)
                }
            } else {
                loading.postValue(false)
            }
        }
    }

    override fun onVideoClick(item: VideoItem) {
        selectedYoutubeId.value = item.url
        postSuccess(item)
    }

}