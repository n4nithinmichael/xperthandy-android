package app.xperthandy.ui.registration.model.category

data class CategoryData(
    var `data`: List<CategoryItem>,
    var message: String?,
    var statusCode: Int?
)