package app.xperthandy.ui.registration.model

import app.xperthandy.ui.registration.view.UserTpe

data class UserRegPref(
    var mobileNumber : String = "",
    var mobileOtp : String = "",
    var isMobileVerified : Boolean = false,
    var userType : Int= UserTpe.CUSTOMER.value,
    var fullName : String = "",
    var alternativeNumber : String = "",
    var email : String = "",
    var dob : String = "",
    var gender : String = "",
    var houseName : String = "",
    var area : String = "",
    var town : String = "",
    var district : String = "",
    var state : String = "",
    var pinCode : Int =0
)