package app.xperthandy.ui.registration.view.verification

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.domain.verification.RequestOtpEmailUseCase
import app.xperthandy.domain.verification.RequestOtpSmsUseCase
import app.xperthandy.domain.verification.VerifyEmailUseCase
import app.xperthandy.domain.verification.VerifyOtpUseCase
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.utils.Event
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class VerificationViewModel @ViewModelInject constructor(
    private val requestOtpSmsUseCase: RequestOtpSmsUseCase,
    private val requestOtpEmailUseCase: RequestOtpEmailUseCase,
    private val verifyOtpUseCase: VerifyOtpUseCase,
    private val verifyEmailUseCase: VerifyEmailUseCase
)
    : BaseViewModel() {

    private val _verifySms = MutableLiveData<Event<Unit>>()
    val verifySms: LiveData<Event<Unit>> get() = _verifySms

    private val _verifyEmail = MutableLiveData<Event<Unit>>()
    val verifyEmail: LiveData<Event<Unit>> get() = _verifyEmail

    var smsVerified = MutableLiveData<Boolean> ().apply { value = false }
    var emailVerified = MutableLiveData<Boolean> ().apply { value = false }

    fun smsOtp() {
        loading.postValue(true)
        viewModelScope.launch {
            when (val result = requestOtpSmsUseCase(Unit)) {
                is Result.Success -> {
                    loading.postValue(false)

                    postError(Status.LOCAL_MESSAGE, "OTP Sent successfully to the mobile \nDevOtp${result.data?.otpCode}")

                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    fun emailOtp() {
        loading.postValue(true)
        viewModelScope.launch {
            when (val result = requestOtpEmailUseCase(Unit)) {
                is Result.Success -> {
                    loading.postValue(false)
                    postError(Status.LOCAL_MESSAGE, "OTP Sent successfully to the email \nDevOtp${result.data?.otpCode}")
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    fun verifyEmail(otp:String) {
        loading.postValue(true)
        var jsonObject = JsonObject()
        jsonObject.addProperty("otp_code",otp)

        viewModelScope.launch {
            when (val result = verifyEmailUseCase(jsonObject)) {
                is Result.Success -> {
                    loading.postValue(false)
                    emailVerified.value = true
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    fun verifyOtpSms(otp:String) {
        loading.postValue(true)
        var jsonObject = JsonObject()
        jsonObject.addProperty("otp_code",otp)

        viewModelScope.launch {
            when (val result = verifyOtpUseCase(jsonObject)) {
                is Result.Success -> {
                    loading.postValue(false)
                    smsVerified.value = true

                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }


    fun verifyEmail () {
        _verifyEmail.value = Event(Unit)
    }
    fun verifySms() {
        _verifySms.value = Event(Unit)
    }


}