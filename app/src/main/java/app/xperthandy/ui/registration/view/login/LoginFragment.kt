package app.xperthandy.ui.registration.view.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.LoginFragmentBinding
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.ui.dashboard.DashBoardActivity
import app.xperthandy.ui.dashboard.CustomerDashboardActivity
import app.xperthandy.ui.registration.view.UserTpe
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.login_fragment.*

@AndroidEntryPoint
class LoginFragment : BaseFragment () {

    private val viewModel : LoginViewModel by viewModels()

    private lateinit var binding: LoginFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        binding = LoginFragmentBinding.inflate(inflater, container, false).apply {
            viewModel = this@LoginFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    private fun setupView() {

        viewModel.submit.observe(viewLifecycleOwner,EventObserver {
            viewModel.doLogin(binding.edtEmail.text.toString(),binding.edtPassword.text.toString())
        })
        viewModel.loading.observe(viewLifecycleOwner, Observer {
            showLoader(it)
        })

        viewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })

        AppPreferences.userRegPref

        viewModel.success.observe(viewLifecycleOwner, EventObserver {
            when(it){
                is UserLoginData -> {

                    if (!it.roles.isNullOrEmpty() && it.roles[0].id == UserTpe.CUSTOMER.value){
                        startActivity(Intent(activity,CustomerDashboardActivity :: class.java))
                        activity?.finish()
                    }
                    else{
                        startActivity(Intent(activity,DashBoardActivity :: class.java))
                        activity?.finish()

                    }
                }

            }
        })

        forgotContainer.setOnClickListener {
            val action =
                LoginFragmentDirections.openSignUp()
            NavHostFragment.findNavController(this).navigate(action)
        }
        tv_forgot_password.setOnClickListener {
            val action =
                LoginFragmentDirections.openForgotPassword()
            NavHostFragment.findNavController(this).navigate(action)
        }

    }
}