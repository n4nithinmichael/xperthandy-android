package app.xperthandy.ui.registration.view.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import app.xperthandy.R
import app.xperthandy.databinding.ForgotPasswordFragmentBinding
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.forgot_password_fragment.*
import kotlinx.android.synthetic.main.forgot_password_fragment.imageBackButton
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class ForgotPasswordFragment : BaseFragment () {

    private val viewModel : PasswordViewModel by viewModels()

    private lateinit var loginFragmentBinding: ForgotPasswordFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.loading.observe(viewLifecycleOwner, Observer {
            showLoader(it)
        })

        viewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })

        viewModel.success.observe(viewLifecycleOwner, EventObserver {

            findNavController().navigate(ForgotPasswordFragmentDirections.openOtpCustomer(otp = it as String))

        })

        setupView()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.forgot_password_fragment, container, false)
        loginFragmentBinding.lifecycleOwner = this
        loginFragmentBinding.viewModel = viewModel
        return loginFragmentBinding.root
    }

    private fun setupView() {

        imageBackButton.setOnClickListener { activity?.onBackPressed() }

        forgotContainer.setOnClickListener {
                                val action =
                        ForgotPasswordFragmentDirections.openLogin()
            NavHostFragment.findNavController(this).navigate(action)
        }


    }
}