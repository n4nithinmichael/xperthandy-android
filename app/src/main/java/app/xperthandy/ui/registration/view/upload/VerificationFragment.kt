package app.xperthandy.ui.registration.view.upload

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.fragment_user_type.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.verification_activity.*
import java.io.File

class VerificationFragment : Fragment () {

    private var selectedUserType :Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textScreenTitle.text = "Verification"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
        btn_next.setOnClickListener { moveToNext() }

        ll_btn_img_upload.setOnClickListener {
            openImagePicker()
        }
        ll_btn_take_pic.setOnClickListener {
            openImagePicker()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.verification_activity,container,false)
    }
    private fun moveToNext() {
       // AppPreferences.userRegPref.userType = selectedUserType
        AppPreferences.userRegPref
        val action =
            VerificationFragmentDirections.selectWorkArea()
        NavHostFragment.findNavController(this).navigate(action)
    }
    private fun openImagePicker () {
        ImagePicker.with(this)
            .crop()	    			//Crop image(Optional), Check Customization for more option
            .compress(1024) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {

                ImagePicker.getFilePath(data)?.let {

                    file_name.text = File(it).name
//
//                    Glide.with(requireActivity())
//                        .load(File(it))
//                        .placeholder(R.drawable.worker_avatar)
//                        .apply(RequestOptions().circleCrop())
//                        .dontAnimate()
//                        .into(img_profile_avatar)
                }

//                //Image Uri will not be null for RESULT_OK
//                val fileUri = data?.data
//                img_profile_avatar.setImageURI(fileUri)


                //You can get File object from intent
                //            val file:File = ImagePicker.getFile(data)!!

                //You can also get File Path from intent
                val filePath:String = ImagePicker.getFilePath(data)!!
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(activity, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(activity, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    }

}