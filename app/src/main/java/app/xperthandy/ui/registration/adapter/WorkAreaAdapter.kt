package app.xperthandy.ui.registration.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.databinding.LayoutJobRowItemBinding
import app.xperthandy.ui.registration.listners.WorkAreaClickListener
import app.xperthandy.ui.registration.model.category.CategoryItem


class WorkAreaAdapter (val listener: WorkAreaClickListener): ListAdapter<CategoryItem,WorkAreaAdapter.ViewHolder>(MerchantListDiff)
{
  inner class ViewHolder(private val binding : LayoutJobRowItemBinding) : RecyclerView.ViewHolder(binding.root
  )
  {
      fun bind(item: CategoryItem)
      {
          binding.apply {
              this.item = item
              this.listener = this@WorkAreaAdapter.listener
              executePendingBindings()
          }
      }
  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutJobRowItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }
}
object MerchantListDiff : DiffUtil.ItemCallback<CategoryItem>() {
    override fun areItemsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean {
        return oldItem == newItem
    }
}
