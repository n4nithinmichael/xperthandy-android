package app.xperthandy.ui.registration.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.FragmentVideoTutorialBinding
import app.xperthandy.ui.registration.adapter.VideoListAdapter
import app.xperthandy.ui.registration.listners.VideoListClickListener
import app.xperthandy.ui.registration.model.video.VideoItem
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.XHConstant
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_user_type.*
import kotlinx.android.synthetic.main.fragment_video_tutorial.*
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class VideoTutorialFragment : Fragment () {

    private var selectedUserType :Int = 1

    private val viewModel : VideoViewModel by viewModels()
    private lateinit var binding: FragmentVideoTutorialBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fetchVInfoVideos()

        viewModel.success.observe(viewLifecycleOwner, EventObserver {

            when (it) {

                is  VideoItem -> {
                    var moveToYoutube = Intent(activity, YoutubeActivity ::class.java)
                    moveToYoutube.putExtra(XHConstant.SELECTED_ID,it.url)
                    startActivity(moveToYoutube)
                }

            }

        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        textScreenTitle.text = "Information Videos"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
        btn_next.setOnClickListener { moveToNext() }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentVideoTutorialBinding.inflate(inflater, container, false).apply {
            viewModel = this@VideoTutorialFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }
    private fun moveToNext() {
       // AppPreferences.userRegPref.userType = selectedUserType

        if (AppPreferences.userRegPref.userType == UserTpe.PROVIDER.value ){

            val action =
                VideoTutorialFragmentDirections.openTutorial()
            NavHostFragment.findNavController(this).navigate(action)

        }else {
            val action =
                VideoTutorialFragmentDirections.completeProfile()
            NavHostFragment.findNavController(this).navigate(action)
        }

    }

    private fun switchUserType(isProvider:Int) {

        when (isProvider){

            1 -> {
                img_skilled_indicator.visibility = View.GONE
                img_customer_indicator.visibility = View.VISIBLE
                selectedUserType = 2
            }
            2 -> {
                img_skilled_indicator.visibility = View.VISIBLE
                img_customer_indicator.visibility = View.GONE
                selectedUserType = 1
            }
        }

    }

    private fun fetchVInfoVideos (){
        viewModel.fetchVInfoVideos()
    }

}

@BindingAdapter(value = ["videoList", "videoClickListener"], requireAll = false)
fun videoList(recyclerView: RecyclerView,
                    list: LiveData<List<VideoItem>>,
                    listener: VideoListClickListener
) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = VideoListAdapter(listener)
    }
    (recyclerView.adapter as VideoListAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}