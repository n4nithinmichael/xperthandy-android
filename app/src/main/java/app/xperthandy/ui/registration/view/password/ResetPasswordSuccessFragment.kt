package app.xperthandy.ui.registration.view.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.databinding.ResetPasswordSuccessBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.forgot_password_fragment.imageBackButton
import kotlinx.android.synthetic.main.reset_password_success.*

@AndroidEntryPoint
class ResetPasswordSuccessFragment : Fragment () {

//    private val mainViewModel : MainViewModel by viewModels()

    private lateinit var loginFragmentBinding: ResetPasswordSuccessBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.reset_password_success, container, false)
        loginFragmentBinding.lifecycleOwner = this
        return loginFragmentBinding.root
    }

    private fun setupView() {

        imageBackButton.setOnClickListener { activity?.onBackPressed() }

        btn_save.setOnClickListener {
                                val action =
                        ResetPasswordSuccessFragmentDirections.openLogin()
            NavHostFragment.findNavController(this).navigate(action)
        }


    }
}