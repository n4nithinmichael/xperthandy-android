package app.xperthandy.ui.registration.view.subscription

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.databinding.SubscriptionListItemBinding
import app.xperthandy.model.subscription.SubscriptionData

class SubscriptionListAdapter (val listener: SubscriptionListener): ListAdapter<SubscriptionData,SubscriptionListAdapter.ViewHolder>(AccountDetailsDiff)
{
  inner class ViewHolder(private val binding : SubscriptionListItemBinding) : RecyclerView.ViewHolder(binding.root)
  {
      fun bind(item: SubscriptionData)
      {
          binding.apply {
              this.item = item
              this.listener = this@SubscriptionListAdapter.listener
              executePendingBindings()
          }
      }
  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(SubscriptionListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }
}
object AccountDetailsDiff : DiffUtil.ItemCallback<SubscriptionData>() {
    override fun areItemsTheSame(oldItem: SubscriptionData, newItem: SubscriptionData): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: SubscriptionData, newItem: SubscriptionData): Boolean {
        return oldItem == newItem
    }
}
