package app.xperthandy.ui.registration.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import kotlinx.android.synthetic.main.fragment_user_type.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class UserTypeFragment : Fragment () {

    companion object {
        private var selectedUserType :Int = UserTpe.PROVIDER.value
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cl_worker_container.setOnClickListener {
            switchUserType(UserTpe.PROVIDER)
        }
        cl_customer_container.setOnClickListener {
            switchUserType(UserTpe.CUSTOMER)
        }
        textScreenTitle.text = "Select user type"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
        btn_create_profile.setOnClickListener {
            if (userTypeCheckBox.isChecked) {
                moveToNext()
            }else{
                Toast.makeText(activity,"Please agree the terms and conditions",Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_user_type,container,false)
    }
    private fun moveToNext() {
        AppPreferences.userRegPref.userType = selectedUserType
        val action =
            UserTypeFragmentDirections.openBasicInfo()
        NavHostFragment.findNavController(this).navigate(action)
    }

    private fun switchUserType(isProvider:UserTpe) {
        selectedUserType = isProvider.value
        when (isProvider){

            UserTpe.CUSTOMER -> {
                img_skilled_indicator.visibility = View.GONE
                img_customer_indicator.visibility = View.VISIBLE
            }
            UserTpe.PROVIDER -> {
                img_skilled_indicator.visibility = View.VISIBLE
                img_customer_indicator.visibility = View.GONE
            }
        }

    }

}

enum class UserTpe(val value:Int){
    CUSTOMER(3),
    PROVIDER(2)
}