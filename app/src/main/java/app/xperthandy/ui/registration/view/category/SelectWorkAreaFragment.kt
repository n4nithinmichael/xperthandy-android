package app.xperthandy.ui.registration.view.category

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.databinding.SelectWorkAreaActivityBinding
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_user_type.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.select_work_area_activity.*
import kotlinx.android.synthetic.main.select_work_area_activity.view.*

@AndroidEntryPoint
class SelectWorkAreaFragment : BaseFragment () {

    private var selectedUserType :Int = 1
    private var callSubmit: okhttp3.Call? = null
    var dialog: Dialog? = null

    private val viewModel : WorkAreaViewModel by viewModels()
    private lateinit var binding: SelectWorkAreaActivityBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textScreenTitle.text = "Select work area"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }

        viewModel.postJob.value = false

        viewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })

        viewModel.success.observe(viewLifecycleOwner, EventObserver {
            moveToNext()
        })

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = SelectWorkAreaActivityBinding.inflate(inflater, container, false).apply {
            viewModel = this@SelectWorkAreaFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fetServiceCategory()
    }

    private fun moveToNext() {
        viewModel.selectedCategory.value?.let {
            val action =
                SelectWorkAreaFragmentDirections.openSubscription()
            NavHostFragment.findNavController(this).navigate(action)
        }

    }


    private fun fetServiceCategory (){
        viewModel.fetchCategory()
    }

}
