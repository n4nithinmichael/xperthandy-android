package app.xperthandy.ui.registration.view.verification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import app.xperthandy.R
import app.xperthandy.databinding.PhoneEmailVerificationBinding
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.GenericKeyEvent
import app.xperthandy.utils.GenericTextWatcher
import app.xperthandy.utils.getOtpEntered
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.phone_email_verification.*
import kotlinx.android.synthetic.main.phone_email_verification.btn_verify_otp
import kotlinx.android.synthetic.main.phone_email_verification.edt_otp_four
import kotlinx.android.synthetic.main.phone_email_verification.edt_otp_one
import kotlinx.android.synthetic.main.phone_email_verification.edt_otp_three
import kotlinx.android.synthetic.main.phone_email_verification.edt_otp_two

@AndroidEntryPoint
class MobEmailVerificationFragment : BaseFragment() {

    val navArgs : MobEmailVerificationFragmentArgs by navArgs()

    private val viewModel : VerificationViewModel by viewModels()

    private lateinit var binding: PhoneEmailVerificationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

        viewModel.loading.observe(viewLifecycleOwner, Observer {
            showLoader(it)
        })

        viewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })
        viewModel.verifySms.observe(viewLifecycleOwner, EventObserver {
            var otp = getOtpEntered(edt_otp_one,edt_otp_two,edt_otp_three,edt_otp_four)
            if (!otp.isNullOrEmpty() && otp.length == 4) {
                viewModel.verifyOtpSms(otp)
            }else {
                Toast.makeText(activity,"Please enter valid OTP",Toast.LENGTH_SHORT).show()
            }
        })
        viewModel.verifyEmail.observe(viewLifecycleOwner, EventObserver {
            var otp  = getOtpEntered(edt_email_one,edt_email_two,edt_email_three,edt_email_four)
            if (!otp.isNullOrEmpty() && otp.length == 4) {
                viewModel.verifyEmail(otp)
            }else{
                Toast.makeText(activity,"Please enter valid OTP",Toast.LENGTH_SHORT).show()

            }
        })


    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.phone_email_verification, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    private fun setupView() {


        navArgs?.let { args ->
            binding.apply {
                edtEmail.setText(args.emailId)
                edtEmail.isEnabled = false

                edtMob.setText(args.mobile)
                edtMob.isEnabled=false
            }
        }

        textScreenTitle.text = "Verification"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }

        btn_verify_otp.setOnClickListener {
            if (viewModel.emailVerified.value == true && viewModel.smsVerified.value == true){
                val action = MobEmailVerificationFragmentDirections.openLocationAdd()
                NavHostFragment.findNavController(this).navigate(action)
            }else{
                Toast.makeText(activity,"Please verify Mobile and Email",Toast.LENGTH_SHORT).show()
            }

        }

        setupSmsEditTextUi()
        setupEmailEditTextUi()

    }

    private fun setupSmsEditTextUi() {
        edt_otp_one.addTextChangedListener(GenericTextWatcher(false, edt_otp_two))
        edt_otp_two.addTextChangedListener(
            GenericTextWatcher(
                false,
                edt_otp_three
            )
        )
        edt_otp_three.addTextChangedListener(
            GenericTextWatcher(
                false,
                edt_otp_four
            )
        )
        edt_otp_four.addTextChangedListener(GenericTextWatcher(true, null))

        edt_otp_one.setOnKeyListener(GenericKeyEvent(true, edt_otp_one,null))
        edt_otp_two.setOnKeyListener(GenericKeyEvent(false, edt_otp_two,edt_otp_one))
        edt_otp_three.setOnKeyListener(GenericKeyEvent(false,edt_otp_three, edt_otp_two))
        edt_otp_four.setOnKeyListener(GenericKeyEvent(false, edt_otp_four,edt_otp_three))
    }

    private fun setupEmailEditTextUi() {
        edt_email_one.addTextChangedListener(GenericTextWatcher(false, edt_email_two))
        edt_email_two.addTextChangedListener(
            GenericTextWatcher(
                false,
                edt_email_three
            )
        )
        edt_email_three.addTextChangedListener(
            GenericTextWatcher(
                false,
                edt_email_four
            )
        )
        edt_email_four.addTextChangedListener(GenericTextWatcher(true, null))

        edt_email_one.setOnKeyListener(GenericKeyEvent(true, edt_email_one,null))
        edt_email_two.setOnKeyListener(GenericKeyEvent(false, edt_email_two,edt_email_one))
        edt_email_three.setOnKeyListener(GenericKeyEvent(false, edt_email_three,edt_email_two))
        edt_email_four.setOnKeyListener(GenericKeyEvent(false, edt_email_four,edt_email_three))
    }

}