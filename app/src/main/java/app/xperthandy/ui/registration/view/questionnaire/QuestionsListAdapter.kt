package app.xperthandy.ui.registration.view.questionnaire

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.databinding.QuestionHeaderItemBinding
import app.xperthandy.model.questionnaire.QuestionnaireData


class QuestionsListAdapter (val listener: QuestionnaireListener): ListAdapter<QuestionnaireData,QuestionsListAdapter.ViewHolder>(QuestionnaireListDiff)
{
  inner class ViewHolder(private val binding : QuestionHeaderItemBinding) : RecyclerView.ViewHolder(binding.root)
  {
      fun bind(item: QuestionnaireData)
      {
          binding.apply {
              this.item = item
              this.position = (adapterPosition+1).toString()
              this.listener = this@QuestionsListAdapter.listener
              executePendingBindings()
          }
      }
  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(QuestionHeaderItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }
}
object QuestionnaireListDiff : DiffUtil.ItemCallback<QuestionnaireData>() {
    override fun areItemsTheSame(oldItem: QuestionnaireData, newItem: QuestionnaireData): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: QuestionnaireData, newItem: QuestionnaireData): Boolean {
        return oldItem == newItem
    }
}
