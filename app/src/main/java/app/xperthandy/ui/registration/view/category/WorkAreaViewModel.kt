package app.xperthandy.ui.registration.view.category

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import app.xperthandy.data.repository.RegistrationRepository
import app.xperthandy.domain.serviceProvider.SaveCategoriesUseCase
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.registration.listners.WorkAreaClickListener
import app.xperthandy.ui.registration.model.category.CategoryData
import app.xperthandy.ui.registration.model.category.CategoryItem
import app.xperthandy.utils.*
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

class WorkAreaViewModel @ViewModelInject constructor(
    private val registrationRepository: RegistrationRepository,
    var  saveCategoriesUseCase: SaveCategoriesUseCase,
    private val networkHelper: NetworkHelper
) : BaseViewModel(),WorkAreaClickListener {

    val postJob = MutableLiveData<Boolean>().apply { value = true }

    private val _serviceCategory = MutableLiveData<Resource<CategoryData>>()
    val serviceCategory: LiveData<Resource<CategoryData>>
        get() = _serviceCategory

    val categoryList = MutableLiveData<List<CategoryItem>>()
    val allCategory = mutableListOf<CategoryItem>()
    val selectedCategory = MutableLiveData<CategoryItem>()
    private val _selectedJobPost = MutableLiveData<Event<CategoryItem>>()
    val selectedJobPost : LiveData<Event<CategoryItem>> get() = _selectedJobPost
    private  val _subCategorySelected = MutableLiveData<Event<Unit>>()
    val subCategorySelected : LiveData<Event<Unit>> get() = _subCategorySelected

    fun fetchCategory() {

        viewModelScope.launch {
            loading.postValue(true)
            if (networkHelper.isNetworkConnected()) {
                registrationRepository.fetchCategory().let {
                    if (it.isSuccessful) {
                        allCategory.clear()
                        categoryList.postValue(it.body()?.data)
                        it.body()?.data?.let { it1 -> allCategory.addAll(it1) }
                    } else _serviceCategory.postValue(Resource.error(it.errorBody().toString(), null))
                    loading.postValue(false)
                }
            } else {
                loading.postValue(false)
            }
        }
    }
    fun onNext () {
        selectedCategory.value?.let {

            saveServiceCategory(it.id)

        }?: kotlin.run {
            postError(Status.LOCAL_MESSAGE,"Please select a category")
        }
    }

    private fun saveServiceCategory(id: Int) {



        viewModelScope.launch {
            var list = arrayListOf<String>()
                list.add(id.toString())

            when (val result = saveCategoriesUseCase(SaveCategoryPayload(serviceCategory = list))){
                is Result.Success -> {
                    postSuccess(true)
                }
                is Result.Error -> {
                    postError(result.exception.code,result.exception.message)
                }

            }
        }



    }

    override fun onWorkAreaClick(item: CategoryItem) {
        if(postJob.value!!){
            _selectedJobPost.value = Event(item)
        }
        else{
            selectedCategory.value = item
            var list = categoryList.value
            list?.forEach {
                if (it.id == item.id){
                    it.selected = !it.selected
                }
                else{
                    it.selected = false
                }
            }
            categoryList.value = list
        }
    }

    fun doSearch(data: String) {
       val list = allCategory.filter { it.name?.toLowerCase()?.contains(data.toLowerCase()) == true }
        categoryList.postValue(list)
    }

}

data class SaveCategoryPayload(
    @SerializedName("serviceCategory")
    var serviceCategory: ArrayList<String>
)