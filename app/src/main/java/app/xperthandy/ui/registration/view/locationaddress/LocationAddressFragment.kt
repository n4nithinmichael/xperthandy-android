package app.xperthandy.ui.registration.view.locationaddress

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import app.xperthandy.R
import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.FragmentLocationAddressBinding
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.ui.registration.view.UserTpe
import app.xperthandy.ui.registration.view.basicinfo.BasicInfoFragmentDirections
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_location_address.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.util.*

@AndroidEntryPoint
class LocationAddressFragment : BaseFragment () {

    private lateinit var binding : FragmentLocationAddressBinding
    private val mViewModel : MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setUpObserver()
        textScreenTitle.text = "Location and address"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
    }

    private fun setupView() {
        val districts = resources.getStringArray(R.array.districts)
        val adapter = ArrayAdapter(requireActivity(), R.layout.spinner_item, districts)
        binding.apply {
            spnrDistrict.adapter = adapter
            btn_next.setOnClickListener {

                if(edtHouseName.text.isNullOrEmpty() || edtHouseName.text.toString().length <3)
                {
                    showError(resources.getString(R.string.str_toast_val_house))
                }
                else if (edtArea.text.isNullOrEmpty() || edtArea.text.toString().length <3)
                {
                    showError(resources.getString(R.string.str_toast_val_area))
                }
                else if(edtTown.text.isNullOrEmpty() || edtTown.text.toString().length <3)
                {
                    showError(resources.getString(R.string.str_toast_val_town))
                }
                else if(spnrDistrict.selectedItem == null || spnrDistrict.selectedItem.toString().isNullOrEmpty() || spnrDistrict.selectedItem =="Select a District")
                {
                    showError(resources.getString(R.string.str_toast_val_district))
                }
                /* else if(edtState.text.isNullOrEmpty() || edtState.text.toString().length<3)
                {
                    showError(resources.getString(R.string.str_toast_val_house))
                }
               else if(edtPincode.text.isNullOrEmpty() || edtPincode.text.toString().length<6)
                {

                }*/
                else
                {
                    binding.apply {
                        AppPreferences.userRegPref.let {
                            it.houseName = edtHouseName.text.toString()
                            it.area = edtArea.text.toString()
                            it.town = edtTown.text.toString()
                            it.district = spnrDistrict.selectedItem.toString()
                            it.state = edtState.text.toString()
                            it.pinCode = edtPincode.text?.toString()?.toInt()?:0
//                            it.fullName = AppPreferences.userName?:""
                            /*it.dob = Util.apiDateFormatter().format(Util.displayDateFormatter().parse(it.dob)
                                ?: Date()
                            )*/
                            val request = UpdateUserRequest(area = it.area,district = it.district,dob = it.dob, gender = it.gender,houseName = it.houseName,name = it.fullName,
                            phoneAlternate = it.alternativeNumber,pin = it.pinCode.toString(),roles = it.userType,state = it.state,
                            town = it.town,email = it.email)
                                 mViewModel.updateUser(request)
//                            moveToNext()
                        }
                    }
                }


            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_location_address,container,false)
        binding.lifecycleOwner = this
        return binding.root
    }
    private fun moveToNext() {

        if (AppPreferences.userRegPref.userType == UserTpe.PROVIDER.value ){
            val action =
                LocationAddressFragmentDirections.openVerificationVideo()
            NavHostFragment.findNavController(this).navigate(action)

        }else {
            val action =
                LocationAddressFragmentDirections.openVerifyVideo()
            NavHostFragment.findNavController(this).navigate(action)
        }


    }

    private fun setUpObserver(){
        mViewModel.users.observe(viewLifecycleOwner, EventObserver {
            it?.let {
                when (it.status){

                    Status.SUCCESS -> {
                        binding.isShowLoader = false
                        moveToNext()
                    }
                    Status.LOADING -> {
                        binding.isShowLoader = true
                    }
                    Status.ERROR -> {
                        errorNavigation(Status.LOCAL_MESSAGE,it.message)
                        binding.isShowLoader = false
                    }

                }

            }
        })

        mViewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })
        mViewModel.success.observe(viewLifecycleOwner, EventObserver {
            when(it){
                is UserLoginData -> {
                    moveToNext()                }

            }
        })
    }


}