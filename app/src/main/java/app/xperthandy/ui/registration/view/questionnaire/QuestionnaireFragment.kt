package app.xperthandy.ui.registration.view.questionnaire

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import app.xperthandy.R
import app.xperthandy.databinding.FragmentQuestionnaireBinding
import app.xperthandy.databinding.QuestionnaireContainerBinding
import app.xperthandy.model.questionnaire.Answer
import app.xperthandy.model.questionnaire.QuestionnaireData
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
//import kotlinx.android.synthetic.main.fragment_questionnaire.*
import kotlinx.android.synthetic.main.layout_toolbar.*
@AndroidEntryPoint
class QuestionnaireFragment : BaseFragment() {

    private val viewModel: QuestionnaireViewModel by viewModels()
    private lateinit var binding: QuestionnaireContainerBinding
//    private lateinit var binding: FragmentQuestionnaireBinding
    private lateinit var viewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textScreenTitle.text = "Questionnaire"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
        viewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })
        viewModel.success.observe(viewLifecycleOwner, EventObserver {

            when (it) {
                is Boolean -> {
                    findNavController().navigate(QuestionnaireFragmentDirections.openQuestionnaireResult())
                }
            }

        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = QuestionnaireContainerBinding.inflate(inflater, container, false).apply {
            viewModel = this@QuestionnaireFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    private fun setupViewMode(){

    }


}

@SuppressLint("NotifyDataSetChanged")
@BindingAdapter(value = ["questionnaire", "questionnaireClick"], requireAll = true)
fun questionnaire(
    recyclerView: RecyclerView,
    list: LiveData<List<QuestionnaireData>>,
    listener: QuestionnaireListener
) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = QuestionsListAdapter(listener)
    }
    (recyclerView.adapter as QuestionsListAdapter).apply {
        submitList(list.value)
        notifyDataSetChanged()
    }
    recyclerView.scrollToPosition(0)
}

@SuppressLint("NotifyDataSetChanged")
@BindingAdapter(value = ["questionAnswer", "questionAnswerClick"], requireAll = true)
fun questionAnswer(
    recyclerView: RecyclerView,
    list: LiveData<QuestionnaireData>,
    listener: QuestionnaireListener
) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = AnswerListAdapter(listener)
    }
    (recyclerView.adapter as AnswerListAdapter).apply {
        list.value?.answerList.let {
            submitList(it)
            this.notifyDataSetChanged()
        }

    }
    recyclerView.scrollToPosition(0)
}
@BindingAdapter("setBackground")
fun setBackground(
    view: TextView,
    selected:Boolean,
) {
    if (selected){
        view.background = ContextCompat.getDrawable(view.context, R.drawable.shape_rect_corner_green_fill)
        view.setTextColor( ContextCompat.getColor(view.context, R.color.white))
    }else{
        view.background = ContextCompat.getDrawable(view.context, R.drawable.shape_rect_corner_offwhite_fill)
        view.setTextColor( ContextCompat.getColor(view.context, R.color.color_usertype_desc))

    }
}

@BindingAdapter("setQuestionBackground")
fun setQuestionBackground(
    view: TextView,
    selected:Boolean,
) {
    if (selected){
        view.background = ContextCompat.getDrawable(view.context, R.drawable.shape_circle_ques_num_bg)
    }else{
        view.background = ContextCompat.getDrawable(view.context, R.drawable.shape_circle_ques_num_bg_whte)

    }
}