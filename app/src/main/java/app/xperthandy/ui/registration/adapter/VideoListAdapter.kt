package app.xperthandy.ui.registration.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.databinding.VideoListItemBinding
import app.xperthandy.ui.registration.listners.VideoListClickListener
import app.xperthandy.ui.registration.model.video.VideoItem


class VideoListAdapter (val listener: VideoListClickListener): ListAdapter<VideoItem,VideoListAdapter.ViewHolder>(VideoListDiff)
{
  inner class ViewHolder(private val binding : VideoListItemBinding) : RecyclerView.ViewHolder(binding.root)
  {
      fun bind(item: VideoItem)
      {
          binding.apply {
              this.item = item
              this.listener = this@VideoListAdapter.listener
              executePendingBindings()
          }
      }
  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(VideoListItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }
}
object VideoListDiff : DiffUtil.ItemCallback<VideoItem>() {
    override fun areItemsTheSame(oldItem: VideoItem, newItem: VideoItem): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: VideoItem, newItem: VideoItem): Boolean {
        return oldItem == newItem
    }
}
