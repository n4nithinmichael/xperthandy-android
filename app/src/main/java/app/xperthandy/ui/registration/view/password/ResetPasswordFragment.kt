package app.xperthandy.ui.registration.view.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import app.xperthandy.R
import app.xperthandy.databinding.ResetPasswordFragmentBinding
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.reset_password_fragment.*

@AndroidEntryPoint
class ResetPasswordFragment : BaseFragment() {

    private val viewModel : PasswordViewModel by viewModels()

    private lateinit var binding: ResetPasswordFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.reset_password_fragment, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    private fun setupView() {

        viewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })

        viewModel.success.observe(viewLifecycleOwner, EventObserver {

            findNavController().navigate(ResetPasswordFragmentDirections.openResetSuccess())

        })

    }
}