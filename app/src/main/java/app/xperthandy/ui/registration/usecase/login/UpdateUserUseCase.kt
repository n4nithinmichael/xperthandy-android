package app.xperthandy.ui.registration.usecase.login

import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.data.repository.JsonRequestRepository
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.registration.model.UserRegPref
import app.xperthandy.ui.registration.usecase.UseCase
import com.airpay.shared.di.IoDispatcher
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import app.xperthandy.utils.*


class UpdateUserUseCase @Inject constructor(
    private val repository: JsonRequestRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : UseCase<UpdateUserRequest, UserLoginData?>(dispatcher) {

    override suspend fun execute(parameters: UpdateUserRequest): UserLoginData? {
        return when (val result = repository.update(parameters)) {
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw  AppException(Constants.UNKNOWN_ERROR)
        }
    }
}