package app.xperthandy.ui.registration.view

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.data.repository.preference.XHPreference
import app.xperthandy.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.intro_activity.*

@AndroidEntryPoint
class RegistrationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registartion_activity)
        hideToolBar()

    }

}