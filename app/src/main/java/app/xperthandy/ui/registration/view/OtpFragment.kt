package app.xperthandy.ui.registration.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.data.repository.preference.XHPreference
import app.xperthandy.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_otp.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class OtpFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupEditTextUi()
        textScreenTitle.text = "OTP verification"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }

        btn_verify_otp.setOnClickListener {

            if (getOtpEntered() == AppPreferences.userRegPref.mobileOtp){
                AppPreferences.userRegPref.isMobileVerified = true
                moveToNext()
            }else{
                showError(resources.getString(R.string.str_toast_val_otp))
            }
        }
    }



    private fun setupEditTextUi() {
        edt_otp_one.addTextChangedListener(GenericTextWatcher(edt_otp_one, edt_otp_two))
        edt_otp_two.addTextChangedListener(GenericTextWatcher(edt_otp_two, edt_otp_three))
        edt_otp_three.addTextChangedListener(GenericTextWatcher(edt_otp_three, edt_otp_four))
        edt_otp_four.addTextChangedListener(GenericTextWatcher(edt_otp_four, null))

        edt_otp_one.setOnKeyListener(GenericKeyEvent(edt_otp_one, null))
        edt_otp_two.setOnKeyListener(GenericKeyEvent(edt_otp_two, edt_otp_one))
        edt_otp_three.setOnKeyListener(GenericKeyEvent(edt_otp_three, edt_otp_two))
        edt_otp_four.setOnKeyListener(GenericKeyEvent(edt_otp_four,edt_otp_three))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_otp,container,false)
    }

    private fun moveToNext() {
        val action =
            OtpFragmentDirections.openUserType()
        NavHostFragment.findNavController(this).navigate(action)
    }

    class GenericKeyEvent internal constructor(private val currentView: AppCompatEditText, private val previousView: EditText?) : View.OnKeyListener{
        override fun onKey(p0: View?, keyCode: Int, event: KeyEvent?): Boolean {
            if(event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL && currentView.id != R.id.edt_otp_one && currentView.text.toString().isEmpty()) {
                //If current is empty then previous EditText's number will also be deleted
                previousView!!.text = null
                previousView.requestFocus()
                return true
            }
            return false
        }
    }

    class GenericTextWatcher internal constructor(private val currentView: View, private val nextView: View?) :
        TextWatcher {
        override fun afterTextChanged(editable: Editable) {
            val text = editable.toString()
            when (currentView.id) {
                R.id.edt_otp_one -> if (text.length == 1) nextView!!.requestFocus()
                R.id.edt_otp_two -> if (text.length == 1) nextView!!.requestFocus()
                R.id.edt_otp_three -> if (text.length == 1) nextView!!.requestFocus()
            }
        }

        override fun beforeTextChanged(
            arg0: CharSequence,
            arg1: Int,
            arg2: Int,
            arg3: Int
        ) {
        }

        override fun onTextChanged(
            arg0: CharSequence,
            arg1: Int,
            arg2: Int,
            arg3: Int
        ) {
        }

    }

    private fun getOtpEntered() : String {
        return  edt_otp_one.text.toString() +
                edt_otp_two.text.toString() +
                edt_otp_three.text.toString() +
                edt_otp_four.text.toString()
    }
}