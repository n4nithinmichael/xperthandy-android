package app.xperthandy.ui.registration.view.subscription

import app.xperthandy.model.subscription.SubscriptionData

interface SubscriptionListener {
    fun onItemClick (item: SubscriptionData)
}