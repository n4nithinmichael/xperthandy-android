package app.xperthandy.ui.registration.view.questionnaire

import app.xperthandy.model.questionnaire.Answer
import app.xperthandy.model.questionnaire.QuestionnaireData

interface QuestionnaireListener {
    fun questionSelection(item: QuestionnaireData)
    fun answerSelection(item: Answer)
}