package app.xperthandy.ui.registration.model.video

data class VideoData(
    var `data`: List<VideoItem>?,
    var message: String?,
    var statusCode: Int?
)