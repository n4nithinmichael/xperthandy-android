package app.xperthandy.ui.registration.view.questionnaire

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.domain.serviceProvider.QuestionnaireListUseCase
import app.xperthandy.domain.serviceProvider.QuestionnaireSaveAnswerUseCase
import app.xperthandy.model.questionnaire.Answer
import app.xperthandy.model.questionnaire.QuestionnaireData
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.utils.Result
import app.xperthandy.utils.Status
import kotlinx.coroutines.launch

class QuestionnaireViewModel @ViewModelInject constructor(
    private val questionnaireUseCase: QuestionnaireListUseCase,
    private val questionnaireSaveAnswerUseCase: QuestionnaireSaveAnswerUseCase,
    private val appPreference: AppPreferences,
) : BaseViewModel(),QuestionnaireListener {

    private val _questionnaireList = MutableLiveData<List<QuestionnaireData>?>()
    val questionnaireList: LiveData<List<QuestionnaireData>?> get() = _questionnaireList

    private val _questionData = MutableLiveData<QuestionnaireData?>()
    val questionData: LiveData<QuestionnaireData?> get() = _questionData

    var selectedQuestionId : String? = ""

    init {
        questionnaire()
    }

    fun questionnaire() {

        loading.postValue(true)
        viewModelScope.launch {
            when (val result = questionnaireUseCase(Unit)) {
                is Result.Success -> {
                    result.data?.let {
                        _questionnaireList.value = it
                        if (!it.isNullOrEmpty()) {
                            questionSelection(it[0])
                            selectedQuestionId = it[0].id
                        }
                    }
                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    override fun questionSelection(item: QuestionnaireData) {
        _questionData.value = item
        selectedQuestionId = item.id
        updateQuestionSelection(item)
    }

    private fun updateQuestionSelection(item: QuestionnaireData) {
        var lists = questionnaireList.value
        lists?.forEach {
            if (it.id == item.id) {
                it.apply {
                    selected = true
                }
            } else {
                it.apply {
                    selected = false
                }
            }
        }
        _questionnaireList.value = lists
    }

    override fun answerSelection(item: Answer) {
        var lists = questionData.value
        lists?.answerList?.forEach {
            if (it.id == item.id) {
                it.apply {
                    selected = true
                }
            } else {
                it.apply {
                    selected = false
                }
            }
        }
        println(lists)
        _questionData.value = lists
    }

    fun next() {
        var nextItem  = questionnaireList.value?.indexOf(questionData.value)
        nextItem?.let {
            questionnaireList.value?.getOrNull(it+1)?.let {
                _questionData.value = it
                updateQuestionSelection(it)
            }

        }?: kotlin.run {
            validateAnswer()
        }
    }

    private fun validateAnswer() {
        var answersCount = 0
        questionnaireList.value?.forEach {
            it.answerList.forEach { answer ->
                if (answer.selected){
                    answersCount += 1
                }
            }
        }
        if (answersCount==questionnaireList.value?.size){

            postSuccess(true)

        }else{
            postError(Status.LOCAL_MESSAGE, "Please answer all questions")

        }
    }

    fun back() {
        var prevItem  = questionnaireList.value?.indexOf(questionData.value)
        prevItem?.let { prev ->
            questionnaireList.value?.getOrNull(prev-1)?.let {
                _questionData.value = it
                updateQuestionSelection(it)
            }

        }
    }
}