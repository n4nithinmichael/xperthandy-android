package app.xperthandy.ui.registration.listners

import app.xperthandy.ui.registration.model.category.CategoryItem


interface WorkAreaClickListener {
    fun onWorkAreaClick(item: CategoryItem)
}