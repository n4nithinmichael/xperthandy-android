package app.xperthandy.ui.registration.view.basicinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.FragmentBasicInfoBinding
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.base.BaseFragment
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.Status
import app.xperthandy.utils.Util
import app.xperthandy.utils.Validator.Companion.isValidEmail
import app.xperthandy.utils.showDatePicker
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_basic_info.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.util.*

@AndroidEntryPoint
class BasicInfoFragment : BaseFragment () {
    private lateinit var binding: FragmentBasicInfoBinding
    private var selectedGender = "Male"
    private var apiDateValue = ""

    private val mainViewModel : MainViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        textScreenTitle.text = "Sign up"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
    }

    private fun setupView() {
        setUpObserver()
        setGenderUI(selectedGender)
        binding.apply {
            btnNext.setOnClickListener {
                if(edtFullname.text.isNullOrEmpty() || edtFullname.text.toString().length <3)
                {
                    showError(resources.getString(R.string.str_toast_val_full_name))
                }
                else if(edtAlternateNum.text.isNullOrEmpty() || edtAlternateNum.text.toString().length <10)
                {
                    showError(resources.getString(R.string.str_toast_val_number))
                }
                else if(edtEmail.text.isNullOrEmpty() || !isValidEmail(edtEmail))
                {
                    showError(resources.getString(R.string.str_toast_val_email))
                }
                else if(tvDobValue.text.isNullOrEmpty())
                {
                    showError(resources.getString(R.string.str_toast_val_dob))
                }
                else{
                    moveToNext()
                }

            }
            clDobValue.setOnClickListener {
                val calendar = Calendar.getInstance()
                val calendarStart = Calendar.getInstance()
                calendarStart.add(Calendar.YEAR, -90)
                val calendarEnd= Calendar.getInstance()
                calendarEnd.add(Calendar.YEAR,-18)
                showDatePicker(
                    calendar,
                    disableBackDateSelection = true,
                    startFrom = calendarStart.time,
                    futureDate =  calendarEnd.time
                ) { calendar ->
                    tvDobValue.text = Util.displayDateFormatter().format(calendar.time)
                    apiDateValue = Util.apiDateFormatter().format(calendar.time)
                }
            }

            clMaleContainer.setOnClickListener {
                if (selectedGender == "Female") switchUserType(selectedGender)
            }
            clFemaleContainer.setOnClickListener {
                if(selectedGender == "Male") switchUserType(selectedGender)
            }
        }

    }

    private fun switchUserType(gender: String) {
        when(gender)
        {
            "Male" ->{
                selectedGender = "Female"
                setGenderUI(selectedGender)
            }
            "Female" ->{
                selectedGender = "Male"
                setGenderUI(selectedGender)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_basic_info,container,false)
        binding.lifecycleOwner = this
        return binding.root
    }
    private fun moveToNext() {
        binding.apply {
            AppPreferences.userRegPref.let {prefs->
                prefs.fullName = edtFullname.text.toString()
                prefs.alternativeNumber = "91"+edtAlternateNum.text.toString()
                prefs.email = edtEmail.text.toString()
                prefs.dob = apiDateValue
                prefs.gender = selectedGender
            }
        }

        var jsonObject = JsonObject ()
        jsonObject.addProperty("phone","91"+binding.edtAlternateNum.text.toString())
        jsonObject.addProperty("email",binding.edtEmail.text.toString())
        jsonObject.addProperty("password",binding.edtPassword.text.toString())
        jsonObject.addProperty("password_confirm",binding.edtCnPassword.text.toString())
        jsonObject.addProperty("userType",AppPreferences.userRegPref.userType)
        jsonObject.addProperty("name",binding.edtFullname.text.toString())
        jsonObject.addProperty("dob",apiDateValue)
        jsonObject.addProperty("gender",selectedGender)
        mainViewModel.registerUser(jsonObject)

    }

    private fun setGenderUI(gender : String)
    {
        when(gender)
        {
            "Male" ->{
                binding.imgMaleIndicator.visibility = View.VISIBLE
                binding.imgFemaleIndicator.visibility = View.GONE

            }
            "Female" ->{
                binding.imgMaleIndicator.visibility = View.GONE
                binding.imgFemaleIndicator.visibility = View.VISIBLE
            }
        }
    }

    private fun setUpObserver(){
        mainViewModel.error.observe(viewLifecycleOwner, EventObserver {
            errorNavigation(it.first, it.second)
        })
        mainViewModel.success.observe(viewLifecycleOwner, EventObserver {
            when(it){
                is UserLoginData -> {
                        findNavController().navigate(BasicInfoFragmentDirections.openVeriFragment(emailId = binding.edtEmail.text.toString(),mobile = binding.edtAlternateNum.text.toString()))
                }

            }
        })
    }

}