package app.xperthandy.ui.registration.view.password

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.domain.ForgotPasswordUseCase
import app.xperthandy.domain.UpdatePasswordUseCase
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.registration.usecase.login.LoginUseCase
import app.xperthandy.utils.Event
import com.google.gson.JsonObject
import kotlinx.coroutines.launch
import app.xperthandy.utils.*

class PasswordViewModel @ViewModelInject constructor(
    private val forgotPasswordUseCase: ForgotPasswordUseCase,
    private val updatePasswordUseCase: UpdatePasswordUseCase,
    private val appPreference: AppPreferences,
) : BaseViewModel() {

    private val _submit = MutableLiveData<Event<Unit>>()
        val submit: LiveData<Event<Unit>> get() = _submit

    val email = MutableLiveData<String> ().apply { value = "" }

    val newPassword = MutableLiveData<String> ().apply { value = "" }
    val confirmPassword = MutableLiveData<String> ().apply { value = "" }
    val otp = MutableLiveData<String> ().apply { value = "" }


    fun submit(){
        _submit.postValue(Event(Unit))
    }


    fun forgotPassword() {

        if (validEmail()){
            sendOtp()
        }
    }

    fun sendOtp (){
        val request = JsonObject()
        request.addProperty("email",email.value)

        loading.postValue(true)
        viewModelScope.launch {
            when (val result = forgotPasswordUseCase(request)) {
                is Result.Success -> {
                    postError(Status.LOCAL_MESSAGE, "Password reset link sent \nDevOtpCode: ${result.data?.otpCode}")
                    result.data?.let {
                        appPreference.token = it.token
                        appPreference.userName = it.name
                    }

                    result.data?.otpCode?.let {
                        postSuccess(it)
                    }


                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }


    fun updatePassword() {

        if (!validInputs()) return

        val request = JsonObject()
        request.addProperty("password_confirm",confirmPassword.value)
        request.addProperty("password",newPassword.value)

        loading.postValue(true)
        viewModelScope.launch {
            when (val result = updatePasswordUseCase(request)) {
                is Result.Success -> {
                    postError(Status.LOCAL_MESSAGE, "Password change successfully")

                    result.data?.let {
                        appPreference.token = it.token
                        appPreference.userName = it.name
                    }

                    postSuccess(Unit)

                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    private fun validEmail() : Boolean{

        var isValid = true

        when {
            email.value.isNullOrEmpty() -> {
                postError(Status.LOCAL_MESSAGE,"Invalid email/mobile")
                isValid = false
            }
        }
        return isValid
    }

    private fun validInputs() : Boolean{

        var isValid = true

        when {
            newPassword.value.isNullOrEmpty() -> {
                postError(Status.LOCAL_MESSAGE,"Invalid new password")
                isValid = false
            }
            confirmPassword.value.isNullOrEmpty() -> {
                postError(Status.LOCAL_MESSAGE,"Invalid confirm password")
                isValid = false
            }
            newPassword.value != confirmPassword.value -> {
                postError(Status.LOCAL_MESSAGE,"Passwords doesn't match")
                isValid = false
            }
        }
        return isValid
    }

}