package app.xperthandy.ui.registration.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.FragmentMobileBinding
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.Status
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_mobile.*

@AndroidEntryPoint
class MobileFragment : Fragment () {

    private val mainViewModel : MainViewModel by viewModels()

    private lateinit var fragmentMobileBinding: FragmentMobileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        setUpObserver()
    }

    private fun setUpObserver() {


        mainViewModel.users.observe(viewLifecycleOwner, EventObserver {
            it?.let {

                when (it.status){

                    Status.SUCCESS -> {
                        fragmentMobileBinding.isShowLoader = false
//
//                        it?.data?.data?.let {
//                            AppPreferences.userRegPref?.run {
//                                mobileNumber = it.phone
//                                mobileOtp = "${it.otp}"
//                                AppPreferences.token = it.token
//                            }
//
//                            Toast.makeText(activity,"${it.otp}",Toast.LENGTH_SHORT).show()
//                        }
                        moveToNext()
                    }
                    Status.LOADING -> {
                        fragmentMobileBinding.isShowLoader = true
                    }
                    Status.ERROR -> {
                        fragmentMobileBinding.isShowLoader = false
                    }

                }

            }

        })

    }

    private fun moveToNext() {
//                    val action =
//                        MobileFragmentDirections.openOtpFragment()
//            NavHostFragment.findNavController(this).navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        fragmentMobileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_mobile, container, false)
        fragmentMobileBinding.lifecycleOwner = this
        return fragmentMobileBinding.root
    }

    private fun setupView() {
        btn_send.setOnClickListener {

            var mobileNumber = edt_mob.text.toString()

            if (mobileNumber.isNullOrEmpty()||mobileNumber.length!=10){
                edt_mob.error = "Invalid Mobile Number"
            }else {
                var jsonObject = JsonObject ()
                jsonObject.addProperty("phone","91${mobileNumber}")
//                mainViewModel.fetchUsers(jsonObject)

            }

        }

    }
}