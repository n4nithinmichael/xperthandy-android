package app.xperthandy.ui.registration.model.video

data class VideoItem(
    var description: String?,
    var id: Int?,
    var title: String?,
    var url: String?
)