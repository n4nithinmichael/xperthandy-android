package app.xperthandy.ui.registration.view.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import app.xperthandy.R
import app.xperthandy.databinding.FragmentOtpCustomerBinding
import app.xperthandy.ui.registration.view.verification.MobEmailVerificationFragmentArgs
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.GenericKeyEvent
import app.xperthandy.utils.GenericTextWatcher
import app.xperthandy.utils.getOtpEntered
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_otp_customer.*
import kotlinx.android.synthetic.main.fragment_otp_customer.btn_verify_otp
import kotlinx.android.synthetic.main.fragment_otp_customer.edt_otp_four
import kotlinx.android.synthetic.main.fragment_otp_customer.edt_otp_one
import kotlinx.android.synthetic.main.fragment_otp_customer.edt_otp_three
import kotlinx.android.synthetic.main.fragment_otp_customer.edt_otp_two
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.imageBackButton
import kotlinx.android.synthetic.main.phone_email_verification.*

@AndroidEntryPoint
class CustomerOtpFragment : Fragment () {

    private val viewModel : PasswordViewModel by viewModels()

    private lateinit var loginFragmentBinding: FragmentOtpCustomerBinding
    val navArgs : CustomerOtpFragmentArgs by navArgs()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_otp_customer, container, false)
        loginFragmentBinding.lifecycleOwner = this
        return loginFragmentBinding.root
    }

    private fun setupView() {

        navArgs.otp.let {
            viewModel.otp.value = it
        }

        textScreenTitle.text = "Reset verification"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }

        btn_verify_otp.setOnClickListener {

            var otp = getOtpEntered(edt_otp_one,edt_otp_two,edt_otp_three,edt_otp_four)
            if (otp == viewModel.otp.value){
                findNavController().navigate(CustomerOtpFragmentDirections.openResetPassword())
            }else{
                Toast.makeText(activity,"Invalid OTP",Toast.LENGTH_SHORT).show()
            }

        }

        setupSmsEditTextUi()

    }
    private fun setupSmsEditTextUi() {
        edt_otp_one.addTextChangedListener(GenericTextWatcher(false, edt_otp_two))
        edt_otp_two.addTextChangedListener(
            GenericTextWatcher(
                false,
                edt_otp_three
            )
        )
        edt_otp_three.addTextChangedListener(
            GenericTextWatcher(
                false,
                edt_otp_four
            )
        )
        edt_otp_four.addTextChangedListener(GenericTextWatcher(true, null))

        edt_otp_one.setOnKeyListener(GenericKeyEvent(true, edt_otp_one,null))
        edt_otp_two.setOnKeyListener(GenericKeyEvent(false, edt_otp_two,edt_otp_one))
        edt_otp_three.setOnKeyListener(GenericKeyEvent(false,edt_otp_three, edt_otp_two))
        edt_otp_four.setOnKeyListener(GenericKeyEvent(false, edt_otp_four,edt_otp_three))
    }

}