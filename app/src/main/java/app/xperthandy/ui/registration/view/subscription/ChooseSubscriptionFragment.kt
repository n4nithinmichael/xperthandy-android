package app.xperthandy.ui.registration.view.subscription

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.databinding.SubscriptionPlanBinding
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.subscription_plan.*

@AndroidEntryPoint
class ChooseSubscriptionFragment : Fragment () {

    private val viewModel : SubscriptionViewModel by viewModels()

    private lateinit var loginFragmentBinding: SubscriptionPlanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

        viewModel.subscriptionList()

        viewModel.submit.observe(viewLifecycleOwner,EventObserver {

            var detailIntent = Intent(activity,SubscriptionDetailActivity::class.java)
            detailIntent.putExtra("subsId",it.id)
            startActivityForResult(detailIntent,103)

//            findNavController().navigate(ChooseSubscriptionFragmentDirections.openDetails(subsId = it.id))
        })

    }

    private fun setUpObserver() {



    }

    private fun moveToNext() {

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.subscription_plan, container, false)
        loginFragmentBinding.lifecycleOwner = this
        loginFragmentBinding.viewModel = viewModel
        return loginFragmentBinding.root
    }

    private fun setupView() {
        textScreenTitle.text = "Subscription plans"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
        val action =
            ChooseSubscriptionFragmentDirections.openDetails()
//        cardView.setOnClickListener {
//            NavHostFragment.findNavController(this).navigate(action)
//
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK){
            findNavController().navigate(ChooseSubscriptionFragmentDirections.openVideo())
        }
    }
}
@BindingAdapter(value = ["subscriptionList","subscriptionItemListener"],requireAll = false)
fun subscriptionList(recyclerView: RecyclerView,
                       list : LiveData<List<SubscriptionData>>,
                       listener: SubscriptionListener){
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = SubscriptionListAdapter(listener)
    (recyclerView.adapter as SubscriptionListAdapter).apply {
        submitList(list.value)
    }
    recyclerView.scrollToPosition(0)
}
