package app.xperthandy.ui.registration.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.ui.dashboard.DashBoardActivity
import app.xperthandy.ui.dashboard.CustomerDashboardActivity
import kotlinx.android.synthetic.main.complete_profile_activity.*

class CompleteProfileFragment : Fragment () {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_continue.setOnClickListener {


            if( AppPreferences.userRegPref.userType== UserTpe.PROVIDER.value){
                startActivity(Intent(activity, DashBoardActivity :: class.java))
                requireActivity().finish()
            }
            else{
                startActivity(Intent(activity, CustomerDashboardActivity :: class.java))
                requireActivity().finish()
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.complete_profile_activity,container,false)
    }
    private fun moveToNext() {
        val action =
            OtpFragmentDirections.openUserType()
        NavHostFragment.findNavController(this).navigate(action)
    }

}