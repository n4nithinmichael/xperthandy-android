package app.xperthandy.ui.registration.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import app.xperthandy.R
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.databinding.FragmentMobileBinding
import app.xperthandy.databinding.LoginFragmentBinding
import app.xperthandy.ui.registration.view.login.LoginFragmentDirections
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.Status
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_mobile.*
import kotlinx.android.synthetic.main.login_fragment.*

@AndroidEntryPoint
class OtpVerifySuccessFragment : Fragment () {

    private val mainViewModel : MainViewModel by viewModels()

    private lateinit var loginFragmentBinding: LoginFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

    }

    private fun setUpObserver() {



    }

    private fun moveToNext() {

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        loginFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)
        loginFragmentBinding.lifecycleOwner = this
        return loginFragmentBinding.root
    }

    private fun setupView() {

        forgotContainer.setOnClickListener {

            findNavController().navigate(LoginFragmentDirections.openSignUp())

        }
        tv_forgot_password.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.openForgotPassword())
        }


    }
}