package app.xperthandy.ui.registration.usecase.login

import app.xperthandy.data.repository.JsonRequestRepository
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.registration.model.UserRegPref
import app.xperthandy.ui.registration.usecase.UseCase
import com.airpay.shared.di.IoDispatcher
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import app.xperthandy.utils.*


class LoginUseCase @Inject constructor(
    private val repository: JsonRequestRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : UseCase<JsonObject, UserLoginData?>(dispatcher) {

    override suspend fun execute(parameters: JsonObject): UserLoginData? {
        return when (val result = repository.doLogin(parameters)) {
            is Result.Success -> result.data
            is Result.Error -> throw result.exception
            is Result.Loading -> throw  AppException(Constants.UNKNOWN_ERROR)
        }
    }
}