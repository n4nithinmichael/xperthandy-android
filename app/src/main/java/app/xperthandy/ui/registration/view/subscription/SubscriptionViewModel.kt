package app.xperthandy.ui.registration.view.subscription

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import app.xperthandy.domain.subscription.*
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.utils.Event
import app.xperthandy.utils.Result
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class SubscriptionViewModel @ViewModelInject constructor(
    private val subscriptionListUseCase: SubscriptionListUseCase,
    private val subscriptionActiveListUseCase: SubscriptionActiveListUseCase,
    private val subscriptionDetailUseCase: SubscriptionDetailUseCase,
    private val chooseSubscriptionDetailUseCase: ChooseSubscriptionDetailUseCase,
    private val confirmSubscriptionUseCase: ConfirmSubscriptionUseCase,
)
    : BaseViewModel(),SubscriptionListener {

    val list = MutableLiveData<List<SubscriptionData>>()
    val activeList = MutableLiveData<SubscriptionData>()

    var subscriptionData = MutableLiveData<SubscriptionData>()

    var _submit = MutableLiveData<Event<SubscriptionData>>()
    val submit : LiveData<Event<SubscriptionData>>  get() =  _submit

    var _subscribe = MutableLiveData<Event<Unit>>()
    val subscribe : LiveData<Event<Unit>>  get() =  _subscribe

    fun subscriptionList() {
        loading.postValue(true)
        viewModelScope.launch {
            when (val result = subscriptionListUseCase(Unit)) {
                is Result.Success -> {
                    loading.postValue(false)
                    list.postValue(result.data)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    fun subscriptionListActive() {
        loading.postValue(true)
        viewModelScope.launch {
            when (val result = subscriptionActiveListUseCase(Unit)) {
                is Result.Success -> {
                    loading.postValue(false)
                    if (!result.data.isNullOrEmpty()) {
                        activeList.postValue(result.data[0])
                    }

                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    fun subscriptionById(subsId:String) {
        loading.postValue(true)
        viewModelScope.launch {
            when (val result = subscriptionDetailUseCase(subsId)) {
                is Result.Success -> {
                    loading.postValue(false)
                    subscriptionData.postValue(result.data)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    private fun chooseSubscription() {
        loading.postValue(true)

        val subscriberId = subscriptionData.value?.id?:""

        viewModelScope.launch {
            when (val result = chooseSubscriptionDetailUseCase(subscriberId)) {
                is Result.Success -> {
                    loading.postValue(false)
                    result.data?.let {
                        _submit.value = Event(result.data)
                    }

                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

    fun confirmSubscription(orderId:String) {

        var subsId = submit.value?.peekContent()?.id?:""

        var request = JsonObject()
        request.addProperty("payment_gateway_order_id",orderId)
        request.addProperty("payment_gateway_payment_id",submit.value?.peekContent()?.order_id)
        request.addProperty("payment_gateway_signature","0")



        loading.postValue(true)
        viewModelScope.launch {
            when (val result = confirmSubscriptionUseCase(Pair(request,subsId))) {
                is Result.Success -> {
                    loading.postValue(false)
                    _subscribe.value = Event(Unit)
                }
                is Result.Error -> {
//                    _subscribe.value = Event(Unit)
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }


    override fun onItemClick(item: SubscriptionData) {
        _submit.value = Event(item)
    }


    fun subscribe() {
        chooseSubscription()
    }
}