package app.xperthandy.ui.registration.view.subscription

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import app.xperthandy.databinding.*
import app.xperthandy.ui.welcome.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_payment_subscription.*
import kotlinx.android.synthetic.main.layout_toolbar.*

@AndroidEntryPoint
class SubscriptionPaymentFragment : Fragment () {

    private val mainViewModel : MainViewModel by viewModels()

    private lateinit var binding: ActivityPaymentSubscriptionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()

    }

    private fun setUpObserver() {



    }

    private fun moveToNext() {

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
         super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_payment_subscription, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    private fun setupView() {
        textScreenTitle.text = "Subscription Payment"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
        val action =
            SubscriptionPaymentFragmentDirections.openVideo()

        btn_next.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(action)

        }


    }
}