package app.xperthandy.ui.registration.view.questionnaire

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import app.xperthandy.R
import kotlinx.android.synthetic.main.fragment_questionaire_result.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class QuestionnaireResultFragment : Fragment () {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textScreenTitle.text = "Result"
        imageBackButton.setOnClickListener { activity?.onBackPressed() }
        btn_complete_profile.setOnClickListener {
            moveToNext();
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_questionaire_result,container,false)
    }
    private fun moveToNext() {
        val action =
            QuestionnaireResultFragmentDirections.openCompleteProfile()
        NavHostFragment.findNavController(this).navigate(action)
    }

}