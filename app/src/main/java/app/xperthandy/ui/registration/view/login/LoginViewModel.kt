package app.xperthandy.ui.registration.view.login

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.ui.base.BaseViewModel
import app.xperthandy.ui.registration.usecase.login.LoginUseCase
import app.xperthandy.utils.Event
import com.google.gson.JsonObject
import kotlinx.coroutines.launch
import app.xperthandy.utils.*

class LoginViewModel @ViewModelInject constructor(
    private val loginUseCase: LoginUseCase,
    private val appPreference: AppPreferences,
) : BaseViewModel() {

    private val _submit = MutableLiveData<Event<Unit>>()
        val submit: LiveData<Event<Unit>> get() = _submit

    fun submit(){
        _submit.postValue(Event(Unit))
    }

    fun doLogin(email: String, password: String) {

        val request = JsonObject()
        request.addProperty("email",email)
        request.addProperty("password",password)

        loading.postValue(true)
        viewModelScope.launch {
            when (val result = loginUseCase(request)) {
                is Result.Success -> {
                    result.data?.let {

                        appPreference.token = it.token
                        appPreference.userName = it.name
                        appPreference.userRegPref?.apply {
                            this.alternativeNumber = it.phone?:""
                            this.dob  = it.dob?:""
                            this.email = it.email?:""
                            this.gender = it.gender?:""
                            this.fullName = it.name?:""

                            if (!it.roles.isNullOrEmpty()){
                                this.userType = it.roles[0].id
                                appPreference.userType = it.roles[0].id
                            }

                        }
                        postSuccess(result.data)
                    }
                    loading.postValue(false)
                }
                is Result.Error -> {
                    postError(result.exception.code, result.exception.message)
                    loading.postValue(false)
                }
                is Result.Loading -> loading.postValue(true)
            }
        }

    }

}