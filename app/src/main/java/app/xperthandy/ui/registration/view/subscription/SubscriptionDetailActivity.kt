package app.xperthandy.ui.registration.view.subscription

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.navArgs
import app.xperthandy.R
import app.xperthandy.databinding.SubscriptionDetailBinding
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.utils.Constants
import app.xperthandy.utils.EventObserver
import app.xperthandy.utils.showMessageBottomSheet
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.subscription_detail.*
import org.json.JSONObject

@AndroidEntryPoint
class SubscriptionDetailActivity : AppCompatActivity (), PaymentResultListener {

    private val viewModel : SubscriptionViewModel by viewModels()

    private lateinit var binding: SubscriptionDetailBinding
    val args: SubscriptionDetailActivityArgs by navArgs()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.subscription_detail
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this


        setupView()
        args.subsId.let {
            viewModel.subscriptionById(it)
        }

        Checkout.preload(applicationContext)


        viewModel.submit.observe(this,EventObserver {
            startPayment(it)
        })
        viewModel.subscribe.observe(this,EventObserver {

            var resultIntent = Intent()
            intent.putExtra(Constants.DEVICE_OS,"successData")
            setResult(RESULT_OK,resultIntent)
            finish()

//            findNavController(R.id.navFragmentIntro).navigate(SubscriptionDetailActivityDirections.openVideo())
        })

        viewModel.error.observe(this, EventObserver {
            showMessageBottomSheet(it.second)
        })

    }
    private fun setUpObserver() {

    }

    private fun moveToNext() {

    }

    private fun setupView() {

        textScreenTitle.text = "Plan details"
        imageBackButton.setOnClickListener { onBackPressed() }
//        val action =
//            SubscriptionDetailFragmentDirections.openPayment()

        btn_post2.setOnClickListener {
//            NavHostFragment.findNavController(this).navigate(action)

        }


    }

    private fun startPayment(subscriptionData: SubscriptionData) {
        /*
        *  You need to pass current activity in order to let Razorpay create CheckoutActivity
        * */

        val co = Checkout()
        co.setKeyID("rzp_test_AekvglK3ygDvVH")

        try {
            val options = JSONObject()
            options.put("name",subscriptionData.name)
            options.put("description",subscriptionData.description)
            options.put("image",subscriptionData.image)
            options.put("theme.color", subscriptionData.themeColor);
            options.put("currency",subscriptionData.currency);
            options.put("order_id", subscriptionData.order_id);
            options.put("amount",subscriptionData.amount)
            val prefill = JSONObject()
            prefill.put("email",subscriptionData.userEmail)
            options.put("prefill",prefill)
            co.open(this,options)


        }catch (e: Exception){
            Toast.makeText(this,"Error in payment: "+ e.message,Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errorCode: Int, response: String?) {
       println("ERROR:$response")
    }

    override fun onPaymentSuccess(razorpayPaymentId: String?) {
        println("SUCCESS:$razorpayPaymentId")
        viewModel.confirmSubscription(razorpayPaymentId?:"")
    }

}