package app.xperthandy.ui.registration.view.questionnaire

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.databinding.QuestionAnswerItemBinding
import app.xperthandy.model.questionnaire.Answer


class AnswerListAdapter (val listener: QuestionnaireListener): ListAdapter<Answer,AnswerListAdapter.ViewHolder>(AnswerListDiff)
{
  inner class ViewHolder(private val binding : QuestionAnswerItemBinding) : RecyclerView.ViewHolder(binding.root)
  {
      fun bind(item: Answer)
      {
          binding.apply {
              this.item = item
              this.listener = this@AnswerListAdapter.listener
              executePendingBindings()
          }
      }
  }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(QuestionAnswerItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }
}
object AnswerListDiff : DiffUtil.ItemCallback<Answer>() {
    override fun areItemsTheSame(oldItem: Answer, newItem: Answer): Boolean {
        return oldItem.id == oldItem.id
    }

    override fun areContentsTheSame(oldItem: Answer, newItem: Answer): Boolean {
        return oldItem == newItem
    }
}
