package app.xperthandy.ui.registration.model.category

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryItem(
    var background_colour_code: String?,
    var id: Int,
    var image: String?,
    var list_order: Int?,
    var name: String?,
    var selected: Boolean =false,
): Parcelable
@Parcelize
data class SubCategoryItem(
    var subCategoryName : String,
    var selected: Boolean = false
): Parcelable