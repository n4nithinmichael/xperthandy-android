package app.xperthandy.ui.registration.listners

import app.xperthandy.ui.registration.model.category.CategoryItem
import app.xperthandy.ui.registration.model.video.VideoItem


interface VideoListClickListener {
    fun onVideoClick(item: VideoItem)
}