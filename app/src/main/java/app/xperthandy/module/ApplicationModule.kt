package app.xperthandy.module


import android.content.Context
import app.xperthandy.BuildConfig
import app.xperthandy.data.api.ApiHelper
import app.xperthandy.data.api.ApiHelperImpl
import app.xperthandy.data.api.ApiService
import app.xperthandy.data.repository.*
import app.xperthandy.data.repository.preference.AppPreferences
import app.xperthandy.data.repository.room.UserDao
import app.xperthandy.utils.AuthInterceptor
import app.xperthandy.utils.EnumConverterFactory
import app.xperthandy.utils.JsonRequestInterceptor
import app.xperthandy.utils.NetworkHelper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import java.util.prefs.Preferences
import javax.inject.Named

@Module
@InstallIn(ApplicationComponent::class)
class ApplicationModule {

    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        val authInterceptor = AuthInterceptor(preferenceStorage = AppPreferences)
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(authInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()
    } else OkHttpClient
        .Builder()
        .build()


    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson,
        BASE_URL: String
    ): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()



    @Provides
    @Singleton
    @Named("jsonRequestOkHttpClient")
    fun provideJsonRequestOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        jsonInterceptor: JsonRequestInterceptor
    ): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.connectTimeout(30, TimeUnit.SECONDS)
        client.readTimeout(30, TimeUnit.SECONDS)
        client.writeTimeout(30, TimeUnit.SECONDS)
        client.addInterceptor(jsonInterceptor)
        client.addInterceptor(loggingInterceptor)
        client.retryOnConnectionFailure(false)
        return client.build()
    }


    @Provides
    @Singleton
    @Named("jsonRequestHandler")
    fun providesJsonRequestHandler(
        @Named("jsonRequestOkHttpClient") okHttpClient: OkHttpClient,
        gson: GsonConverterFactory,
        enumConverterFactory: EnumConverterFactory
    ): ApiService =
        Retrofit.Builder()
            .addConverterFactory(gson)
            .addConverterFactory(enumConverterFactory)
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build().create(ApiService::class.java)
//
    @Singleton
    @Provides
    fun provideJsonRequestRepository(
        remoteDataSource: ApiService,
        networkUtils: NetworkHelper
    ): JsonRequestRepository {
        return DefaultJsonRequestRepository(remoteDataSource, networkUtils)
    }

    @Singleton
    @Provides
    fun providesCustomerRepository(
        remoteDataSource: ApiService,
        networkUtils: NetworkHelper
    ): CustomerRepository = DefaultCustomerRepository(remoteDataSource,networkUtils)

    @Singleton
    @Provides
    fun providesServiceProviderRepository(
        remoteDataSource: ApiService,
        networkUtils: NetworkHelper
    ): ServiceProviderRepository = DefaultServiceProviderRepository(remoteDataSource,networkUtils)


    @Singleton
    @Provides
    fun providesSubscriptionRepository(
        remoteDataSource: ApiService,
        networkUtils: NetworkHelper
    ): SubscriptionRepository = DefaultSubscriptionRepository(remoteDataSource,networkUtils)


    @Singleton
    @Provides
    fun providesBankAccountRepository(
        remoteDataSource: ApiService,
        networkUtils: NetworkHelper
    ): BankAccountRepository = DefaultBankAccountRepository(remoteDataSource,networkUtils)


    @Singleton
    @Provides
    fun providesUploadRepository(
        remoteDataSource: ApiService,
        networkUtils: NetworkHelper
    ): UploadRepository = DefaultUploadRepository(remoteDataSource,networkUtils)

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiHelper: ApiHelperImpl): ApiHelper = apiHelper

    @Provides
    @Singleton
    fun provideGson(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Singleton
    fun provideEnumConverter(): EnumConverterFactory = EnumConverterFactory()

    @Provides
    @Singleton
    fun provideInstanceGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideInstanceLogging(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) }

    @Singleton
    @Provides
    fun providePreferenceStorage(@ApplicationContext context: Context) = AppPreferences

}