package app.xperthandy.Adapter

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListAdapter
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.R
import app.xperthandy.data.model.bankAccount.BankAccountListResItem
import app.xperthandy.databinding.BankListActivityBinding
import app.xperthandy.databinding.LayoutBankListRowItemBinding
import app.xperthandy.ui.dashboard.listener.BankListener
import kotlinx.android.synthetic.main.layout_dashboard_notification_item.view.*
import kotlinx.android.synthetic.main.layout_payment_row_item.view.*
import java.text.SimpleDateFormat

class BankListAdapter(val listener: BankListener) : androidx.recyclerview.widget.ListAdapter<BankAccountListResItem,BankListAdapter.ViewHolder>(BankDiff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankListAdapter.ViewHolder {
        return ViewHolder(LayoutBankListRowItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }
    inner class  ViewHolder(private val binding: LayoutBankListRowItemBinding ): RecyclerView.ViewHolder(binding.root){
       fun bind(item : BankAccountListResItem){
           binding.apply {
               this.item = item
               this.listener = this@BankListAdapter.listener
           }
       }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
object BankDiff : DiffUtil.ItemCallback<BankAccountListResItem>(){
    override fun areItemsTheSame(
        oldItem: BankAccountListResItem,
        newItem: BankAccountListResItem
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: BankAccountListResItem,
        newItem: BankAccountListResItem
    ): Boolean {
        return oldItem == newItem
    }

}