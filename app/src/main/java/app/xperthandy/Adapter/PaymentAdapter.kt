package app.xperthandy.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.xperthandy.databinding.LayoutPaymentRowItemBinding
import app.xperthandy.model.payments.PaymentsData
import app.xperthandy.ui.dashboard.bankdetails.PaymentsListener
import kotlinx.android.synthetic.main.layout_payment_row_item.view.*


class PaymentAdapter(val listener: PaymentsListener) : androidx.recyclerview.widget.ListAdapter<PaymentsData,PaymentAdapter.ViewHolder>(PaymentDiff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentAdapter.ViewHolder {
        return ViewHolder(LayoutPaymentRowItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }
    inner class  ViewHolder(private val binding: LayoutPaymentRowItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item : PaymentsData){
            binding.apply {
                this.item = item
                this.listener = this@PaymentAdapter.listener
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
object PaymentDiff : DiffUtil.ItemCallback<PaymentsData>(){
    override fun areItemsTheSame(
        oldItem: PaymentsData,
        newItem: PaymentsData
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: PaymentsData,
        newItem: PaymentsData
    ): Boolean {
        return oldItem == newItem
    }

}

