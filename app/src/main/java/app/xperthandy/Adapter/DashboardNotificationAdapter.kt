package app.xperthandy.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.xperthandy.databinding.LayoutDashboardNotificationItemBinding
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.ui.dashboard.listener.TaskListener
import kotlinx.android.synthetic.main.layout_dashboard_notification_item.view.*

class DashboardNotificationAdapter(val listener: TaskListener) : ListAdapter<JobListAllResItem,DashboardNotificationAdapter.ViewHolder>(NotificationListDiff) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutDashboardNotificationItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(getItem(position))
    }

    inner class  ViewHolder(val binding : LayoutDashboardNotificationItemBinding): androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root){
       fun bind(item : JobListAllResItem){
           binding.apply {
               this.item = item
               this.listener = this@DashboardNotificationAdapter.listener
               executePendingBindings()
           }
       }
    }
}
object NotificationListDiff : DiffUtil.ItemCallback<JobListAllResItem>() {
    override fun areItemsTheSame(oldItem: JobListAllResItem, newItem: JobListAllResItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: JobListAllResItem,
        newItem: JobListAllResItem
    ): Boolean {
        return oldItem.id == newItem.id
    }
}