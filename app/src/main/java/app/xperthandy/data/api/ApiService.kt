package app.xperthandy.data.api

import app.xperthandy.data.model.TermsUrlRes
import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.data.model.UserTypeRes
import app.xperthandy.data.model.bankAccount.AddBankAccountPayload
import app.xperthandy.data.model.bankAccount.BankAccountListRes
import app.xperthandy.data.model.bankAccount.BankAccountListResItem
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.data.model.payment.PaymentData
import app.xperthandy.data.model.payment.SelectedPayment
import app.xperthandy.domain.generic.JobStatusPayload
import app.xperthandy.model.payments.PaymentsResp
import app.xperthandy.model.questionnaire.QuestionnaireData
import app.xperthandy.model.serviceProvider.JobListAllRes
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.model.user.ProfileImage
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.model.customer.job.PostJobPayload
import app.xperthandy.ui.registration.model.category.CategoryData
import app.xperthandy.ui.registration.model.video.VideoData
import app.xperthandy.ui.registration.view.category.SaveCategoryPayload
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.ResultData
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @POST("api/v1/register")
    suspend fun registerUser(@Body jsonObject: JsonObject): Response<ResultData<UserLoginData>>
    @PUT("api/v1/user")
    suspend fun updateUser(@Body request: UpdateUserRequest): Response<ResultData<UserLoginData>>
    @GET("api/v1/user")
    suspend fun getProfile(): Response<ResultData<UserLoginData>>

    @GET("api/v1/serviceCategory")
    suspend fun serviceCategory(): Response<CategoryData>
    @GET("api/v1/informationVideo")
    suspend fun fetchInformativeVideo(): Response<VideoData>

    @POST("api/v1/login")
    suspend fun doLogin(@Body jsonObject: JsonObject): Response<ResultData<UserLoginData>>

    @POST("api/v1/passwordReset")
    suspend fun forgotPassword(@Body jsonObject: JsonObject): Response<ResultData<UserLoginData>>

    @POST("api/v1/changePassword")
    suspend fun updatePassword(@Body jsonObject: JsonObject): Response<ResultData<UserLoginData>>


    @GET("api/v1/verify-mobile")
    suspend fun requestOtpSms():Response<ResultData<UserLoginData>>
    @POST("api/v1/verify-mobile")
    suspend fun verifyOtp(@Body jsonObject: JsonObject): Response<ResultData<ResultData<UserLoginData>>>

    @GET("api/v1/verify-email")
    suspend fun requestOtpEmail(): Response<ResultData<UserLoginData>>
    @POST("api/v1/verify-email")
    suspend fun verifyEmail(@Body jsonObject: JsonObject): Response<ResultData<UserLoginData>>



    // customer

    @POST("api/v1/job")
    suspend fun postJob(@Body jsonObject: PostJobPayload): Response<ResultData<UserData>>

    @GET("api/v1/job/myPosts")
    suspend fun jobListCustomerWise():Response<ResultData<JobListAllRes>>

    @GET("api/v1/offer")
    suspend fun offerList(): Response<ResultData<UserData>>

    @GET("api/v1/offer/serviceCategory/{serviceCategory}")
    suspend fun categoryOffer(@Path (value = "serviceCategory",encoded = true) catId : Int):Response<ResultData<UserData>>




    // generic

    @GET("api/v1/job/{job}")
    suspend fun jobDetails(@Path (value = "job",encoded = true) jobId : Int) : Response<ResultData<JobListAllResItem>>

    @POST("api/v1/job/{job}/updateStatus")
    suspend fun updateJobStatus(@Body payload : JobStatusPayload, @Path (value = "job")jobId : Int) : Response<ResultData<UserData>>

    @GET("api/v1/{job}/jobPayment")
    suspend fun paymentDetails(@Path (value = "job")jobId : String) : Response<ResultData<PaymentData>>

    @POST("api/v1/{job}/applyOffer")
    suspend fun applyOffer(@Body payload : JsonObject, @Path (value = "job")jobId : String) : Response<ResultData<PaymentData>>

    @GET("api/v1/offer")
    suspend fun getOffers() : Response<ResultData<List<OfferData>>>


    @POST("api/v1/{job}/paymentMethod")
    suspend fun paymentMethod(@Body payload : JsonObject, @Path (value = "job")jobId : String) : Response<ResultData<SelectedPayment>>

    @POST("api/v1/{job}/confirmPayment")
    suspend fun confirmPayment(@Body payload : JsonObject, @Path (value = "job")jobId : String) : Response<ResultData<SelectedPayment>>

    @POST("api/v1/jobPaymentsPaid")
    suspend fun jobPaymentsPaid(@Body payload : JsonObject) : Response<ResultData<PaymentsResp>>

    @POST("api/v1/jobPaymentsReceived")
    suspend fun jobPaymentsReceived(@Body payload : JsonObject) : Response<ResultData<PaymentsResp>>


    // service provider

    @POST("api/v1/serviceCategory")
    suspend fun saveCategories(@Body payload: SaveCategoryPayload):Response<ResultData<UserData>>

    @GET("api/v1/serviceProvider")
    suspend fun serviceProviderProfile() : Response<ResultData<UserData>>

    @POST("api/v1/serviceProvider")
    suspend fun serviceProviderDataUpload(@Body payload: JsonObject):Response<ResultData<UserData>>

    @GET("api/v1/questionnaire")
    suspend fun questionnaireList():Response<ResultData<List<QuestionnaireData>?>>

    @POST("api/v1/questionnaire")
    suspend fun questionnaireSaveAnswer(@Body payload: JsonObject): Response<ResultData<UserData>>

    @GET("api/v1/job")
    suspend fun jobListAll():Response<ResultData<JobListAllRes>>

    @GET("api/v1/jobNotification")
    suspend fun jobPostedNotification():Response<ResultData<JobListAllRes>>


    // Subscription
    @GET("api/v1/subscription")
    suspend fun subscriptionList():Response<ResultData<List<SubscriptionData>>>
    @GET("api/v1/subscription/active")
    suspend fun subscriptionListActive():Response<ResultData<List<SubscriptionData>>>
    @GET("api/v1/subscription/{subscription}")
    suspend fun subscriptionActivate(@Path (value = "subscription",encoded = true) subscription : String):Response<String>
    @GET("api/v1/subscription/{subscription}")
    suspend fun subscriptionDetail(@Path (value = "subscription",encoded = true) subscription : String):Response<ResultData<SubscriptionData>>
    @POST("api/v1/subscription/{subscription}")
    suspend fun chooseSubscription(@Path (value = "subscription",encoded = true) subscription : String):Response<ResultData<SubscriptionData>>
    @POST("api/v1/subscription/confirm/{orderId}")
    suspend fun confirmSubscription(@Path (value = "orderId",encoded = true) orderId : String,@Body payload: JsonObject):Response<ResultData<SubscriptionData>>


    // bank account

    @GET("api/v1/bankAccount")
    suspend fun bankAccountList(): Response<ResultData<BankAccountListRes>>

    @POST("api/v1/bankAccount")
    suspend fun addBankAccount(@Body payload: AddBankAccountPayload): Response<ResultData<BankAccountListResItem>>

    @GET("GET api/v1/bankAccount/{bankAccount}")
    suspend fun viewBankAccount(@Path (value = "bankAccount",encoded = true)bankAccount : Int):Response<ResultData<BankAccountListResItem>>

    @PUT("api/v1/bankAccount/{bankAccount}")
    suspend fun updateBankAccount(@Path (value = "bankAccount",encoded = true)bankAccount: Int,@Body payload: JsonObject): Response<ResultData<BankAccountListResItem>>

    @DELETE("api/v1/bankAccount/{bankAccount}")
    suspend fun deleteBankAccount(@Path (value = "bankAccount",encoded = true)bankAccount:String):Response<ResultData<UserData>>

    @GET("api/v1/termsUrl")
    suspend fun termsUrl():Response<ResultData<TermsUrlRes>>

    @GET("api/v1/userType")
    suspend fun userType():Response<ResultData<UserTypeRes>>


    @Multipart
    @POST("api/v1/user/profileImage")
    suspend fun uploadStoreData(@Part image: MultipartBody.Part?): Response<ResultData<ProfileImage>>
}