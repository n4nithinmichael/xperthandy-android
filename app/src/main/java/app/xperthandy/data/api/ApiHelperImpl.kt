package app.xperthandy.data.api

import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.ui.registration.model.category.CategoryData
import app.xperthandy.ui.registration.model.video.VideoData
import app.xperthandy.ui.welcome.model.user.UserData
import com.google.gson.JsonObject
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

//    override suspend fun registerUser(jsonObject: JsonObject): Response<UserData> = apiService.registerUser(jsonObject)

//    override suspend fun updateUser(request: UpdateUserRequest): Response<UserData> {
//        return apiService.updateUser(request)
//    }

    override suspend fun fetchCategory(): Response<CategoryData> = apiService.serviceCategory()
    override suspend fun fetchInformativeVideo(): Response<VideoData> = apiService.fetchInformativeVideo()
}