package app.xperthandy.data.api

import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.ui.registration.model.category.CategoryData
import app.xperthandy.ui.registration.model.video.VideoData
import app.xperthandy.ui.welcome.model.user.UserData
import com.google.gson.JsonObject
import retrofit2.Response

interface ApiHelper {

//    suspend fun registerUser(jsonObject: JsonObject): Response<UserData>
//    suspend fun updateUser(request: UpdateUserRequest): Response<UserData>
    suspend fun fetchCategory(): Response<CategoryData>
    suspend fun fetchInformativeVideo(): Response<VideoData>

}