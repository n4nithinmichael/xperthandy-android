package app.xperthandy.data.model


import com.google.gson.annotations.SerializedName

data class TermsUrlRes(
    @SerializedName("url")
    val url: String
)