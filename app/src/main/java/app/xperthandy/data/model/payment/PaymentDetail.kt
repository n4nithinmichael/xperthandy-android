package app.xperthandy.data.model.payment


import com.google.gson.annotations.SerializedName

data class PaymentDetail(
    @SerializedName("data")
    val `data`: PaymentData?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("statusCode")
    val statusCode: Int?
)