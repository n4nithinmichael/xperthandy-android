package app.xperthandy.data.model.payment


import com.google.gson.annotations.SerializedName

data class PaymentData(
    @SerializedName("convenience_fee")
    val convenienceFee: String?,
    @SerializedName("gst")
    val gst: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("job_charges")
    val jobCharges: String?,
    @SerializedName("payment_date")
    val paymentDate: Any?,
    @SerializedName("payment_gateway_order_id")
    val paymentGatewayOrderId: Any?,
    @SerializedName("payment_status")
    val paymentStatus: Any?,
    @SerializedName("payment_type")
    val paymentType: Any?,
    @SerializedName("reward_redemption")
    val rewardRedemption: Any?,
    @SerializedName("total")
    val total: String?,
    @SerializedName("user")
    val user: User?,
    @SerializedName("voucher")
    val voucher: String?,
//    @SerializedName("voucherData")
//    val voucherData: List<Any>?
)

data class OfferData(
    @SerializedName("flat_price")
    val flatPrice: Any?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("offer_code")
    val offerCode: String?,
    @SerializedName("offer_percentage")
    val offerPercentage: Int?,
    @SerializedName("service_category")
    val serviceCategory: String?,
    @SerializedName("validity")
    val validity: String?,
    var selected : Boolean
)

data class SelectedPayment(
    @SerializedName("convenience_fee")
    val convenienceFee: String?,
    @SerializedName("gst")
    val gst: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("job_charges")
    val jobCharges: String?,
    @SerializedName("payment_date")
    val paymentDate: Any?,
    @SerializedName("payment_gateway_order_id")
    val paymentGatewayOrderId: String?,
    @SerializedName("payment_status")
    val paymentStatus: String?,
    @SerializedName("payment_type")
    val paymentType: String?,
    @SerializedName("reward_redemption")
    val rewardRedemption: Int?,
    @SerializedName("total")
    val total: String?,
    @SerializedName("user")
    val user: User?,
    @SerializedName("voucher")
    val voucher: String?,
    @SerializedName("voucherData")
    val voucherData: List<Any>?
)
