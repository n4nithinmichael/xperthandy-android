package app.xperthandy.data.model


import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class UpdateUserRequest(
    @SerializedName("area")
    val area: String?,
    @SerializedName("district")
    val district: String?,
    @SerializedName("dob")
    val dob: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("house_name")
    val houseName: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("phone_alternate")
    val phoneAlternate: String?,
    @SerializedName("pin")
    val pin: String,
    @SerializedName("roles")
    val roles: Int?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("town")
    val town: String?
) : Parcelable