package app.xperthandy.data.model

enum class Status {
    LOADING,
    FETCH,
    SUCCESS,
    ERROR,
    WARNING,
    IDLE,
}