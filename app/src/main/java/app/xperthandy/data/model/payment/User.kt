package app.xperthandy.data.model.payment


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("area")
    val area: String?,
    @SerializedName("district")
    val district: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("pin")
    val pin: String?,
    @SerializedName("profile_image")
    val profileImage: String?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("town")
    val town: String?
)