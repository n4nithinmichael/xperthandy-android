package app.xperthandy.data.model.bankAccount
import com.google.gson.annotations.SerializedName

class BankAccountListRes : ArrayList<BankAccountListResItem>()
data class BankAccountListResItem(
    @SerializedName("account_name")
    val accountName: String,
    @SerializedName("account_number")
    val accountNumber: String,
    @SerializedName("active")
    val active: Int,
    @SerializedName("bank_name")
    val bankName: String,
    @SerializedName("branch_address")
    val branchAddress: String,
    @SerializedName("branch_city")
    val branchCity: String,
    @SerializedName("branch_ifsc")
    val branchIfsc: String,
    @SerializedName("branch_name")
    val branchName: String,
    @SerializedName("branch_pin")
    val branchPin: String,
    @SerializedName("branch_state")
    val branchState: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("default")
    val default: Int,
    @SerializedName("id")
    val id: String
)
data class AddBankAccountPayload(
    @SerializedName("account_name")
    val accountName : String,
    @SerializedName("account_number")
    val accountNumber : String,
    @SerializedName("bank_name")
    val bankName : String,
    @SerializedName("branch_name")
    val branchName : String,
    @SerializedName("branch_address")
    val branchAddress : String,
    @SerializedName("branch_city")
    val branchCity : String,
    @SerializedName("branch_state")
    val branchState : String,
    @SerializedName("branch_pin")
    val branchPin : String
)
