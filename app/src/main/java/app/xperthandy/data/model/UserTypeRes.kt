package app.xperthandy.data.model


import com.google.gson.annotations.SerializedName

data class UserTypeRes(
    @SerializedName("userType")
    val userType: List<UserType>
)
data class UserType(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("guard_name")
    val guardName: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("permissions")
    val permissions: List<Any>,
    @SerializedName("type")
    val type: String,
    @SerializedName("updated_at")
    val updatedAt: String
)