package app.xperthandy.data.repository

import app.xperthandy.data.api.ApiService
import app.xperthandy.data.model.bankAccount.AddBankAccountPayload
import app.xperthandy.data.model.bankAccount.BankAccountListRes
import app.xperthandy.data.model.bankAccount.BankAccountListResItem
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.*
import com.google.gson.JsonObject
import javax.inject.Inject

interface BankAccountRepository {
    suspend fun bankAccountList():Result<BankAccountListRes?>
    suspend fun addBankAccount(payload : AddBankAccountPayload): Result<BankAccountListResItem?>
    suspend fun viewBankAccount(bankAccountId : Int): Result<BankAccountListResItem?>
    suspend fun updateBankAccount(bankAccountId : Int,payload: JsonObject): Result<BankAccountListResItem?>
    suspend fun deleteBankAccount(bankAccountId : String):Result<UserData?>
}
class DefaultBankAccountRepository @Inject constructor(
    private val dataSource : ApiService,
    networkUtils : NetworkHelper
): RemoteRepository(networkUtils),BankAccountRepository {
    override suspend fun bankAccountList(): Result<BankAccountListRes?> {
        return when(val result = execute { dataSource.bankAccountList() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun addBankAccount(payload: AddBankAccountPayload): Result<BankAccountListResItem?> {
       return when(val result = execute { dataSource.addBankAccount(payload) }){
           is Result.Success -> Result.Success(result.data.data)
           is Result.Error -> throw result.exception
           else -> throw AppException(Constants.HTTP_NO_INTERNET)
       }
    }

    override suspend fun viewBankAccount(bankAccountId: Int): Result<BankAccountListResItem?> {
        return when(val result = execute { dataSource.viewBankAccount(bankAccountId) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun updateBankAccount(
        bankAccountId: Int,
        payload: JsonObject
    ): Result<BankAccountListResItem?> {
        return when(val result = execute { dataSource.updateBankAccount(bankAccountId,payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw  result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun deleteBankAccount(bankAccountId: String): Result<UserData?> {
        return when(val result = execute { dataSource.deleteBankAccount(bankAccountId) }){
            is Result.Success ->Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
}