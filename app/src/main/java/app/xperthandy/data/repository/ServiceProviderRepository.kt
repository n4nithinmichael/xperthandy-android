package app.xperthandy.data.repository

import app.xperthandy.data.api.ApiService
import app.xperthandy.model.questionnaire.QuestionnaireData
import app.xperthandy.model.serviceProvider.JobListAllRes
import app.xperthandy.ui.registration.view.category.SaveCategoryPayload
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.*
import com.google.gson.JsonObject
import org.json.JSONObject
import javax.inject.Inject

interface ServiceProviderRepository {
    suspend fun saveCategories(payload : SaveCategoryPayload): Result<UserData?>
    suspend fun serviceProviderProfile(): Result<UserData?>
    suspend fun serviceProviderDataUpload(payload: JsonObject): Result<UserData?>
    suspend fun questionnaireList():Result<List<QuestionnaireData>?>
    suspend fun questionnaireSaveAnswer(payload: JsonObject):Result<UserData?>
    suspend fun jobListAll():Result<JobListAllRes?>
    suspend fun jobPostedNotification():Result<JobListAllRes?>
}
class DefaultServiceProviderRepository @Inject constructor(
    private val datasource : ApiService,
    networkUtils : NetworkHelper
): RemoteRepository(networkUtils),ServiceProviderRepository{
    override suspend fun saveCategories(payload: SaveCategoryPayload): Result<UserData?> {
        return when(val result = execute { datasource.saveCategories(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun serviceProviderProfile(): Result<UserData?> {
        return when(val result = execute { datasource.serviceProviderProfile() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun serviceProviderDataUpload(payload: JsonObject): Result<UserData?> {
        return when(val result = execute { datasource.serviceProviderDataUpload(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun questionnaireList(): Result<List<QuestionnaireData>?> {
        return when(val result = execute { datasource.questionnaireList() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw  AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun questionnaireSaveAnswer(payload: JsonObject): Result<UserData?> {
        return when( val result = execute { datasource.questionnaireSaveAnswer(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun jobListAll(): Result<JobListAllRes?> {
       return when(val result = execute { datasource.jobListAll() }){
           is Result.Success -> Result.Success(result.data.data)
           is Result.Error -> throw result.exception
           else -> throw AppException(Constants.HTTP_NO_INTERNET)
       }
    }

    override suspend fun jobPostedNotification(): Result<JobListAllRes?> {
        return when(val result =  execute { datasource.jobPostedNotification() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

}