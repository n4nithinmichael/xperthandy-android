package app.xperthandy.data.repository

import app.xperthandy.data.api.ApiService
import app.xperthandy.model.subscription.SubscriptionData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.NetworkHelper
import app.xperthandy.utils.Result
import com.google.gson.JsonObject
import javax.inject.Inject

interface SubscriptionRepository {
    suspend fun subscriptionList(): Result<List<SubscriptionData>?>
    suspend fun subscriptionActive(): Result<List<SubscriptionData>?>
    suspend fun subscriptionActivate(subscription:String): Result<String?>
    suspend fun subscriptionDetail(subscription:String): Result<SubscriptionData?>
    suspend fun chooseSubscription(subscription:String): Result<SubscriptionData?>
    suspend fun confirmSubscription(parameters: Pair<JsonObject, String>): Result<SubscriptionData?>

}

class DefaultSubscriptionRepository @Inject constructor(
    private val dataSource: ApiService,
    networkUtils : NetworkHelper
): RemoteRepository(networkUtils),SubscriptionRepository {

    override suspend fun subscriptionList(): Result<List<SubscriptionData>?> {
        return when(val result = execute { dataSource.subscriptionList() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }


    override suspend fun subscriptionActive(): Result<List<SubscriptionData>?> {
        return when(val result = execute { dataSource.subscriptionListActive() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }


    override suspend fun subscriptionActivate(subscription:String): Result<String?> {
        return when(val result = execute { dataSource.subscriptionActivate(subscription = subscription) }){
            is Result.Success -> Result.Success(result.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun subscriptionDetail(subscription:String): Result<SubscriptionData?> {
        return when(val result = execute { dataSource.subscriptionDetail(subscription = subscription) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun chooseSubscription(subscription:String): Result<SubscriptionData?> {
        return when(val result = execute { dataSource.chooseSubscription(subscription = subscription) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun confirmSubscription(params:Pair<JsonObject, String>): Result<SubscriptionData?> {
        return when(val result = execute { dataSource.confirmSubscription(orderId = params.second,params.first) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
}