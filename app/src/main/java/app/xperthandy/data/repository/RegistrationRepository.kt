package app.xperthandy.data.repository

import app.xperthandy.data.api.ApiHelper
import app.xperthandy.data.model.UpdateUserRequest
import com.google.gson.JsonObject
import javax.inject.Inject

class RegistrationRepository @Inject constructor(private val apiHelper: ApiHelper) {

//    suspend fun registerUser(jsonObject: JsonObject) =  apiHelper.registerUser(jsonObject)
//    suspend fun updateUser(request: UpdateUserRequest) = apiHelper.updateUser(request)
    suspend fun fetchCategory() = apiHelper.fetchCategory()
    suspend fun fetchInformativeVideo() = apiHelper.fetchInformativeVideo()
}