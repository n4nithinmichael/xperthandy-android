package app.xperthandy.data.repository.preference

import android.content.Context
import android.content.SharedPreferences
import app.xperthandy.ui.registration.model.UserRegPref
import app.xperthandy.utils.XHConstant
import dagger.Module
import javax.inject.Singleton

@Singleton
object  AppPreferences {

    private const val NAME = "xperthandy_app_preference"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    var userRegPref = UserRegPref()

    fun init(context: Context) {
        preferences = context.getSharedPreferences(
            NAME,
            MODE
        )
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    var firstRun: Boolean
        get() = preferences.getBoolean(XHConstant.IS_FIRST_RUN_PREF.first, XHConstant.IS_FIRST_RUN_PREF.second)
        set(value) = preferences.edit {
            it.putBoolean(XHConstant.IS_FIRST_RUN_PREF.first, value)
        }


    var token: String?
        get() = preferences?.getString(XHConstant.TOKEN.first, XHConstant.TOKEN.second)
        set(value) = preferences.edit {
            it.putString(XHConstant.TOKEN.first, value)
        }

    var userName: String?
        get() = preferences.getString(XHConstant.USER_NAME.first, XHConstant.USER_NAME.second)
        set(value) = preferences.edit {
            it.putString(XHConstant.USER_NAME.first, value)
        }

    var userType: Int
        get() = preferences?.getInt(XHConstant.USER_TYPE.first, XHConstant.USER_TYPE.second)
        set(value) = preferences.edit {
            it.putInt(XHConstant.USER_TYPE.first, value)
        }

    fun writePreferenceString (value:String,key:String) {
        preferences.edit {
            it.putString(key, value)
            it.commit()
        }
    }
    fun getPreferenceString (key:String) : String? {
        var defaultValue = when (key) {
            XHConstant.PREF_LANGUAGES -> {
                "en"
            }
            else -> {
                ""
            }
        }
        return preferences.getString(key,defaultValue)

    }

    fun logOut() {
        var editor =  preferences.edit()
        editor.clear()
        editor.apply()
    }
}