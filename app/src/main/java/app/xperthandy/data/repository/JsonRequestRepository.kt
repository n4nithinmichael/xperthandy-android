package app.xperthandy.data.repository

import app.xperthandy.data.api.ApiService
import app.xperthandy.data.model.UpdateUserRequest
import app.xperthandy.data.model.TermsUrlRes
import app.xperthandy.data.model.UserTypeRes
import app.xperthandy.model.user.UserLoginData
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.ui.welcome.model.user.UserInfoData
import app.xperthandy.utils.*
import app.xperthandy.utils.NetworkHelper
import com.google.gson.JsonObject
import javax.inject.Inject

interface JsonRequestRepository {
    suspend fun register(payload: JsonObject): Result<UserLoginData?>
    suspend fun update(payload: UpdateUserRequest): Result<UserLoginData?>
    suspend fun getProfile(): Result<UserLoginData?>
    suspend fun doLogin(payload: JsonObject): Result<UserLoginData?>
    suspend fun forgotPassword(payload: JsonObject):Result<UserLoginData?>
    suspend fun updatePassword(payload: JsonObject): Result<UserLoginData?>

    suspend fun requestOtpSms():Result<UserLoginData?>
    suspend fun verifyOtp(payload: JsonObject):Result<UserLoginData?>

    suspend fun requestOtpEmail(): Result<UserLoginData?>
    suspend fun verifyEmail(payload: JsonObject):Result<UserLoginData?>
    suspend fun termsUrl():Result<TermsUrlRes?>
    suspend fun userType(): Result<UserTypeRes?>
}

class DefaultJsonRequestRepository @Inject constructor(
    private val dataSource: ApiService,
    networkUtils: NetworkHelper
) : RemoteRepository(networkUtils), JsonRequestRepository {

    override suspend fun doLogin(payload: JsonObject): Result<UserLoginData?> {
        return when (val result = execute { dataSource.doLogin(payload) }) {
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun register(payload: JsonObject): Result<UserLoginData?> {
        return when (val result = execute { dataSource.registerUser(payload) }) {
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun update(payload: UpdateUserRequest): Result<UserLoginData?> {
        return when (val result = execute { dataSource.updateUser(payload) }) {
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun getProfile(): Result<UserLoginData?> {
        return when (val result = execute { dataSource.getProfile() }) {
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }


    override suspend fun forgotPassword(payload: JsonObject): Result<UserLoginData?> {
        return when(val result = execute { dataSource.forgotPassword(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun updatePassword(payload: JsonObject): Result<UserLoginData?> {
        return when(val result = execute { dataSource.updatePassword(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun requestOtpSms(): Result<UserLoginData?> {
        return when(val result = execute { dataSource.requestOtpSms() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun verifyOtp(payload: JsonObject): Result<UserLoginData?> {
        return when(val result = execute { dataSource.verifyOtp(payload) }){
            is Result.Success -> Result.Success(result.data.data?.data)
            is Result.Error -> throw  result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }


    override suspend fun requestOtpEmail(): Result<UserLoginData?> {
        return when(val result = execute { dataSource.requestOtpEmail() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun verifyEmail(payload: JsonObject): Result<UserLoginData?> {
        return when(val result = execute { dataSource.verifyEmail(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw  result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun termsUrl(): Result<TermsUrlRes?> {
        return when(val result = execute { dataSource.termsUrl() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun userType(): Result<UserTypeRes?> {
        return when(val result = execute { dataSource.userType() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw  result.exception
            else -> throw  AppException(Constants.HTTP_NO_INTERNET)
        }
    }


}