package app.xperthandy.data.repository.preference

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.core.content.edit
import app.xperthandy.utils.XHConstant
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class XHPreference @Inject constructor(@ApplicationContext context: Context)  {

    private val prefs = context.getSharedPreferences(
        XHConstant.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);

    fun getStoredString(key:String): String {
        return prefs.getString(key, "")!!
    }

    fun setStoredString(key:String,query: String) {
        prefs
            .edit()
            .putString(key, query)
            .apply()
    }

    var firstRun: Boolean
        get() = prefs.getBoolean(XHConstant.IS_FIRST_RUN_PREF.first, XHConstant.IS_FIRST_RUN_PREF.second)
        set(value) = prefs.edit {
            this.putBoolean(XHConstant.IS_FIRST_RUN_PREF.first, value)
        }

}