package app.xperthandy.data.repository


import app.xperthandy.data.api.ApiService
import app.xperthandy.model.user.ProfileImage
import app.xperthandy.utils.NetworkHelper
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject
import app.xperthandy.utils.*

interface UploadRepository {

    suspend fun uploadProfileImage(image: MultipartBody.Part?): Result<ProfileImage?>
}

class DefaultUploadRepository @Inject constructor(private val dataSource: ApiService,
                                                  networkUtils : NetworkHelper) : RemoteRepository(networkUtils), UploadRepository {

    override suspend fun uploadProfileImage(image: MultipartBody.Part?): Result<ProfileImage?> {
        return when (val result = execute {
            dataSource.uploadStoreData(image)
        }) {
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> result
            is Result.Loading -> result

        }
    }

}