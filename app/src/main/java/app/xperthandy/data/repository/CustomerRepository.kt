package app.xperthandy.data.repository

import app.xperthandy.data.api.ApiService
import app.xperthandy.data.model.payment.OfferData
import app.xperthandy.data.model.payment.PaymentData
import app.xperthandy.data.model.payment.SelectedPayment
import app.xperthandy.domain.generic.JobStatusPayload
import app.xperthandy.model.payments.PaymentsResp
import app.xperthandy.model.serviceProvider.JobListAllRes
import app.xperthandy.model.serviceProvider.JobListAllResItem
import app.xperthandy.model.customer.job.PostJobPayload
import app.xperthandy.ui.welcome.model.user.UserData
import app.xperthandy.utils.AppException
import app.xperthandy.utils.Constants
import app.xperthandy.utils.NetworkHelper
import app.xperthandy.utils.Result
import com.google.gson.JsonObject
import javax.inject.Inject

interface CustomerRepository {
    suspend fun postJob(payload : PostJobPayload): Result<UserData?>
    suspend fun jobList(): Result<JobListAllRes?>
    suspend fun offerList():Result<UserData?>
    suspend fun categoryOffer(payload: Int): Result<UserData?>
    suspend fun jobDetails(payload: Int):Result<JobListAllResItem?>
    suspend fun updateJobStatus(payload: JobStatusPayload, jobId : Int) : Result<UserData?>
    suspend fun paymentDetails(jobId : String) : Result<PaymentData?>
    suspend fun getOffers() : Result<List<OfferData>?>
    suspend fun applyOffer(payload: JsonObject,jobId : String) : Result<PaymentData?>
    suspend fun paymentMethod(payload: JsonObject,jobId : String) : Result<SelectedPayment?>
    suspend fun confirmPayment(payload: JsonObject,jobId : String) : Result<SelectedPayment?>
    suspend fun jobPaymentsPaid(payload: JsonObject) : Result<PaymentsResp?>
    suspend fun jobPaymentsReceived(payload: JsonObject) : Result<PaymentsResp?>
}

class DefaultCustomerRepository @Inject constructor(
    private val dataSource: ApiService,
    networkUtils : NetworkHelper
): RemoteRepository(networkUtils),CustomerRepository {
    override suspend fun postJob(payload: PostJobPayload): Result<UserData?> {
        return when(val result = execute { dataSource.postJob(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
            
        }
    }

    override suspend fun jobList(): Result<JobListAllRes?> {
        return when(val result = execute { dataSource.jobListCustomerWise() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun offerList(): Result<UserData?> {
        return when(val result = execute { dataSource.offerList() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw  result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun categoryOffer(payload: Int): Result<UserData?> {
        return when(val result =  execute { dataSource.categoryOffer(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun jobDetails(payload: Int): Result<JobListAllResItem?> {
        return when(val result = execute { dataSource.jobDetails(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun updateJobStatus(payload: JobStatusPayload, jobId: Int): Result<UserData?> {
        return when(val result = execute { dataSource.updateJobStatus(payload, jobId) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun paymentDetails(jobId: String): Result<PaymentData?> {
        return when(val result = execute { dataSource.paymentDetails(jobId) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }

    override suspend fun applyOffer(payload: JsonObject, jobId: String): Result<PaymentData?> {
        return when(val result = execute { dataSource.applyOffer(payload, jobId) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun getOffers(): Result<List<OfferData>?> {
        return when(val result = execute { dataSource.getOffers() }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun paymentMethod(payload: JsonObject, jobId: String): Result<SelectedPayment?> {
        return when(val result = execute { dataSource.paymentMethod(payload, jobId) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun confirmPayment(payload: JsonObject, jobId: String): Result<SelectedPayment?> {
        return when(val result = execute { dataSource.confirmPayment(payload, jobId) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun jobPaymentsPaid(payload: JsonObject): Result<PaymentsResp?> {
        return when(val result = execute { dataSource.jobPaymentsPaid(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
    override suspend fun jobPaymentsReceived(payload: JsonObject): Result<PaymentsResp?> {
        return when(val result = execute { dataSource.jobPaymentsReceived(payload) }){
            is Result.Success -> Result.Success(result.data.data)
            is Result.Error -> throw result.exception
            else -> throw AppException(Constants.HTTP_NO_INTERNET)
        }
    }
}