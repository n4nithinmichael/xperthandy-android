package app.xperthandy.data.repository

import androidx.annotation.WorkerThread
import app.xperthandy.utils.*
import app.xperthandy.utils.Constants.EMPTY_RESPONSE_MESSAGE
import retrofit2.Response
import java.net.SocketTimeoutException

abstract class RemoteRepository constructor(private val networkUtils: NetworkHelper) {

    @WorkerThread
    suspend fun <T : Any> execute(
        call: suspend () -> Response<T>
    ): Result<T> {
        return if (networkUtils.isNetworkConnected()) try {
            val response = call.invoke()
            if (response.isSuccessful) {
                val responseBody = response.body()
                return if (responseBody != null) {
                    when (responseBody) {
                        is ResultData<*> -> {
                            return if (responseBody.statusCode == Status.SUCCESS || responseBody.status == Status.SUCCESS) {
                                Result.Success(responseBody)
                            } else {
                                Result.Error.RecoverableError(
                                    AppException(if(responseBody.message.isNullOrEmpty())  EMPTY_RESPONSE_MESSAGE else responseBody.message
                                    , responseBody.statusCode)
                                )
                            }
                        }
                        else -> Result.Error.NonRecoverableError(AppException(Constants.UNKNOWN_ERROR))
                    }
                } else {
                    Result.Error.NonRecoverableError(AppException(if(response.message().isNullOrEmpty()) EMPTY_RESPONSE_MESSAGE else response.message(),Status.TIME_OUT))
                }

            } else {
                Result.Error.NonRecoverableError(AppException(if(response.message().isNullOrEmpty()) EMPTY_RESPONSE_MESSAGE else response.message(),Status.TIME_OUT))
            }
        } catch (se3: SocketTimeoutException) {
            Result.Error.NonRecoverableError(AppException(Constants.TIMEOUT_ERROR,Status.TIME_OUT))
        } catch (e: Exception) {
            println(e.printStackTrace())
            Result.Error.NonRecoverableError(AppException(Constants.UNKNOWN_ERROR))
        }
        else Result.Error.NonRecoverableError(AppException(Constants.HTTP_NO_INTERNET, Status.NO_INTERNET))
    }
}
